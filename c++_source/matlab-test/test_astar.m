%%
clear variables; close all; clc

n_grid_pt	= 30;
n_vertices	= n_grid_pt^2;
wksp		= 10;
grid_sep	= 2*wksp/(n_grid_pt - 1);
grid_coordinates= zeros(n_vertices, 2);
x_grid_srch	= linspace(-wksp, wksp, n_grid_pt);
y_grid_srch	= linspace(-wksp, wksp, n_grid_pt);
[x_plot_grid, y_plot_grid] = meshgrid(x_grid_srch, y_grid_srch);

t_step	= 1;
t_max	= 300;
t_grid	= 0:t_step:t_max;

n_edges		= 0;
n_exp_edges	= n_vertices*4;
edge_list	= zeros(n_exp_edges, 3);

for m = 1:n_vertices
	grid_coordinates(m, 1)	= -wksp + rem(m-1, n_grid_pt)*grid_sep;
	grid_coordinates(m, 2)	= -wksp + floor((m-1)/n_grid_pt)*grid_sep;
	if (m + 1 <= n_vertices) && (mod(m, n_grid_pt) ~= 0)
		edge_cost				= (mod(m*(m + 1)*(m + 1), 37) / 19) * ((m + 1)/m) + 1e-8;
		n_edges					= n_edges + 1;
		edge_list(n_edges, :)	= [m (m + 1) edge_cost];
		n_edges					= n_edges + 1;
		edge_list(n_edges, :)	= [(m + 1) m edge_cost];
	end

	if (m + n_grid_pt) <= n_vertices
		edge_cost				= (mod(m*(m + n_grid_pt)*(m + n_grid_pt), 41) / 19) * ((m + n_grid_pt)/m) + 1e-8;
		n_edges					= n_edges + 1;
		edge_list(n_edges, :)	= [m (m + n_grid_pt) edge_cost];
		n_edges					= n_edges + 1;
		edge_list(n_edges, :)	= [(m + n_grid_pt) m edge_cost];
	end
end
G = sparse(edge_list(1:n_edges,1), edge_list(1:n_edges,2), edge_list(1:n_edges,3));

disp(G)


search_data.adjacency_struct	= [];
search_data.fcn_find_nhbr		= [];
search_data.adjacency_matrix	= G;
search_data.v_start				= 687;
search_data.v_goal				= 889;
search_data.heuristic			= zeros(n_vertices, 1);

search_data.mode = 'any';


%% Search

tic
vertex_data	= astar(search_data);
path_optimal= greedy_trace(search_data.v_start, search_data.v_goal, vertex_data);
toc

vertex_data(search_data.v_goal).d

return

