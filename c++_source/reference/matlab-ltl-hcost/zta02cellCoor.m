function sgmaInit = zta02cellCoor(vertSeq, zta0)
global problem_data

faceRef = problem_data.geometry.faceRef;
vPermut	= problem_data.geometry.vPermut;
invTf	= problem_data.geometry.invTf;
H		= problem_data.hcost.H;

nSol	= 21;
nSmp	= 101;

nGridPts1	= problem_data.tiles.nGridPts_w;
nGridPts2	= problem_data.tiles.nGridPts_a;
waGrid		= problem_data.tiles.waGrid;

%----- Transform zta0 to cell coordinates
	Rtrans(:, :, 1)	= [0 -1; 1 0];											% Rotate -90
	Rtrans(:, :, 2)	= [0 1; -1 0];											% Rotate 90
	Rtrans(:, :, 3)	= [1 0; 0 -1];											% Flip H
	Rtrans(:, :, 4)	= [-1 0; 0 1];											% Flip V
	pos(1:2, 1)		= zta0(1:2, 1) - (problem_data.VCell(vertSeq(2), 1:2))' - ...
		(problem_data.VCell(vertSeq(2), 3))*[0.5;0.5];						% From inertial axes to history cell axes
	ort			= zta0(3);
	S			= eye(2);
	tileData0	= getTileData(vertSeq(1:end-1));
% 	[~, rTran0]	= getTileData(vertSeq(1:end-1));
	rTran0		= tileData0.rTran;
	for m = 1:2
		if rTran0(1, m) ~= 0
			if (m == 2) && (rTran0(1, m) <= 2) && (rTran0(1, m-1) > 2)
				S	= (Rtrans(:, :, rTran0(1, m)))'*S;
			else
				S	= Rtrans(:, :, rTran0(1, m))*S;
			end
		end	
		if m == 1
			switch rTran0(1, m)
				case {1, 2}
					ort = ort - sign(rTran0(1,m) - 1.5)*pi/2;
				case 3
					ort = -ort;
				case 4
					ort	= pi - ort;			
			end
		end
		if m == 2
			switch rTran0(1, m)
				case {1, 2}
					if rTran0(1, m-1) > 2
						ort = ort + sign(rTran0(1,m) - 1.5)*pi/2;
					else
						ort = ort - sign(rTran0(1,m) - 1.5)*pi/2;
					end					
				case 3
					ort = -ort;
				case 4
					ort	= pi - ort;			
			end
		end
	end
	pos = S*pos + (problem_data.VCell(vertSeq(2), 3))*[0.5; 0.5];						% [pos ort] is the configuration in the axes
% 	ort
    ort	= pi2pi(ort);														% system attached to the second cell of the tile
	initState= [pos; ort];
	
	%----- Pick nearest initial state
	[~, wIndx]	= min( abs(initState(2)/problem_data.nomCellSize - problem_data.tiles.dStateSp_w) );
	[~, aIndx]	= min( abs(initState(3) - problem_data.tiles.dStateSp_a) );
	sgmaInit	= zeros(nGridPts1*nGridPts2, 1);
	sgmaInit( (wIndx - 1)*problem_data.tiles.nGridPts_a + aIndx ) = 1;
    
    
    
    	function tileData = getTileData(Vseq)
		
		%----- Locations of cells
		I4	= eye(4);
		V	= problem_data.VCell(Vseq, :) / problem_data.nomCellSize;
		C	= numel(Vseq) - 2;												% Number of rectangles in channel
		pType = zeros(C, 1);												% Problem 1 or 2
		rTran = zeros(C, 2);												% Transformation to bring to standard
		
		chan(:, 1:2)	= (diag(V(2:(end-1), 3)))*ones(C, 2);				% Rectangle dimensions (dx, dy)
		chan(:, 3:4)	= V(2:(end-1), 1:2);								% Rectangle center coordinates		
		
		fChan(:, 1:2)	= (diag(V(:, 3)))*ones(C+2, 2);						% Full channel, needed only to define
		fChan(:, 3:4)	= V(:, 1:2);										% entry and exit segments

		%----- Entry segments
		ul		= zeros(C, 1);		exSeg	= zeros(C+1, 1);
		cEdge	= zeros(C, 4);

		for n = 2:(C+2)
			delX = fChan(n, 3) - fChan(n-1, 3);	
			tol = 1e-6;

			if abs(abs(delX) - sum(fChan(n-1:n, 1))/2) < tol
				if fChan(n, 3) > fChan(n-1, 3)								% X transition (left or right)
					exSeg(n-1)	= -1;										% Right
				else
					exSeg(n-1)	= 1;										% Left
				end
				ul(n-1) = 0;
				xLim	= fChan(n-1, 3) - exSeg(n-1)*fChan(n-1, 1)/2;
				yLim	= [min(fChan(n, 4) + fChan(n, 2)/2, fChan(n-1, 4) + fChan(n-1, 2)/2) ...
							max(fChan(n, 4) - fChan(n, 2)/2, fChan(n-1, 4) - fChan(n-1, 2)/2)];
				cEdge(n-1, :) = [xLim yLim(1) xLim yLim(2)];
			else
				if fChan(n, 4) > fChan(n-1, 4)								% Y transition (up or down)
					exSeg(n-1)	= 2;										% Up
				else
					exSeg(n-1)	= -2;										% Down
				end
				ul(n-1) = 1;
				yLim	= fChan(n-1, 4) + (exSeg(n-1)/2)*fChan(n-1, 2)/2;
				xLim	= [min(fChan(n, 3) + fChan(n, 1)/2, fChan(n-1, 3) + fChan(n-1, 1)/2) ...
							max(fChan(n, 3) - fChan(n, 1)/2, fChan(n-1, 3) - fChan(n-1, 1)/2)];
				cEdge(n-1, :) = [xLim(1) yLim xLim(2) yLim];
			end	
		end
		fromFace	= -exSeg(1:C, 1);
		toFace		= exSeg(2:(C+1), 1);		

		%----- Type of transition
		cVert = zeros(4, 2*C);												% Vertices of each rectangle, side by side
		for n = 1:C
			rVert(1,:) = [(chan(n, 3) - chan(n, 1)/2) (chan(n, 4) + chan(n, 2)/2)];	% Vertices of the rectangle
			rVert(2,:) = [(chan(n, 3) + chan(n, 1)/2) (chan(n, 4) + chan(n, 2)/2)];	% in CW order starting
			rVert(3,:) = [(chan(n, 3) + chan(n, 1)/2) (chan(n, 4) - chan(n, 2)/2)];	% with top-left
			rVert(4,:) = [(chan(n, 3) - chan(n, 1)/2) (chan(n, 4) - chan(n, 2)/2)];
			
			[~, loc]	= ismember([fromFace(n) toFace(n)], faceRef(:, 1:2), 'rows');
			rTran(n, :) = faceRef(loc, 4:5);								% Existing transformation on current rectangle
			pType(n)	= (ul(n) ~= ul(n+1)) + 1;
			
			tf1	= invTf(faceRef(loc, 5) + 1);	tf2 = invTf(faceRef(loc, 4) + 1);	% Get inverse transforms
			p1	= vPermut(tf1 + 1, :);			p2	= vPermut(tf2 + 1, :);
			
			rVert = I4(p2,:)*I4(p1,:)*rVert;
			cVert(:, (2*n - 1):(2*n)) = rVert;
		end
		
		tileData.chan	= chan;
		tileData.cVert	= cVert;
		tileData.pType	= pType;
		tileData.rTran	= rTran;
		tileData.fromFace= fromFace;
		tileData.toFace = toFace;
		tileData.cEdge	= cEdge;
		tileData.verts	= Vseq';
        end

end