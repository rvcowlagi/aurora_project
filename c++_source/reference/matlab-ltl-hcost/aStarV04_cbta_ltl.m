% *************************************************************************
% Author:		Raghvendra V. Cowlagi
%				Dept. of Aerospace Engineering, Georgia Inst. Tech.
% Description:	A* algorithm Last Mod.:	10/17/2010 Version:		Handles
% mmultiple goals when cost-to-come for each goal is
%				required
% *************************************************************************
function [nodeG nodeData] = aStarV04_cbta_ltl(G, states_id, nodeS, zta0, heur, final_buchi)
global problem_data;
pThres	= problem_data.pfThreshold;
H = problem_data.hcost.H;
% G	: Trasition cost matrix nodeS		: Start node nodeG		: Goal node
% set heur	: Heuristic (N x 1 vector, where N is number of nodes)

%	nodeData(n).mk	= marker, 0 = NEW, 1 = OPEN, 2 = CLOSED nodeData(n).d
%	= cost to come nodeData(n).b	= backpointer

nNodes		= size(G, 1);
nodeStruct	= struct('mk', 0, 'd', Inf, 'b', 0);
nodeData	= repmat(nodeStruct, 1, nNodes);
nodeData(nodeS).mk	= 1;	nodeData(nodeS).d	= 0;
nodeData(nodeS).st = zta0;
% nodeData.path = [];

nOpen	= 1;
openL	= [nodeS heur(nodeS)];
goalCl	= 0;

nIter	= 0;
while (nOpen ~= 0) && (~goalCl)
    nIter	= nIter + 1;
    nodeAct	= openL(1, 1);													% Get node from top of (sorted) open stack
%     fprintf('~~~~~~~nodeAct is :~~~~~~~~~~~~~~~')
%     states_id(nodeAct,1:H+1)
    %     nodeAct
    initst_Act = nodeData(nodeAct).st;                                               % Get the node init st from cbta
    nodeData(nodeAct).mk = 2;												% Mark that node as dead
    
    nOpen		= nOpen - 1;
    openL(1, :) = [];
    
    nhbrs	= find(G(nodeAct,:));
    for nodeNew = nhbrs														% For all neighbors
        %          nodeNew
        %         fprintf('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        %         vertSeq = [states_id(nodeAct,:) states_id(nodeNew,H+1)];
        %         vertSeq
        %         tpResult = cbtaFeas_ltl(vertSeq, initst_Act, pThres);
        %         newCost	= tpResult.cost;
        % % 		tHistPf		= tpResult.pfeas;
        % 		newSt		= tpResult.sgmaF;
        %         if (newCost==inf)
        %             G(nodeAct,nodeNew) = 0;
        %             G(nodeNew,nodeAct) = 0;
        % %              fprintf('~~~~~~~break~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        %             continue;
        %         end
        
        
        %         newCost	= G(nodeAct,nodeNew);
        %         % Cost to go from act to new
        
        if nodeData(nodeNew).mk == 0										% Unvisited
            vertSeq = [states_id(nodeAct,1:H+1) states_id(nodeNew,H+1)];
%             fprintf('~~~~~~~vertSeq is :~~~~~~~~~~~~~~~')
            vertSeq
            tpResult = cbtaFeas_ltl(vertSeq, initst_Act, pThres);
            newCost	= tpResult.cost;
            % 		tHistPf		= tpResult.pfeas;
            newSt		= tpResult.sgmaF;
            if (newCost==inf)
                %                 G(nodeAct,nodeNew) = 0;
                %                 G(nodeNew,nodeAct) = 0;
%                               fprintf('~~~~~~~break~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
                continue;
            end
            
            %             if  states_id(nodeNew,H+2)==final_buchi
            %                 goalCl = 1;
            %                 break;
            %             end
            
            
            
            
            nodeData(nodeNew).mk	= 1;									% Mark open
            nodeData(nodeNew).d		= nodeData(nodeAct).d + newCost;		% Update c2come of newly visited state
            nodeData(nodeNew).b		= nodeAct;
            nodeData(nodeNew).st	= newSt;
            
            tmpOpen = binSort(openL(1:nOpen, :), [nodeNew nodeData(nodeNew).d + heur(nodeNew)], 2);
            if numel(tmpOpen) == 0
                nOpen	= 0;
                openL	= [];
            else
                nOpen	= size(tmpOpen, 1);
                openL(1:nOpen, :)	= tmpOpen;								% Add [nodeNew cost] to sorted open list
                
            end
            if  states_id(nodeNew,H+2)==final_buchi
                nodeG = nodeNew;
                goalCl = 1;
                break;
            end
            
            
        elseif nodeData(nodeNew).mk == 1									% Already open, update c2come if necessary
            vertSeq = [states_id(nodeAct,1:H+1) states_id(nodeNew,H+1)];
            vertSeq
            tpResult = cbtaFeas_ltl(vertSeq, initst_Act, pThres);
            newCost	= tpResult.cost;
            % 		tHistPf		= tpResult.pfeas;
            newSt	= tpResult.sgmaF;
            if (newCost==inf)
                %                 G(nodeAct,nodeNew) = 0;
                %                 G(nodeNew,nodeAct) = 0;
%                               fprintf('~~~~~~~break~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
                continue;
            end
            if states_id(nodeNew,H+2)==final_buchi
                nodeG = nodeNew;
                goalCl = 1;
                break;
            end
            
            if nodeData(nodeNew).d > nodeData(nodeAct).d + newCost
                nodeData(nodeNew).d	= nodeData(nodeAct).d + newCost;
                nodeData(nodeNew).b	= nodeAct;
                nodeData(nodeNew).st = newSt;
                
                [~, loc] = ismember(nodeNew, openL(1:nOpen, 1));
                openL(loc, :)= [];		nOpen = nOpen - 1;
                
                tmpOpen = binSort(openL(1:nOpen, :), [nodeNew nodeData(nodeNew).d + heur(nodeNew)], 2);
                if numel(tmpOpen) == 0
                    nOpen	= 0;
                    openL	= [];
                else
                    nOpen	= size(tmpOpen, 1);
                    openL(1:nOpen, :)	= tmpOpen;							% Add [nodeNew cost] to sorted open list
                end
                if  states_id(nodeNew,H+2)==final_buchi
                    nodeG = nodeNew;
                    goalCl = 1;
                    break;
                end
                
            end
        end
    end
    
    %     goalCl = 1;
    %     for k = 1:numel(nodeG)
    %         if nodeData(nodeG(k)).mk ~= 2, goalCl = 0; break; end
    %     end
    
    
    
    
    
end

% GoalC2Come = []; for k = 1:numel(G)
% 	GoalC2Come = cat(1, GoalC2Come, nodeData(G(k)).d);
% end [LCost, LGoal] = min(GoalC2Come);
%
% if LCost < Inf
% 	SPath = G(LGoal); while SPath(1) ~= S
% 		SPath = cat(2, nodeData(SPath(1)).b, SPath);
% 	end
% else
% 	SPath = [];
% end