%{
Copyright (c) 2014 Raghvendra V. Cowlagi. All rights reserved.

Copyright notice:
=================
No part of this work may be reproduced without the written permission of
the copyright holder, except for non-profit and educational purposes under
the provisions of Title 17, USC Section 107 of the United States Copyright
Act of 1976. Reproduction of this work for commercial use is a violation of
copyright.


Disclaimer:
===========
This software program is intended for educational and research purposes.
The author and the institution with which the author is affiliated are not
liable for damages resulting the application of this program, or any
section thereof, which may be attributed to any errors that may exist in
this program.


Author information:
===================
Raghvendra V. Cowlagi, Ph.D,
Assistant Professor, Aerospace Engineering Program,
Department of Mechanical Engineering, Worcester Polytechnic Institute.
 
Higgins Laboratories, 247,
100 Institute Road, Worcester, MA 01609.
Phone: +1-508-831-6405
Email: rvcowlagi@wpi.edu
Website: http://www.wpi.edu/~rvcowlagi


The author welcomes questions, comments, suggestions for improvements, and
reports of errors in this program.


Program description:
====================
Compare given sequence of vertices with a tile from the library.
%}

function h_edge_data = get_lifted_transition(H, tile_vertices, rgn_idx_init)
global all_tile_traversal_data

if ~numel(rgn_idx_init)
	h_edge_data.cost		= 1e9;
	h_edge_data.rgn_idx_next= [];
	return;
end

%----- Find equivalent tile in library
field_name		= ['H' num2str(H)];
this_tile_data	= get_tile_data(H, tile_vertices);

this_tile_equivalent = 0;
for m1 = 1:all_tile_traversal_data.(field_name).n_tiles
	if all(this_tile_data.traversal_type == ...
			all_tile_traversal_data.(field_name).tiles(:, m1) )
		this_tile_equivalent = m1;
		break;
	end
end

if ~this_tile_equivalent, error('Eh?'); end

%----- Find rotation / reflection of tile
%{
	If any one cell is flipped, then the tile is flipped.
%}
% is_flipped = false(min(H, 2), 1);
this_tile_name		= ['tile' num2str(this_tile_equivalent)];
% for m1 = 1:min(H, 2)
% 	n_flip_base = sum(...
% 		(all_tile_traversal_data.(field_name).(this_tile_name).cell_xform(m1, :) == 3) + ...
% 		(all_tile_traversal_data.(field_name).(this_tile_name).cell_xform(m1, :) == 4) );
% 	n_flip_tile	= sum( (this_tile_data.cell_xform(m1, :) == 3) + ...
% 		(this_tile_data.cell_xform(m1, :) == 4) );
% 	
% 	if n_flip_base ~= n_flip_tile
% 		is_flipped(m1) = true;
% 	end
% end

%----- Get connectivity and cost
% if is_flipped(1)	
% 	rgn_idx_init = all_tile_traversal_data.N_REGION_TOTAL - rgn_idx_init;
% end
% size_init = size(rgn_idx_init)
% rgn_idx_next = zeros(1, all_tile_traversal_data.N_REGION_TOTAL+1);
% for m2 = 1:numel(rgn_idx_init)
% % 	disp(rgn_idx_init(m2))
% % 	if ~any( all_tile_traversal_data.(field_name).(this_tile_name).connectivity(rgn_idx_init(m2) + 1, :) ),
% % 		continue; end
% %     shenmegui = all_tile_traversal_data.(field_name).(this_tile_name).connectivity(rgn_idx_init(m2), :)
% %     size_shenmgui = size(shenmegui)
% %     next = size(rgn_idx_next)
% % 	all_tile_traversal_data.(field_name).(this_tile_name).connectivity(rgn_idx_init(m2) + 1, :)
% 	rgn_idx_next = rgn_idx_next | ...
% 		all_tile_traversal_data.(field_name).(this_tile_name).connectivity(rgn_idx_init(m2) + 1, :);
% end

rgn_idx_next = sum( all_tile_traversal_data.(field_name).(this_tile_name).connectivity(rgn_idx_init' + 1, :), 1 ) > 0;

% sparse(rgn_idx_next2 - rgn_idx_next)

% if is_flipped(2)
% 	rgn_idx_next = fliplr(rgn_idx_next);
% end

if sum(rgn_idx_next)
	h_edge_data.cost	= 1 + ( ( all_tile_traversal_data.N_REGION_TOTAL + 1 ...
		- sum(rgn_idx_next) ) / ( all_tile_traversal_data.N_REGION_TOTAL + 1 ) )^2;
	h_edge_data.rgn_idx_next= find(rgn_idx_next) - 1;
else
	h_edge_data.cost	= 1e9;
	h_edge_data.rgn_idx_next= [];
end

if h_edge_data.cost < 0
	error('What?');
end


