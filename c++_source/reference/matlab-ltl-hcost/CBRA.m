%{
Raghvendra V. Cowlagi

Description: Pre-processing Curvature-Bounded Reachability Analysis

Date: Jan. 20, 2013.
%}

function CBRA_Results = CBRA

global missionData
% drawingsOn = true;

RCrvSp		= missionData.tiles.RCrvSp;
nGridPts1	= missionData.tiles.nGridPts_w;
nGridPts2	= missionData.tiles.nGridPts_a;
waGrid		= missionData.tiles.waGrid;

CBRA_Results= cell(4, numel(RCrvSp));

for m2 = 1:numel(RCrvSp)
	RCrv	= RCrvSp(m2);
	
	%----- P1: w/o reflection
% 	travMat	= zeros(nGridPts1*nGridPts2);
	travMatOnes = zeros(20, 2);
	nReach		= 0;
	for m3 = 1:(nGridPts1*nGridPts2)
		
		dStateFrom_w_C1 = waGrid(m3, 1);
		dStateFrom_a_C1	= waGrid(m3, 2);
		
		xL	= sqrt(RCrv^2 - (RCrv*sin(dStateFrom_a_C1) - 1)^2) ...
			- RCrv*cos(dStateFrom_a_C1) + dStateFrom_w_C1;					% Clockwise
		xU	= RCrv*cos(dStateFrom_a_C1) - sqrt(RCrv^2 - ...					% Counter-clockwise
			(RCrv*sin(dStateFrom_a_C1) + 1)^2) + dStateFrom_w_C1;
		btaStL	= asin(sin(dStateFrom_a_C1) - 1/RCrv);
		btaStU	= asin(sin(dStateFrom_a_C1) + 1/RCrv);

		maxLength	= max( RCrv*abs(btaStU - dStateFrom_a_C1), ...
			RCrv*abs(dStateFrom_a_C1 - btaStL) );

		for m4 = 1:(nGridPts1*nGridPts2)
			dStateTo_w_C2	= waGrid(m4, 1);
			dStateTo_a_C2	= waGrid(m4, 2);			

			dStateTo_w_C1	= dStateTo_w_C2;
			dStateTo_a_C1	= dStateTo_a_C2;

			if ((dStateTo_w_C1 < xL) || (dStateTo_w_C1 > xU)), continue; end
			
			if ((dStateTo_a_C1 < btaStL) || (dStateTo_a_C1 > btaStU)), continue; end

			%---- Transform to coordinates relative to initial config.
			Rot0= [cos(-dStateFrom_a_C1) -sin(-dStateFrom_a_C1); ...
				sin(-dStateFrom_a_C1) cos(-dStateFrom_a_C1)];
			zF	= [Rot0*[1; (dStateTo_w_C1 - dStateFrom_w_C1)]; ...
				(dStateTo_a_C1 - dStateFrom_a_C1)];
			[~, trajLength] = asymDubinsV01(zF, RCrv, RCrv);

% 			if (trajLength <= 1.05*maxLength), travMat(m3, m4) = 1; 	end
			if (trajLength <= 1.05*maxLength)
				nReach = nReach + 1;
				travMatOnes(nReach, :) = [m3 m4];
			end
		end
	end
% 	CBRA_Results{1, m2} = travMat;
	CBRA_Results{1, m2} = sparse(travMatOnes(:, 1), travMatOnes(:, 2), ...
		ones(nReach, 1), (nGridPts1*nGridPts2), (nGridPts1*nGridPts2));
	
	
	%----- P1: w/ reflection
% 	travMat	= zeros(nGridPts1*nGridPts2);
	travMatOnes = zeros(20, 2);
	nReach		= 0;
	for m3 = 1:(nGridPts1*nGridPts2)
		
		dStateFrom_w_C1 = waGrid(m3, 1);
		dStateFrom_a_C1	= waGrid(m3, 2);
		
		xL	= sqrt(RCrv^2 - (RCrv*sin(dStateFrom_a_C1) - 1)^2) ...
			- RCrv*cos(dStateFrom_a_C1) + dStateFrom_w_C1;					% Clockwise
		xU	= RCrv*cos(dStateFrom_a_C1) - sqrt(RCrv^2 - ...					% Counter-clockwise
			(RCrv*sin(dStateFrom_a_C1) + 1)^2) + dStateFrom_w_C1;
		btaStL	= asin(sin(dStateFrom_a_C1) - 1/RCrv);
		btaStU	= asin(sin(dStateFrom_a_C1) + 1/RCrv);

		maxLength	= max( RCrv*abs(btaStU - dStateFrom_a_C1), ...
			RCrv*abs(dStateFrom_a_C1 - btaStL) );

		for m4 = 1:(nGridPts1*nGridPts2)
			dStateTo_w_C2	= waGrid(m4, 1);
			dStateTo_a_C2	= waGrid(m4, 2);
			
			dStateTo_w_C1	= 1 - dStateTo_w_C2;
			dStateTo_a_C1	= -dStateTo_a_C2;

			if ((dStateTo_w_C1 < xL) || (dStateTo_w_C1 > xU)), continue; end
			
			if ((dStateTo_a_C1 < btaStL) || (dStateTo_a_C1 > btaStU)), continue; end

			%---- Transform to coordinates relative to initial config.
			Rot0= [cos(-dStateFrom_a_C1) -sin(-dStateFrom_a_C1); ...
				sin(-dStateFrom_a_C1) cos(-dStateFrom_a_C1)];
			zF	= [Rot0*[1; (dStateTo_w_C1 - dStateFrom_w_C1)]; ...
				(dStateTo_a_C1 - dStateFrom_a_C1)];
			[~, trajLength] = asymDubinsV01(zF, RCrv, RCrv);

% 			if (trajLength <= maxLength), travMat(m3, m4) = 1; 	end
			if (trajLength <= 1.05*maxLength)
				nReach = nReach + 1;
				travMatOnes(nReach, :) = [m3 m4];
			end			
		end
	end
% 	CBRA_Results{2, m2} = travMat;
	CBRA_Results{2, m2} = sparse(travMatOnes(:, 1), travMatOnes(:, 2), ...
		ones(nReach, 1), (nGridPts1*nGridPts2), (nGridPts1*nGridPts2));
	
	
	%----- P2: w/o reflection
% 	travMat	= zeros(nGridPts1*nGridPts2);
	travMatOnes = zeros(20, 2);
	nReach		= 0;
	for m3 = 1:(nGridPts1*nGridPts2)
		
		dStateFrom_w_C1 = waGrid(m3, 1);
		dStateFrom_a_C1	= waGrid(m3, 2);
		
		xL	= sqrt(RCrv^2 - (RCrv*cos(dStateFrom_a_C1) - ...				% Clockwise
			dStateFrom_w_C1)^2) + RCrv*sin(dStateFrom_a_C1);
		btaStL	= asin(cos(dStateFrom_a_C1) - dStateFrom_w_C1/RCrv);
		if dStateFrom_a_C1 < -acos((RCrv - dStateFrom_w_C1)/RCrv)
			xU	= RCrv*sin(-dStateFrom_a_C1) - sqrt(RCrv^2 - ...			% Counter-clockwise
				(RCrv*cos(dStateFrom_a_C1) + dStateFrom_w_C1)^2);
			btaStU	= asin(cos(dStateFrom_a_C1) + dStateFrom_w_C1/RCrv);
			maxLU	= RCrv*abs((-pi/2 + btaStU) - dStateFrom_a_C1);
		else
			xU		= Inf;
			btaStU	= pi/2;
			maxLU	= RCrv*pi/2;
		end
		maxLength	= max( maxLU, RCrv*abs(dStateFrom_a_C1 - (-pi/2 + btaStL)) );
		
		for m4 = 1:(nGridPts1*nGridPts2)
			
			dStateTo_w_C2	= waGrid(m4, 1);
			dStateTo_a_C2	= waGrid(m4, 2);
			
			dStateTo_w_C1	= dStateTo_w_C2;
			dStateTo_a_C1	= dStateTo_a_C2;

			if ((dStateTo_w_C1 < xL) || (dStateTo_w_C1 > xU)), continue; end
			
			if ((dStateTo_a_C1 < btaStL) || (dStateTo_a_C1 > btaStU)), continue; end

			%---- Transform to coordinates relative to initial config.
			Rot0= [cos(-dStateFrom_a_C1) -sin(-dStateFrom_a_C1); ...
				sin(-dStateFrom_a_C1) cos(-dStateFrom_a_C1)];
			zF	= [Rot0*[dStateTo_w_C1; -dStateFrom_w_C1]; ...
				((- pi/2 + dStateTo_a_C1) - dStateFrom_a_C1)];
			[~, trajLength] = asymDubinsV01(zF, RCrv, RCrv);

% 			if (trajLength <= maxLength), travMat(m3, m4) = 1; 	end
			if (trajLength <= 1.05*maxLength)
				nReach = nReach + 1;
				travMatOnes(nReach, :) = [m3 m4];
			end
		end
	end
% 	CBRA_Results{3, m2} = travMat;
	CBRA_Results{3, m2} = sparse(travMatOnes(:, 1), travMatOnes(:, 2), ...
		ones(nReach, 1), (nGridPts1*nGridPts2), (nGridPts1*nGridPts2));	
	
	
	%----- P2: w/ reflection
% 	travMat	= zeros(nGridPts1*nGridPts2);
	travMatOnes = zeros(20, 2);
	nReach		= 0;
	for m3 = 1:(nGridPts1*nGridPts2)
		
		dStateFrom_w_C1 = waGrid(m3, 1);
		dStateFrom_a_C1	= waGrid(m3, 2);
		
		xL	= sqrt(RCrv^2 - (RCrv*cos(dStateFrom_a_C1) - ...				% Clockwise
			dStateFrom_w_C1)^2) + RCrv*sin(dStateFrom_a_C1);
		btaStL	= asin(cos(dStateFrom_a_C1) - dStateFrom_w_C1/RCrv);
		if dStateFrom_a_C1 < -acos((RCrv - dStateFrom_w_C1)/RCrv)
			xU	= RCrv*sin(-dStateFrom_a_C1) - sqrt(RCrv^2 - ...			% Counter-clockwise
				(RCrv*cos(dStateFrom_a_C1) + dStateFrom_w_C1)^2);
			btaStU	= asin(cos(dStateFrom_a_C1) + dStateFrom_w_C1/RCrv);
			maxLU	= RCrv*abs((-pi/2 + btaStU) - dStateFrom_a_C1);
		else
			xU		= Inf;
			btaStU	= pi/2;
			maxLU	= RCrv*pi/2;
		end
		maxLength	= max( maxLU, RCrv*abs(dStateFrom_a_C1 - (-pi/2 + btaStL)) );
		
		for m4 = 1:(nGridPts1*nGridPts2)
			
			dStateTo_w_C2	= waGrid(m4, 1);
			dStateTo_a_C2	= waGrid(m4, 2);
			
			dStateTo_w_C1	= 1 - dStateTo_w_C2;
			dStateTo_a_C1	= -dStateTo_a_C2;

			if ((dStateTo_w_C1 < xL) || (dStateTo_w_C1 > xU)), continue; end
			
			if ((dStateTo_a_C1 < btaStL) || (dStateTo_a_C1 > btaStU)), continue; end

			%---- Transform to coordinates relative to initial config.
			Rot0= [cos(-dStateFrom_a_C1) -sin(-dStateFrom_a_C1); ...
				sin(-dStateFrom_a_C1) cos(-dStateFrom_a_C1)];
			zF	= [Rot0*[dStateTo_w_C1; -dStateFrom_w_C1]; ...
				((- pi/2 + dStateTo_a_C1) - dStateFrom_a_C1)];
			[~, trajLength] = asymDubinsV01(zF, RCrv, RCrv);

% 			if (trajLength <= maxLength), travMat(m3, m4) = 1; 	end
			if (trajLength <= 1.05*maxLength)
				nReach = nReach + 1;
				travMatOnes(nReach, :) = [m3 m4];
			end
		end
	end
% 	CBRA_Results{4, m2} = travMat;
	CBRA_Results{4, m2} = sparse(travMatOnes(:, 1), travMatOnes(:, 2), ...
		ones(nReach, 1), (nGridPts1*nGridPts2), (nGridPts1*nGridPts2));
	
end


% 	if drawingsOn
% 		fig2 = figure('Units', 'normalized', 'Position', [0.5 0.5 0.3 0.4]);
% 		hold on; grid on; axis equal;
% 		rectangle('Position', [0 0 nomCellSize nomCellSize], 'EdgeColor', 'k');
% 		xlim([-0.05, 1.05*nomCellSize]); ylim([-0.05, 1.05*nomCellSize]);
% 	end


% 			if drawingsOn
% 				figure(fig2); cla;
% 				rectangle('Position', [0 0 nomCellSize nomCellSize], 'EdgeColor', 'k');
% 				plot(0, dStateFrom_w_C1, 'bo', 'MarkerFaceColor', 'b');
% 				quiver(0, dStateFrom_w_C1, 0.05*cos(dStateFrom_a_C1), 0.05*sin(dStateFrom_a_C1));
% 				if tileData.pType(1) == 1
% 					plot(nomCellSize, xL, 'ro', 'MarkerFaceColor', 'r');
% 					plot(nomCellSize, xU, 'ro', 'MarkerFaceColor', 'r');
% 					plot(nomCellSize, dStateTo_w_C1, 'bo', 'MarkerFaceColor', 'b');
% 					quiver(nomCellSize, dStateTo_w_C1, 0.05*cos(dStateTo_a_C1), 0.05*sin(dStateTo_a_C1));
% 				else
% 					plot(xL, 0, 'ro', 'MarkerFaceColor', 'r');
% 					plot(xU, 0, 'ro', 'MarkerFaceColor', 'r');
% 					plot(dStateTo_w_C1, 0, 'bo', 'MarkerFaceColor', 'b');
% 					quiver(dStateTo_w_C1, 0, 0.05*cos(-pi/2 + dStateTo_a_C1), 0.05*sin(-pi/2 + dStateTo_a_C1));
% 				end
% 			end

