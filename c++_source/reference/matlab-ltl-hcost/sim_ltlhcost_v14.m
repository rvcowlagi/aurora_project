%{
Copyright (c) 2014 Raghvendra V. Cowlagi. All rights reserved.

Copyright notice:
=================
No part of this work may be reproduced without the written permission of
the copyright holder, except for non-profit and educational purposes under
the provisions of Title 17, USC Section 107 of the United States Copyright
Act of 1976. Reproduction of this work for commercial use is a violation of
copyright.


Disclaimer:
===========
This software program is intended for educational and research purposes.
The author and the institution with which the author is affiliated are not
liable for damages resulting the application of this program, or any
section thereof, which may be attributed to any errors that may exist in
this program.


Author information:
===================
Raghvendra V. Cowlagi, Ph.D,
Assistant Professor, Aerospace Engineering Program,
Department of Mechanical Engineering, Worcester Polytechnic Institute.
 
Higgins Laboratories, 247,
100 Institute Road, Worcester, MA 01609.
Phone: +1-508-831-6405
Email: rvcowlagi@wpi.edu
Website: http://www.wpi.edu/~rvcowlagi


The author welcomes questions, comments, suggestions for improvements, and
reports of errors in this program.


Program description:
====================
H-cost planning algorithm for satisfying LTL specifications.
%}


clear variables; close all; clc;
global problem_data;
addpath(genpath(pwd));
global all_tile_traversal_data;

fprintf('CBTA precomputations ... \t\t\t')
% tic
% all_tile_traversal_data = calc_hcost();
% save all_tile_traversal_data_75_r3.mat all_tile_traversal_data
% toc
% return

tic
load all_tile_traversal_data_75_r3.mat all_tile_traversal_data
toc
% % return

%% Units
units.ft2m		= 0.3048;													% feet to meters
units.d2r		= pi/180;													% degrees to radians
units.mi2m		= 1609.344;													% miles to meters
units.mps2miph	= 3600/units.mi2m;											% m/s to mi/h
units.kt2miph	= 1.15078;													% knots to mi/h
units.rpm2rad	= 2*pi/60;													% rpm to rad/s
units.lbf2n		= 4.44822162;												% pounds-force to newtons
units.slug2kg	= 14.5939029;												% slugs to kg

%% Simple environment with square cell decomposition

fprintf('Loading environment map ... \t\t')
tic
n_row= 12;
n_col= 12;

nom_cell_size = 1;															% base cell size, m
VCell	= [];

for cY = 1:n_row
    for cX = 1:n_col
        VCell = cat(1, VCell, [cX-1 cY-1 1 1]*nom_cell_size);
    end
end

nVertsCD = size(VCell, 1);

%----- Construct transition cost matrix
GCell = get_adjacency_matrix_4conn(VCell);

targetCells = 134;
baseCells	= 1;

toc


%% Draw environment
fprintf('Drawing environment map ... \t\t')
tic
targetColor = [0.8 0.15 0.15];
baseColor	= [0 0.4 0.25];
threatColor = [0.25 0.25 0.2];
noFlyColor	= [0.25 0.25 0.2]; %[0 0.25 0.4];
txtSize		= 10;

figure('Units', 'normalized', 'Position', [0 0 0.9 0.9]); envAxes = axes; axis equal; hold on;
% figure; envAxes = axes; axis equal; hold on;
drawCdV07(VCell, envAxes, 'k', 1, txtSize)
drawCdV07(VCell, envAxes, 'k', 1, txtSize, baseCells, targetColor)
commandwindow;
problem_data.plots.envAxes	= envAxes;
toc

%% Set problem data
problem_data.GCell			= GCell;
problem_data.VCell			= VCell;
problem_data.vertS			= 1;
problem_data.vertG			= 144;
problem_data.nomCellSize	= nom_cell_size;
problem_data.maxHCellSize	= Inf;
problem_data.nFreeCells		= size(GCell, 1);

problem_data.constants.UNITS= units;
problem_data.constants.TOL	= 1e-6;
problem_data.constants.INFTY= 1e15;
problem_data.constants.BDTOL= 0.001;

problem_data.tilePlanner	= @cbtaFeasTPV01;
% problem_data.zta0			= [ [1; 0.5]*problem_data.nomCellSize; 0];		% initial discrete "state"
problem_data.zta0			= [0.45,0];
%----- Manhattan heuristic
mnhtHeur = zeros(size(GCell,1), 1);
vertData = astar_multiplegoals(GCell, problem_data.vertG, 1:nVertsCD, zeros(nVertsCD, 1));
for m1 = 1:nVertsCD
    mnhtHeur(m1) = vertData(m1).d;
end
problem_data.heuristic		= 1.5*mnhtHeur;

problem_data.tiles.nGridPts_w= 41;
problem_data.tiles.nGridPts_a= 121;
problem_data.tiles.dStateSp_w= linspace(0, 1, problem_data.tiles.nGridPts_w);
problem_data.tiles.dStateSp_a= linspace(-pi/2, pi/2, problem_data.tiles.nGridPts_a);
problem_data.tiles.RCrvSp	= [linspace(6, 7.5, 61) linspace(7.5, 15, 16)];
problem_data.tiles.waGrid	= zeros(problem_data.tiles.nGridPts_w*...
    problem_data.tiles.nGridPts_a, 2);
nGridPts= 0;
for m1 = 1:problem_data.tiles.nGridPts_w
    for m2 = 1:problem_data.tiles.nGridPts_a
        nGridPts = nGridPts + 1;
        problem_data.tiles.waGrid(nGridPts, :) = ...
            [problem_data.tiles.dStateSp_w(m1), problem_data.tiles.dStateSp_a(m2)];
    end
end


%% H-Cost planner parameters
H		= 5;
nHMem	= 50;

problem_data.hcost.H	= H;
problem_data.hcost.nHMem= nHMem;
% % problem_data.pfThreshold = 0.9;

zta0 = zta02rgn_idx(problem_data.zta0);

%% Create lifted graph
fprintf('Constructing lifted graph ... \t\t')
tic
%********************* Initial states.. FIX***********
vert_s	= 1;
[VH, GH, vertH_s] = get_lifted_graph(GCell, H, vert_s);
toc

%% Create Buchi automaton

fprintf('Constructing Buchi automaton ... \t')
tic
alphabet	= {'p1','p2','p3','p4','p5','p6','p7'};
orig_alph	= {'p1', 'p2', 'p3','p4','p5','p6'};
Alph_s		= alphabet_set(alphabet);

% formula		= '(G p1) & (G !p2) & (F (p3 & F p4))'; %read_formula(alphabet,orig_alph);
formula		= '(G p1) & (G !p2) & (F p3 & F p4 & F p5 & (FG p6))';
% formula		= '(G p1) & (G !p2) & (F p3))'; %read_formula(alphabet,orig_alph);
buchi_aut	= get_buchi(formula, Alph_s);
toc

p1 = [1:1:144]';
p2 = [13:1:20]';
p3 = [25 26 37 38]';
p4 = [31 32 43 44]';
p5 = [99 100 111 112]';
p6 = [131 132 143 144]';

l = max([length(p1), length(p2), length(p3), length(p4), length(p5), length(p6)]);

region = [padarray(p1,[l-length(p1) 0],'post') padarray(p2,[l-length(p2) 0],'post') padarray(p3,[l-length(p3) 0],'post') padarray(p4,[l-length(p4) 0],'post') padarray(p5,[l-length(p5) 0],'post') padarray(p6,[l-length(p6) 0],'post')];

avoidColor = [0.4 0.4 0.2];

passColor = [1 1 0.2];

drawCdV07(VCell, envAxes, 'k', 1, txtSize, p2', avoidColor)
drawCdV07(VCell, envAxes, 'k', 1, txtSize, p3', passColor)
drawCdV07(VCell, envAxes, 'k', 1, txtSize, p4', passColor)
drawCdV07(VCell, envAxes, 'k', 1, txtSize, p5', passColor)
drawCdV07(VCell, envAxes, 'k', 1, txtSize, p6', targetColor)
%% Form product graph

bl_product = get_product(H, VH, GH, vertH_s, buchi_aut, region, alphabet, Alph_s);
states_id = bl_product.states_id;
final_buchi = buchi_aut.F;
GCell_product = bl_product.adjacency;
[n_edges,~] = size(GCell_product);
nodeS = 1;
% nodeG = [2823];


%% apply A* on the big porduct graph

fprintf('Searching shortest path ...')
tic
[nodeG, shortest_path] = aStarV04_cbta_ltl_v02(GCell_product, states_id, nodeS, zta0, zeros(n_edges, 1), final_buchi);
toc
%draw shortest path from lifted graph
current = nodeG;
i=1;
while(current ~= nodeS)
    opt_path(i) = current;
    current = shortest_path(current).b;
    i=i+1;
end
opt_path(i) = nodeS;
opt_path = opt_path(end:-1:1);

path_orig = [];

for i = 1:length(opt_path)
    path_orig = [path_orig, bl_product.states_id(opt_path(i),1:H+1)];
	bl_product.states_id(opt_path(i),1);
end

pathColor = [0.1 0.9 0.2];
drawCdV07(VCell, envAxes, 'k', 2, txtSize, path_orig, pathColor)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

drawCdV07(VCell, envAxes, 'k', 2, txtSize, baseCells, targetColor)
% drawCdV07(VCell, envAxes, 'k', 2, txtSize, targetCells, targetColor)




return
