%{
Copyright (c) 2014 Raghvendra V. Cowlagi. All rights reserved.

Copyright notice: 
=================
No part of this work may be reproduced without the written permission of
the copyright holder, except for non-profit and educational purposes under
the provisions of Title 17, USC Section 107 of the United States Copyright
Act of 1976. Reproduction of this work for commercial use is a violation of
copyright.


Disclaimer:
===========
This software program is intended for educational and research purposes.
The author and the institution with which the author is affiliated are not
liable for damages resulting the application of this program, or any
section thereof, which may be attributed to any errors that may exist in
this program.


Author information:
===================
Raghvendra V. Cowlagi, Ph.D,
Assistant Professor, Aerospace Engineering Program,
Department of Mechanical Engineering, Worcester Polytechnic Institute.
 
Higgins Laboratories, 247,
100 Institute Road, Worcester, MA 01609.
Phone: +1-508-831-6405
Email: rvcowlagi@wpi.edu
Website: http://www.wpi.edu/~rvcowlagi


The author welcomes questions, comments, suggestions for improvements, and
reports of errors in this program.


Program description:
====================
Adjacency matrix (sparse matrix) from a rectangular cell decomposition, for
4-connectivity.
%}

function G = get_adjacency_matrix_4conn(V)
% Calculate G for the first time

n_verts		= size(V, 1);
edge_list	= [];

for m1 = 1:n_verts
	for m2 = (m1 + 1):n_verts
		v1 = V(m1, 1:3);
		v2 = V(m2, 1:3);
		
		if v1(3) > v2(3)
			temp = v1;
			v1 = v2;
			v2 = temp;
		end
		
		idx = (v1(1:2) - v2(1:2))./v1(3);
		if ((idx(1) <= v2(3)/v1(3)) && (idx(2) < v2(3)/v1(3))...
				&& (idx(1) >= -1) && (idx(2) > -1)) || ...
			((idx(2) <= v2(3)/v1(3)) && (idx(1) < v2(3)/v1(3))...
				&& (idx(2) >= -1) && (idx(1) > -1))						% 4-connectivity
			edge_list = cat(1, edge_list, [m1 m2 1; m2 m1 1]);
		end
		
	end
end
G = sparse(edge_list(:,1), edge_list(:,2), edge_list(:,3), n_verts, n_verts);