function dist = Find_dist(VCell, nodeG)
   nVertsCD = size(VCell, 1);
   dist = zeros(nVertsCD, 1);
   
   coodi_x = VCell(nodeG, 1);
   coodi_y = VCell(nodeG, 2);
   
   for i = 1:nVertsCD
       abs_X = abs(VCell(i, 1) - coodi_x);
       abs_Y = abs(VCell(i, 2) - coodi_y);
       dist(i) = abs_X + abs_Y;
   end 
end