%{
Raghvendra V Cowlagi, 
Cambridge, MA

Jan. 23, 2013

CBTA-based tile planner for probabilistic feasibility guarantees.
%}

function tpResult = cbtaFeas_ltl(vertSeq, sgmaInit, pThres, zta0)
global problem_data

%----- Find equivalent tile in database
faceRef = problem_data.geometry.faceRef;
vPermut	= problem_data.geometry.vPermut;
invTf	= problem_data.geometry.invTf;
H		= problem_data.hcost.H;

nSol	= 21;
nSmp	= 101;

nGridPts1	= problem_data.tiles.nGridPts_w;
nGridPts2	= problem_data.tiles.nGridPts_a;
waGrid		= problem_data.tiles.waGrid;

if nargin == 2
	pThres = 1;
end

if nargin > 3																% Input argumnet is actual state, not index	
	%----- Transform zta0 to cell coordinates
	Rtrans(:, :, 1)	= [0 -1; 1 0];											% Rotate -90
	Rtrans(:, :, 2)	= [0 1; -1 0];											% Rotate 90
	Rtrans(:, :, 3)	= [1 0; 0 -1];											% Flip H
	Rtrans(:, :, 4)	= [-1 0; 0 1];											% Flip V
	pos(1:2, 1)		= zta0(1:2, 1) - (problem_data.VCell(vertSeq(2), 1:2))' - (problem_data.VCell(vertSeq(2), 3))*[0.5;0.5];						% From inertial axes to history cell axes
	ort			= zta0(3);
	S			= eye(2);
	tileData0	= getTileData(vertSeq(1:end-1));
% 	[~, rTran0]	= getTileData(vertSeq(1:end-1));
	rTran0		= tileData0.rTran;
	for m = 1:2
		if rTran0(1, m) ~= 0
			if (m == 2) && (rTran0(1, m) <= 2) && (rTran0(1, m-1) > 2)
				S	= (Rtrans(:, :, rTran0(1, m)))'*S;
			else
				S	= Rtrans(:, :, rTran0(1, m))*S;
			end
		end	
		if m == 1
			switch rTran0(1, m)
				case {1, 2}
					ort = ort - sign(rTran0(1,m) - 1.5)*pi/2;
				case 3
					ort = -ort;
				case 4
					ort	= pi - ort;			
			end
		end
		if m == 2
			switch rTran0(1, m)
				case {1, 2}
					if rTran0(1, m-1) > 2
						ort = ort + sign(rTran0(1,m) - 1.5)*pi/2;
					else
						ort = ort - sign(rTran0(1,m) - 1.5)*pi/2;
					end					
				case 3
					ort = -ort;
				case 4
					ort	= pi - ort;			
			end
		end
	end
	pos = S*pos + (problem_data.VCell(vertSeq(2), 3))*[0.5; 0.5];						% [pos ort] is the configuration in the axes
	ort	= pi2pi(ort);														% system attached to the second cell of the tile
	initState= [pos; ort];
	
	%----- Pick nearest initial state
	[~, wIndx]	= min( abs(initState(2)/problem_data.nomCellSize - problem_data.tiles.dStateSp_w) );
	[~, aIndx]	= min( abs(initState(3) - problem_data.tiles.dStateSp_a) );
	sgmaInit	= zeros(nGridPts1*nGridPts2, 1);
	sgmaInit( (wIndx - 1)*problem_data.tiles.nGridPts_a + aIndx ) = 1;
end

sc			= problem_data.sc;
tile_edge	= getTileData(vertSeq);											% tile corresponding to the edge in lifted graph

nRCrvSp			= numel(problem_data.tiles.RCrvSp);
tpResult.cost	= Inf;
tpResult.pfeas	= 0;
tpResult.sgmaF	= zeros(nGridPts1*nGridPts2, 1);
% prevCost		= 1;

%***** IN THE FUTURE, THIS WILL BE A RANGE CORRESPONDING TO AN INTERVAL OF
% PFAIL
nRCrv_first = find((problem_data.tiles.pfeasSp >= pThres), 1, 'first');




itf1= problem_data.geometry.invTf(tile_edge.rTran(1,1) + 1);
itf2= problem_data.geometry.invTf(tile_edge.rTran(1,2) + 1);	
[~, cell2tfIndx_tile1]	= ismember([tile_edge.pType(2) tile_edge.rTran(2, :)], ...
	problem_data.geometry.faceRef(:, 3:5), 'rows');
cell2itfIndx	= problem_data.geometry.tfRef( problem_data.geometry.tfRef( ...
	cell2tfIndx_tile1, (itf2 + 1)), (itf1 + 1) );
cell2IrTran_tile1 = faceRef(cell2itfIndx, 4:5);	
nReflections_tile1= sum(cell2IrTran_tile1 > 2);
if		(tile_edge.pType(1) == 1) && mod(nReflections_tile1, 2) == 0
	tileIndx1 = 1;
elseif	(tile_edge.pType(1) == 1) && mod(nReflections_tile1, 2) ~= 0
	tileIndx1 = 2;
elseif	(tile_edge.pType(1) == 2) && mod(nReflections_tile1, 2) == 0
	tileIndx1 = 3;
elseif	(tile_edge.pType(1) == 2) && mod(nReflections_tile1, 2) ~= 0
	tileIndx1 = 4;
end
	
for m2 = nRCrv_first:nRCrvSp
	tile_edge.rad	= problem_data.tiles.RCrvSp(m2)*ones(1, H+1);
	cbtaResults		= CBTA_RNested(tile_edge);
	
		
	reachMat1	= problem_data.tiles.CBRAResults{tileIndx1, m2};
	
	%{
		sgma1: configurations that
		1. belong to first cell target set of tile 1 (corresponding to
		vertex 1)
		2. have non-empty reachable set across first cell
	
		sgma1I: configurations that belong to both sgma1 and sgmaInit
	%}
% 	sgma1 = zeros(nGridPts1*nGridPts2, 1);
	tmp		= 1:(nGridPts1*nGridPts2);
	sgma1	= zeros(nGridPts1*nGridPts2, 1);
	for m30 = tmp(sgmaInit' > 0)
		if ~any( reachMat1(m30, :) ), continue; end							% must have non-empty reachable set across first cell
		
		dStateFrom_w_C1 = waGrid(m30, 1);
		dStateFrom_a_C1	= waGrid(m30, 2);
		smp_wFrom		= min(max(ceil(dStateFrom_w_C1*nSmp), 1), nSmp);
		if cbtaResults.alfaSmp(1, smp_wFrom) < ...
				cbtaResults.alfaSmp(2, smp_wFrom)
			continue;
		end
		if (dStateFrom_a_C1 > cbtaResults.alfaSmp(1, smp_wFrom)) ||...
				(cbtaResults.alfaSmp(2, smp_wFrom) > dStateFrom_a_C1)
			continue;														% belong to first cell target set of tile 1
		end
		sgma1(m30) = 1;
	end
	
	sgma1I	= sgma1;
	
% 	disp( problem_data.tiles.RCrvSp(m2) )
% 	sparse(reachMat1((sgma1I > 0), :))
	
	reach_sgma1I = (sum(reachMat1((sgma1I > 0), :), 1)) > 0;				% points reachable from sgma1I
	
	sgmaF	= zeros(nGridPts1*nGridPts2, 1);
	
	for m30 = tmp(reach_sgma1I)
		dStateFrom_w_C1 = waGrid(m30, 1);
		dStateFrom_a_C1	= waGrid(m30, 2);
		smp_wFrom		= min(max(ceil(dStateFrom_w_C1*nSmp), 1), nSmp);
		if cbtaResults.alfaSmp(3, smp_wFrom) < ...
				cbtaResults.alfaSmp(4, smp_wFrom)
			continue;
		end
		if (dStateFrom_a_C1 > cbtaResults.alfaSmp(3, smp_wFrom)) ||...
				(cbtaResults.alfaSmp(4, smp_wFrom) > dStateFrom_a_C1)
			continue;														% belong to first cell target set of tile 2
		end
		sgmaF(m30) = 1;
	end
	
	if any(sgmaF)
		thisCost	= (nGridPts1*nGridPts2 - sum(sgmaF)) / (nGridPts1*nGridPts2);		% larger the reachable set, the better
% 		thisCost	= 1;													% binary: is/not feasible
		thisPFeas	= 0.5 + 0.5*erf( (problem_data.tiles.RCrvSp(m2) ...
			- sc.mean) / (sqrt(2)*sc.stddev) );								% specific formula for normal distribution

		tpResult.cost	= thisCost;
		tpResult.pfeas	= thisPFeas;
		tpResult.sgmaF	= sgmaF;
		
% 		if (thisPFeas >= pThres) || (prevCost == 1)
% 			tpResult.cost	= thisCost;
% 			tpResult.pfeas	= thisPFeas;
% 			tpResult.sgmaF	= sgmaF;
% 		else
% 			break;
% 		end
% 		prevCost	= 0;
	else
		break;
	end
	
	if thisPFeas > pThres, break; end
end

	%**********************************************************************
	function tileData = getTileData(Vseq)
		
		%----- Locations of cells
		I4	= eye(4);
		V	= problem_data.VCell(Vseq, :) / problem_data.nomCellSize;
		C	= numel(Vseq) - 2;												% Number of rectangles in channel
		pType = zeros(C, 1);												% Problem 1 or 2
		rTran = zeros(C, 2);												% Transformation to bring to standard
		
		chan(:, 1:2)	= (diag(V(2:(end-1), 3)))*ones(C, 2);				% Rectangle dimensions (dx, dy)
		chan(:, 3:4)	= V(2:(end-1), 1:2);								% Rectangle center coordinates		
		
		fChan(:, 1:2)	= (diag(V(:, 3)))*ones(C+2, 2);						% Full channel, needed only to define
		fChan(:, 3:4)	= V(:, 1:2);										% entry and exit segments

		%----- Entry segments
		ul		= zeros(C, 1);		exSeg	= zeros(C+1, 1);
		cEdge	= zeros(C, 4);

		for n = 2:(C+2)
			delX = fChan(n, 3) - fChan(n-1, 3);	
			tol = 1e-6;

			if abs(abs(delX) - sum(fChan(n-1:n, 1))/2) < tol
				if fChan(n, 3) > fChan(n-1, 3)								% X transition (left or right)
					exSeg(n-1)	= -1;										% Right
				else
					exSeg(n-1)	= 1;										% Left
				end
				ul(n-1) = 0;
				xLim	= fChan(n-1, 3) - exSeg(n-1)*fChan(n-1, 1)/2;
				yLim	= [min(fChan(n, 4) + fChan(n, 2)/2, fChan(n-1, 4) + fChan(n-1, 2)/2) ...
							max(fChan(n, 4) - fChan(n, 2)/2, fChan(n-1, 4) - fChan(n-1, 2)/2)];
				cEdge(n-1, :) = [xLim yLim(1) xLim yLim(2)];
			else
				if fChan(n, 4) > fChan(n-1, 4)								% Y transition (up or down)
					exSeg(n-1)	= 2;										% Up
				else
					exSeg(n-1)	= -2;										% Down
				end
				ul(n-1) = 1;
				yLim	= fChan(n-1, 4) + (exSeg(n-1)/2)*fChan(n-1, 2)/2;
				xLim	= [min(fChan(n, 3) + fChan(n, 1)/2, fChan(n-1, 3) + fChan(n-1, 1)/2) ...
							max(fChan(n, 3) - fChan(n, 1)/2, fChan(n-1, 3) - fChan(n-1, 1)/2)];
				cEdge(n-1, :) = [xLim(1) yLim xLim(2) yLim];
			end	
		end
		fromFace	= -exSeg(1:C, 1);
		toFace		= exSeg(2:(C+1), 1);		

		%----- Type of transition
		cVert = zeros(4, 2*C);												% Vertices of each rectangle, side by side
		for n = 1:C
			rVert(1,:) = [(chan(n, 3) - chan(n, 1)/2) (chan(n, 4) + chan(n, 2)/2)];	% Vertices of the rectangle
			rVert(2,:) = [(chan(n, 3) + chan(n, 1)/2) (chan(n, 4) + chan(n, 2)/2)];	% in CW order starting
			rVert(3,:) = [(chan(n, 3) + chan(n, 1)/2) (chan(n, 4) - chan(n, 2)/2)];	% with top-left
			rVert(4,:) = [(chan(n, 3) - chan(n, 1)/2) (chan(n, 4) - chan(n, 2)/2)];
			
			[~, loc]	= ismember([fromFace(n) toFace(n)], faceRef(:, 1:2), 'rows');
			rTran(n, :) = faceRef(loc, 4:5);								% Existing transformation on current rectangle
			pType(n)	= (ul(n) ~= ul(n+1)) + 1;
			
			tf1	= invTf(faceRef(loc, 5) + 1);	tf2 = invTf(faceRef(loc, 4) + 1);	% Get inverse transforms
			p1	= vPermut(tf1 + 1, :);			p2	= vPermut(tf2 + 1, :);
			
			rVert = I4(p2,:)*I4(p1,:)*rVert;
			cVert(:, (2*n - 1):(2*n)) = rVert;
		end
		
		tileData.chan	= chan;
		tileData.cVert	= cVert;
		tileData.pType	= pType;
		tileData.rTran	= rTran;
		tileData.fromFace= fromFace;
		tileData.toFace = toFace;
		tileData.cEdge	= cEdge;
		tileData.verts	= Vseq';
	end
	%======================================================================
	
	%**********************************************************************
	function cbtaTileResults = CBTA_RNested(tileData)
	%{	
		Curvature-Bounded Traversability Analysis
		1. tileData struct:			
			tileData.rad	= min radius of turn within each cell in the tile
			tileData.cVert	= coordinates of A,B,C,D vertices of second cell of
				the tile (through which traj is to be planned)
			tileData.pType	= whether second cell has traversal across parallel
				(1) or adjacent edges (2)
		2. cbtaTileResults struct:
			cbtaTileResults.alfaSmp	= reachable sets for cells 2 through H+1
			cbtaTileResults.btaSmp	= target sets for cells 2 through H+1; for
				cell C = H+1, target set is ([-pi/2, pi/2] x entire exit segment)
			cbtaTileResults.nSol	= number of points on each entry segment
				for which reachable orientation set is computed
			cbtaTileResults.nSmp	= number of grid points on each entry and
				exit segments
			cbtaTileResults.wL		= lower boundary of entry segments
			cbtaTileResults.wU		= upper boundary of entry segments
			cbtaTileResults.xSmp	= grid points on exit segments
			cbtaTileResults.wSmp	= grid points on entry segments
	%}	
	
		yEx	= zeros(H,1);		zEx	= zeros(H,1);
		wU	= zeros(H,1);		wL	= zeros(H,1);
		
		cVert	= tileData.cVert;
		cEdge	= tileData.cEdge;
		chan	= tileData.chan;

		wSol	= zeros(H, nSol);
		wSmp	= zeros(H, nSmp);
		xSmp	= zeros(H, nSmp);
		btaSmp	= zeros(2*H, nSmp);											% aSmp stores relative angles, not transformed angles
		btaSmp((2*H-1):(2*H), :) = [pi/2*ones(1, nSmp); -pi/2*ones(1, nSmp)];

		alfaSol	= zeros(2*H, nSol);
		alfaSmp(1:2:(2*H-1), :) = -Inf(H, nSmp);
		alfaSmp(2:2:2*H, :)		= Inf(H, nSmp);
		
		pType	= tileData.pType;
		rTran	= tileData.rTran;
		for n = 1:H
			switch pType(n)
				case 1														% Measure distance from C for y, z
					y1	= norm(cVert(3, (2*n - 1):(2*n)) - cEdge(n+1, 1:2));
					y2	= norm(cVert(3, (2*n - 1):(2*n)) - cEdge(n+1, 3:4));
				case 2														% Measure distance from D for y, z
					y1	= norm(cVert(4, (2*n - 1):(2*n)) - cEdge(n+1, 1:2));
					y2	= norm(cVert(4, (2*n - 1):(2*n)) - cEdge(n+1, 3:4));
			end
			yEx(n)	= (min(y1, y2));	zEx(n)	= (max(y1, y2));
		end
		
		
		for n = 1:H
			w1	= norm(cVert(4, (2*n - 1):(2*n)) - cEdge(n, 1:2));			% Measure distance from D for wL, wU
			w2	= norm(cVert(4, (2*n - 1):(2*n)) - cEdge(n, 3:4));
			wL(n)	= (min(w1, w2));
			wU(n)	= (max(w1, w2));
		end
		for n = 1:H
			if pType(n) == 2		
				wSolMin	= max(wL(n), 1e-3);
			else
				wSolMin	= wL(n);
			end
			wSol(n,:)	= linspace(wSolMin, wU(n), nSol);
			wSmp(n, :)	= linspace(wL(n), wU(n), nSmp);
			xSmp(n, :)	= linspace(yEx(n), zEx(n), nSmp);
		end
		
		if pType(H) == 1
			coneFhandle = @CBTA_S1;
		else
			coneFhandle = @CBTA_S2;
		end
		for q = 1:nSol
			w = wSol(H, q);	
			[alfa, ~,~,~]	= coneFhandle(w, chan(H,1), tileData.rad(H+1), ...
				xSmp(H,:), btaSmp((2*H-1):(2*H), :));
			alfaSol((2*H-1):(2*H), q)	= alfa';		
		end

		% --------- Interpolate alfaSol over the whole grid wSmp
		alfaSmp(((2*H-1):(2*H)), :) = interpWInf(wSol(H, :), ...
			alfaSol(((2*H-1):(2*H)), :), wSmp(H, :), 5);

		for p = (H-1):-1:1
			% ------- Transform alfa of previous to get bta for current
			if (ismember(3, rTran(p + 1, :)) && ~ismember(4, rTran(p + 1, :))) ...
					|| (~ismember(3, rTran(p + 1, :)) && ismember(4, rTran(p + 1, :)))	% If flipped once
				btaSmp((2*p-1):(2*p), :) = fliplr(-flipud(alfaSmp((2*p+1):(2*p+2), :)));
			else
				btaSmp((2*p-1):(2*p), :) = alfaSmp((2*p+1):(2*p+2), :);
			end
			if (ismember(3, rTran(p, :)) && ~ismember(4, rTran(p, :))) ...
					|| (~ismember(3, rTran(p, :)) && ismember(4, rTran(p, :)))		% If flipped once

				btaSmp((2*p-1):(2*p), :) = fliplr(-flipud(btaSmp((2*p-1):(2*p), :)));
			end

			% ------- Solve for target sets
			if pType(p) == 1
				coneFhandle = @CBTA_S1;
			else
				coneFhandle = @CBTA_S2;
			end

			for q = 1:nSol		
				w = wSol(p, q);	
				[alfa, ~,~,~]	= coneFhandle(w, chan(p,1), tileData.rad(p+1), ...
					xSmp(p,:), btaSmp((2*p-1):(2*p), :));
				alfaSol((2*p-1):(2*p), q)	= alfa';
			end

			% ------- Interpolate over all x		
			alfaSmp(((2*p-1):(2*p)), :) = interpWInf(wSol(p, :), ...
				alfaSol(((2*p-1):(2*p)), :), wSmp(p, :), 5);
		end

		cbtaTileResults.alfaSmp	= alfaSmp;
		cbtaTileResults.btaSmp	= btaSmp;
		cbtaTileResults.wL		= wL;
		cbtaTileResults.wU		= wU;
		cbtaTileResults.xSmp	= xSmp;
		cbtaTileResults.wSmp	= wSmp;
		cbtaTileResults.wSol	= wSol;
		cbtaTileResults.alfaSol	= alfaSol;
	end
	%======================================================================
	
	%**********************************************************************
	function yInter = interpWInf(x, y, xInter, polyOrder)

		[~, notInfIndx] = remInf(y(1,:));
		polyOrder		= min(polyOrder, (numel(notInfIndx)-1));
		if numel(notInfIndx)
			indx1		= find(xInter >= x(notInfIndx(1)), 1, 'first');
			indx2		= find(xInter <= x(notInfIndx(end)), 1, 'last');
			yInter		= [-Inf(1, numel(xInter)); Inf(1, numel(xInter))];	
			yPolyApp1	= polyfit(x(notInfIndx), y(1, notInfIndx), polyOrder);
			yPolyApp2	= polyfit(x(notInfIndx), y(2, notInfIndx), polyOrder);
			yInter(:, indx1:indx2) = [polyval(yPolyApp1, xInter(indx1:indx2)); ...
				polyval(yPolyApp2, xInter(indx1:indx2))];	
		else	
			yInter		= kron([-Inf; Inf], ones(1, numel(xInter)));
		end
	end
	%======================================================================
end