%{
Copyright (c) 2014 Raghvendra V. Cowlagi. All rights reserved.

Copyright notice: 
=================
No part of this work may be reproduced without the written permission of
the copyright holder, except for non-profit and educational purposes under
the provisions of Title 17, USC Section 107 of the United States Copyright
Act of 1976. Reproduction of this work for commercial use is a violation of
copyright.


Disclaimer:
===========
This software program is intended for educational and research purposes.
The author and the institution with which the author is affiliated are not
liable for damages resulting the application of this program, or any
section thereof, which may be attributed to any errors that may exist in
this program.


Author information:
===================
Raghvendra V. Cowlagi, Ph.D,
Assistant Professor, Aerospace Engineering Program,
Department of Mechanical Engineering, Worcester Polytechnic Institute.
 
Higgins Laboratories, 247,
100 Institute Road, Worcester, MA 01609.
Phone: +1-508-831-6405
Email: rvcowlagi@wpi.edu
Website: http://www.wpi.edu/~rvcowlagi


The author welcomes questions, comments, suggestions for improvements, and
reports of errors in this program.


Program description:
====================
Create lifted graph.
%}

function [VH, GH, vertH_s] = get_lifted_graph(G, H, vert_s)
% G	: Adjacency matrix
% H	: Length  of history, each vertex in lifted graph is of length H+1
% VH: List identifying vertices of GH with those of G
% GH: Adjacency matrix of lifted graph


n_verts		= size(G, 1);													% Number of vertices in original graph

n_Hseries	= [4 12 36 100 284 780 2172 5916 16268 44100]';
n_vertsH_exp= n_Hseries(H)*n_verts;

% ---- Variables initialization ----
VH = zeros(n_vertsH_exp, H+1);

idx_hist= zeros(n_verts + 1, 1);
for v = 1:n_verts
	allHist			= get_histories(G, v, H+1, 0, v, []);
	idx_hist(v + 1)	= idx_hist(v) + size(allHist, 1);
	if ~numel(allHist), continue; end
	VH( (idx_hist(v) + 1):idx_hist(v + 1), :) = allHist;
	if v == vert_s
		vertH_s = ((idx_hist(v) + 1):idx_hist(v + 1))';
	end
end
n_vertsH	= idx_hist(end);
n_edgesH_exp= n_vertsH*4;
edgesH		= zeros(n_edgesH_exp, 3);
VH( (n_vertsH + 1):end, :) = [];


% GH	= sparse(n_vertsH, n_vertsH);
n_edgesH= 0;
for vertH = 1:n_vertsH	
	for m1 = (idx_hist(VH(vertH, 2)) + 1):(idx_hist(VH(vertH, 2) + 1))
		if (~any(VH(m1, 1:H) - VH(vertH, 2:(H+1)))) && (G(VH(vertH, H+1), VH(m1, H+1))) ...
				&& (VH(vertH, 1) ~= VH(m1, H+1))
			n_edgesH			= n_edgesH + 1;
			edgesH(n_edgesH, :) = [vertH m1 1];
% 			GH(vertH, m1) = 1;
		end
	end
end
GH = sparse(edgesH(1:n_edgesH, 1), edgesH(1:n_edgesH, 2), edgesH(1:n_edgesH, 3), n_vertsH, n_vertsH);