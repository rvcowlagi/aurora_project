% *************************************************************************
% Author:		Raghvendra V. Cowlagi
%				Dept. of Aerospace Engineering, Georgia Inst. Tech.
% Description:	Remove Inf values from a vector; return the largest
%				contiguous interval with non-Inf values
% Last Mod.:	08/12/2010
% *************************************************************************
function [u, indx] = remInf(v)

infty = (v == Inf) + (v == -Inf);
if ~nnz(infty), u = v; indx = 1:numel(v); return; end
if nnz(infty) == numel(v); u = []; indx = []; return; end

firstInf	= find(infty, 1, 'first');
nonInf		= (v(firstInf:end) ~= Inf).*(v(firstInf:end) ~= -Inf);
endFirstInf = find(nonInf, 1, 'first') - 1 + firstInf - 1;
if ~numel(endFirstInf), endFirstInf = numel(v); end

if (firstInf - 1) >= (numel(v) - endFirstInf)
	u	= v(1:(firstInf-1));
	indx= 1:(firstInf - 1);
else
	[u, indx]	= remInf(v((endFirstInf + 1):end));
	indx		= endFirstInf + indx;
end