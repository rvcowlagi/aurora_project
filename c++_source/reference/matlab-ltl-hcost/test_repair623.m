%{
Raghvendra V. Cowlagi
Cambridge, MA.

Jan. 23, 2013

Simulation script for completely decoupled H-Cost planning with
probabilistic guarantees on feasibility 
%}

clear variables; close all; clc;
global missionData 

addpath(genpath(pwd));

%% Units
units.ft2m		= 0.3048;													% feet to meters
units.d2r		= pi/180;													% degrees to radians
units.mi2m		= 1609.344;													% miles to meters
units.mps2miph	= 3600/units.mi2m;											% m/s to mi/h
units.kt2miph	= 1.15078;													% knots to mi/h
units.rpm2rad	= 2*pi/60;													% rpm to rad/s
units.lbf2n		= 4.44822162;												% pounds-force to newtons
units.slug2kg	= 14.5939029;												% slugs to kg

%% Simple environment with square cell decomposition
    
fprintf('Loading environment map ... \t')
tic

nRow= 15;
nCol= 25;

nomCellSize = 1;															% base cell size, m
VCell	= [];
for cY = 1:2 
	for cX = 1:nCol
		VCell = cat(1, VCell, [cX-1 cY-1 1 1]*nomCellSize);
	end
end
VCell	= cat(1, VCell, [0 2 4 4]*nomCellSize);

for cY = 3:6 
	for cX = 5:nCol
		VCell = cat(1, VCell, [cX-1 cY-1 1 1]*nomCellSize);
	end
end

for cY = 7:nRow 
	for cX = 1:nCol
		VCell = cat(1, VCell, [cX-1 cY-1 1 1]*nomCellSize);
	end
end

nVertsCD = size(VCell, 1);

%----- Construct transition cost matrix
GCell = findAdj04(VCell);

targetCells = 51;
baseCells	= 32; %[1 9 34];
% threatCells	= [3];%[41:47 51:57 61:67 75:81];
tmp = kron((140:nCol:265)', ones(1,7)) + kron(1:7, ones(6,1));
noFlyCells	= [6 31 53 54:59 tmp(:)' 95:101];

% obstacles	= [threatCells noFlyCells]';
obstacles = noFlyCells';
for nObs = 1:size(obstacles, 1)
	nhbrs = find(GCell(obstacles(nObs), :));
	for m = nhbrs
		GCell(m, obstacles(nObs)) = 0;
		GCell(obstacles(nObs), m) = 0;
	end
end
toc


%% Draw environment
fprintf('Drawing environment map ... \t')
tic
targetColor = [0.8 0.15 0.15];
baseColor	= [0 0.4 0.25];
threatColor = [0.25 0.25 0.2];
noFlyColor	= [0.25 0.25 0.2]; %[0 0.25 0.4];
txtSize		= 0;

figure('Units', 'normalized', 'Position', [0.05 0.05 0.85 0.85], 'WindowStyle', 'docked');
envAxes = axes; axis equal; hold on;
% figure; envAxes = axes; axis equal; hold on;

drawCdV07(VCell, envAxes, 'k', 1, txtSize)
% drawCdV07(VCell, envAxes, 'k', 1, txtSize, threatCells, threatColor)
drawCdV07(VCell, envAxes, 'k', 1, txtSize, noFlyCells, noFlyColor)
drawCdV07(VCell, envAxes, 'k', 1, txtSize, baseCells, baseColor)
drawCdV07(VCell, envAxes, 'k', 2, txtSize, targetCells, targetColor)

% commandwindow;
missionData.plots.envAxes	= envAxes;
toc

%return

%% Video
% missionData.plots.writerObject	= VideoWriter('damagedSearch2.avi');
% missionData.plots.writerObject.Quality = 100;
% missionData.plots.writerObject.FrameRate = 1;
% 
% thisFrame = getframe(gcf);

% open(missionData.plots.writerObject);
% writeVideo(missionData.plots.writerObject, thisFrame);
missionData.plots.visitedColors	= [ones(10, 1) linspace(1, 0.7, 10)' zeros(10, 1)];

% commandwindow;
% toc



%% Set mission data
missionData.GCell			= GCell;
missionData.VCell			= VCell;
missionData.vertS			= 32;
missionData.vertG			= 51;
missionData.nomCellSize		= nomCellSize;
missionData.maxHCellSize	= Inf;
missionData.nFreeCells		= size(GCell, 1) - numel(obstacles);

missionData.constants.UNITS	= units;
missionData.constants.TOL	= 1e-6;
missionData.constants.INFTY = 1e15;
missionData.constants.BDTOL	= 0.001;

missionData.tilePlanner		= @cbtaFeasTPV01;
missionData.zta0			= [ [9; 1.1]*missionData.nomCellSize; 0];		% initial discrete "state"

%----- Manhattan heuristic
mnhtHeur = zeros(size(GCell,1), 1);
vertData = aStarV04(GCell, missionData.vertG, 1:nVertsCD, zeros(nVertsCD, 1));
for m1 = 1:nVertsCD
	mnhtHeur(m1) = vertData(m1).d;
end
missionData.heuristic		= 1.5*mnhtHeur;

%----- Associate transitions with transformations
missionData.geometry.faceRef = ...
	  [	 1	-1	1	0	0;
		-2	 2	1	2	0;
		 2	-2	1	1	0;
		-1	 1	1	4	0;
		 1	-2	2	0	0;
		-2	-1	2	2	0;
		 2	 1	2	1	0;
		-1	-2	2	4	0;
		 1	 2	2	3	0;
		 2	-1	2	4	2;
		-2	 1	2	4	1;
		-1	 2	2	4	3];

%----- Describe how vertices are permuted
missionData.geometry.vPermut	= ...
		[	1	2	3	4;													% Row 1 = Base: no transformation
			4	1	2	3;													% Row 2 = Rotate -90
			2	3	4	1;													% Row 3	= Rotate 90
			4	3	2	1;													% Row 4 = Flip horizontal
			2	1	4	3];													% Row 5 = Flip vertical

%----- Describe inverse transforms
missionData.geometry.invTf	= [0; 2; 1; 3; 4];								% Inverse transformations in the same order as above rows

%----- Describe how cell faces are permuted
missionData.geometry.tfRef	= ...
		[	1	2	3	4	5	6	7	8	9	10	11	12;					% Col 1 = Base: no transformation
			3	1	4	2	7	5	12	11	10	8	9	6;					% Col 2 = Rotate -90
			2	4	1	3	6	12	5	10	11	9	8	7;					% Col 3 = Rotate 90
			1	3	2	4	9	10	11	12	5	6	7	8;					% Col 4 = Flip horizontal
			4	2	3	1	8	11	10	5	12	7	6	9]';				% Col 5 = Flip vertical
		
% missionData.tiles.nGridPts_w= 31;
% missionData.tiles.nGridPts_a= 101;
missionData.tiles.nGridPts_w= 41;
missionData.tiles.nGridPts_a= 121;
missionData.tiles.dStateSp_w= linspace(0, 1, missionData.tiles.nGridPts_w);
missionData.tiles.dStateSp_a= linspace(-pi/2, pi/2, missionData.tiles.nGridPts_a);
% missionData.tiles.RCrvSp	= [linspace(1.5, 7.5, 13) linspace(7.5, 15, 7)];
missionData.tiles.RCrvSp	= [linspace(1.5, 7.5, 61) linspace(7.5, 15, 16)];
missionData.tiles.waGrid	= zeros(missionData.tiles.nGridPts_w*...
	missionData.tiles.nGridPts_a, 2);
nGridPts= 0;
for m1 = 1:missionData.tiles.nGridPts_w
	for m2 = 1:missionData.tiles.nGridPts_a
		nGridPts = nGridPts + 1;
		missionData.tiles.waGrid(nGridPts, :) = ...
			[missionData.tiles.dStateSp_w(m1), missionData.tiles.dStateSp_a(m2)];
	end
end

%% Dummy structural capability
missionData.sc.mean		= 2.25;
missionData.sc.stddev	= 0.05;
sc		= missionData.sc;

missionData.tiles.pfeasSp = zeros(1, numel(missionData.tiles.RCrvSp));
for m2 = 1:numel(missionData.tiles.RCrvSp)
	missionData.tiles.pfeasSp(m2) = 0.5 + 0.5*erf( (missionData.tiles.RCrvSp(m2) ...
		- sc.mean) / (sqrt(2)*sc.stddev) );									% specific formula for normal distribution
end

%% H-Cost planner parameters
H		= 2;
nHMem	= 10;

missionData.hcost.H		= H;
missionData.hcost.nHMem = nHMem;
missionData.pfThreshold = 0.9;

%% Pre-process CBTA

% tic
% fprintf('Pre-processing tiles ... \t\t')
% CBRAResults	= CBRA;
% toc
% 
% tic
% fprintf('Saving pre-processed data ... \t')
% save CBRAResults_41x121x77.mat CBRAResults
% toc
%return

tic
% load CBRAResults_31x101x20.mat CBRAResults
load CBRAResults_41x121x77.mat CBRAResults
% load CBRAResults_51x201x15.mat CBRAResults
toc
missionData.hcost.H = 1;
missionData.tiles.CBRAResults = CBRAResults;

%% Test TP
% vertS	= 12;
% allHist	= getAllHist(GCell, vertS, H+2, 0, vertS, []);
% disp(allHist)
% 
% tmp1= size(allHist, 1);
% tmp2= min((round(rand*tmp1) + 1), tmp1);
% tmp2= 5;
% 
% sgmaInit	= zeros(missionData.tiles.nGridPts_w*missionData.tiles.nGridPts_a, 1);
% % sgmaInit( (floor(missionData.tiles.nGridPts_w/2))*missionData.tiles.nGridPts_a + ...
% % 	floor(missionData.tiles.nGridPts_a/2) + 1 ) = 1;
% 
% 
% sgmaInit( ((floor(missionData.tiles.nGridPts_w/2) - 3)*missionData.tiles.nGridPts_a + 1) : ...
% 	( (floor(missionData.tiles.nGridPts_w/2) - 3)*missionData.tiles.nGridPts_a ...
% 	+ floor(missionData.tiles.nGridPts_a/2) + 1 ) ) = 1;
% sgmaInit( ((floor(missionData.tiles.nGridPts_w/2) - 2)*missionData.tiles.nGridPts_a + 1) : ...
% 	( (floor(missionData.tiles.nGridPts_w/2) - 2)*missionData.tiles.nGridPts_a ...
% 	+ floor(missionData.tiles.nGridPts_a/2) + 1 ) ) = 1;
% sgmaInit( ((floor(missionData.tiles.nGridPts_w/2) - 1)*missionData.tiles.nGridPts_a + 1) : ...
% 	( (floor(missionData.tiles.nGridPts_w/2) - 1)*missionData.tiles.nGridPts_a ...
% 	+ floor(missionData.tiles.nGridPts_a/2) + 1 ) ) = 1;
% sgmaInit( ((floor(missionData.tiles.nGridPts_w/2) - 0)*missionData.tiles.nGridPts_a + 1) : ...
% 	( (floor(missionData.tiles.nGridPts_w/2) - 0)*missionData.tiles.nGridPts_a ...
% 	+ floor(missionData.tiles.nGridPts_a/2) + 1 ) ) = 1;
% 
% 
% % disp(missionData.tiles.waGrid(sgmaInit > 0, :))
% % find(sgmaInit > 0)
% tic
% tpResult = cbtaFeasTPV01(allHist(tmp2, :), sgmaInit);
% toc
% 
% zta0 = [ [12; 0.7]*missionData.nomCellSize; 45*pi/180];
% % tpResult = cbtaFeasTPV01(allHist(tmp2, :), [], zta0);
% 
% return

%% Search!
% fprintf('\nSearching ... \n')
  
tic
%Use the distance as the heurstic
dist_goal = Find_dist(missionData.VCell, missionData.vertG);
%Apply A*
astar_search = aStarV04(GCell, missionData.vertS, missionData.vertG, dist_goal);
%Find the inital path from the A* result
first_search = find_path([astar_search.b], missionData.vertG, missionData.vertS);

%----- Display initial resultant path
if ~isempty(first_search)
	drawCdV07(VCell, envAxes, 'k', 2, 7, first_search(2:end-1), [1 0.7 0]);
% 	drawCdV07(VCell, envAxes, 'k', 0.5, 7, searchResult.optPathVerts(2:end-1), [1 0.7 0]);
% 	searchResult.optPath.pfeas	
% 	drawCdV07(VCell, envAxes, 'r', 2, txtSize, ...
% 		searchResult.optPathVerts(2:end-1), 'w');
	
end









% fprintf('\nSearching ... \n')

% tic
% searchResult = HCostPlan;
% toc

%----- Display resultant path
% if ~isempty(searchResult)
% 	drawCdV07(VCell, envAxes, 'k', 2, 7, searchResult.optPathVerts(2:end-1), [1 0.7 0]);
% 	
% 	searchResult.optPath.pfeas	
% % 	drawCdV07(VCell, envAxes, 'r', 2, txtSize, ...
% % 		searchResult.optPathVerts(2:end-1), 'w');
% 	
% end



missionData.hcost.H = missionData.hcost.H + 1;
init_loc = [7; 1.1];
init_angle = 0;
%Start repair the path iteratly
path_new = PathRepair(first_search, 6, init_loc, init_angle);


[~, size_path] = size(path_new);

%optimal process

missionData.zta0 = [ [9; 1.1]*missionData.nomCellSize; 0];

% num_opt_node = size_path - 1;

num_opt_node = 10;
opt_node = path_new(num_opt_node);

path_head = path_new(1:num_opt_node);
path_tail = path_new(num_opt_node + 1:end);

head_tpResult = cbtaFeasTPV01(path_head, [], 0.9, missionData.zta0);

init_st = head_tpResult.sgmaF;

searchResult_opt_path = HCostPlan_repair_2(path_tail, init_st);

opt_path = searchResult_opt_path.optPathVerts;

% path_new = update_path(path_new, opt_path);











toc




% %% 
% clear all; clc
% 
% thta = -10 + 20*rand(2,1);
% 
% tic
% a = pi2pi(thta);
% toc
% 
% tic
% b = [pi2pi(thta(1)); pi2pi(thta(2))];
% toc
% 
% disp( (a - b)')







