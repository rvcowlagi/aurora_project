%{
Raghvendra V. Cowlagi
Aurora Flight Sciences RDC, Cambridge, MA.

Jan 20, 2013

- Modification of multi-resolution H-cost search for completely decoupled
planning with probabilistic guarantees on feasibility
- Discrete "states" attached to vertices, instead of vehicle states as
before, labeled by scalars
%}

function searchResult = HCostPlan_repair_2(path_tail, init_st)
global missionData


flag = 0;

%----- Planner parameters
H		= missionData.hcost.H;
nHMem	= missionData.hcost.nHMem;
%{
	H		: length of history
	nHMem	: number of histories to be remembered per vert,
			Inf => remember everything
%}

%----- Problem Data -------
G		= missionData.GCell;												% transition cost matrix of c.d. graph
VCell	= missionData.VCell;												% locations and dimensions of cells
heur	= missionData.heuristic;											% heuristic
vertS	= missionData.vertS;												% start vertex in c.d. graph
vertG	= missionData.vertG;												% goal vertex in c.d. graph
dMax	= missionData.maxHCellSize;											% largest cell size to consider for history, Inf => all
nStates = (missionData.tiles.nGridPts_w)*(missionData.tiles.nGridPts_a);	% number of discrete "states"
initSt	= missionData.zta0;	% initial (search) state
%init_st = missionData.zta0;
tpHandle= missionData.tilePlanner;											% handle to tile planning function
% Format: [newCost, newPFeas] = tilePlannerName(vertSeq, stAct)

%********* THIS SHOULDN'T BE ARBITRARY ***********%
pThres	= missionData.pfThreshold;

%----- Variables Initialization -------
N		= size(G, 1);														% Number of verts in original graph
nOpen	= 0;
openL	= [];																% Sorted OPEN lists

nHSeries= [4 12 36 100 284 780 2172 5916 16268 44100]';
nHWorst	= min(nHMem, nHSeries(H));

% ipStr	= struct('primSeq', [], 'primPar', [], 'coastTimes', [], ...
% 	'traj', [], 'trajPt', []);
% mainStr	= struct('hist', [], 'hLen', [], 'd', [], 'h', [], 'mk', [], ...
% 	'nHX', [], 'st', [], 'ipSeq', (repmat(ipStr, 1, nHWorst)), ...
% 	'ot', [], 'ohst', [], 'ohL', []);
mainStr	= struct('hist', [], 'hLen', [], 'd', [], 'h', [], 'mk', [], ...
    'nHX', [], 'st', [], 'ot', [], 'ohst', [], 'ohL', [], 'pf', []);
vertData= repmat(mainStr, 1, N);

for m1 = 1:(N+1)
    vertData(m1).hist	= zeros(nHWorst, H+1);								% List of histories known so far
    vertData(m1).hLen	= zeros(nHWorst, 1);								% Length of each history, needed for "multi-history"
    vertData(m1).d		= Inf(nHWorst, 1);								% Label, one per history
    vertData(m1).pf		= zeros(nHWorst, 1);								% P-feas, one per history
    vertData(m1).h		= zeros(nHWorst, 1);								% 'Tail', one per history
    vertData(m1).mk		= zeros(nHWorst, 1);								% Marker, 0 = NEW; 1 = OPEN; 2 = CLOSED
    vertData(m1).nHX	= 0;												% Number of histories known so far (X for explored)
    vertData(m1).st		= zeros(nStates, nHWorst);							% Possible discrete states, one set per history
    vertData(m1).ot		= zeros(nHWorst, 1);								% Original 'Tail', one per history
    vertData(m1).ohst	= zeros(nHWorst, H+1);								% List of original histories known so far
    vertData(m1).ohL	= zeros(nHWorst, 1);								% Original length of each history, needed for "multi-history"
end

%----- Algorithm Initialization -------
vertSHist = sizeConstrHistory(H+1, vertS, []);
% for L = H:-1:1
% 	allnSHist	= sizeConstrHistory(L, vertS, vertSHist);					% Get "single" histories of start vert of length L+1
% 	if numel(allnSHist)
% 		vertSHist	= cat(1, vertSHist, [allnSHist zeros(size(allnSHist, 1), H+1-L)]);
% 	end
% end

vertSnhbrs = find(G(vertS, :));
disp(initSt')

relvSNhbr = 0;
for m = vertSnhbrs
    if max(abs(initSt(1:2, :) - (VCell(m, 1:2) + VCell(m, 3)/2)')) ...
            <= (0.5 + missionData.constants.BDTOL)*missionData.nomCellSize
        relvSNhbr = m;
        break;
    end
end
disp(relvSNhbr)


if relvSNhbr
    vertSHist = vertSHist(vertSHist(:, 2) == relvSNhbr, :);
else
    searchResult = [];
    
    fprintf('Search failed, incompatible initial state and start vertex.\n');
    return
end

disp(vertSHist)

% searchResult = [];
% return

vertExp = [];
% initWtbar = waitbar(0, 'Initializing search data structure');
for m1 = 1:size(vertSHist, 1)
    tHistLen	= sum(vertSHist(m1, :) > 0);								% In next line, tHistLen replaces (H+2) from standard history search
    tVert		= vertSHist(m1, tHistLen);									% Last vert in H+2 history, will go to OPEN
    
    tpResult	= tpHandle(vertSHist(m1,1:(tHistLen)), init_st, pThres);			% Cost of this history
    % 	waitbar(m1/size(vertSHist,1), initWtbar);
    
    tHistCost	= tpResult.cost;
    tHistSt		= tpResult.sgmaF;
    tHistPf		= tpResult.pfeas;
    
    tnHX		= vertData(tVert).nHX;
    if tnHX < nHMem
        tnHX				= tnHX + 1;
        vertData(tVert).nHX	= tnHX;
        tIndx				= tnHX;
    else																	% What to do when enough histories are known
        [worstCost, tIndx]	= max(vertData(tVert).d(1:tnHX));
        if tHistCost >= worstCost, continue; end							% If this history is worse than all known ones, ignore it
        
        % If not, remove the worst known history from OPEN and replace it
        % with this one
        [~, loc]		= ismember([tVert tIndx], openL(1:nOpen, 1:2), 'rows');
        openL(loc, :)	= [];
        nOpen			= nOpen - 1;
    end
    
    updateVertData(tVert, tIndx, (vertSHist(m1, 2:tHistLen)), vertS, ...
        (tHistLen - 1), tHistCost, tHistSt, tHistPf, 1, 1);
    
    vertExp = cat(1, vertExp, tVert);
    openL	= cat(1, openL, [tVert tIndx (tHistCost + heur(tVert))]);		% Add to OPEN list
    nOpen	= nOpen + 1;
end


openL(1:nOpen, :) = sortrows(openL(1:nOpen, :), 3);
% close(initWtbar);
drawnow();

%----- Algorithm Iterative Steps -------
% mainWtBar = waitbar(0, 'H-Cost search in progress');
nIter	= 0; nStExp = nOpen; nHExp = nOpen;
goalClosed = 0;

while (nOpen > 0) && (~goalClosed) && (openL(1, 3) < Inf)
    nIter	= nIter + 1;
    
    fprintf('******************** Iteration %i\n', nIter);
    
    vertAct	= openL(1, 1);		histAct	= openL(1, 2);
    
    % 	if (vertAct == (N+1)), goalClosed = 1; continue; end					% Goal is a dummy vert
    if (vertAct == vertG)
        goalClosed = 1;
        % 		waitbar(1, mainWtBar);
        continue;
    end
    
    fprintf('*** vertAct = %i\n', vertAct);
    disp(vertData(vertAct).hist(histAct, :))
    
    nhbrs		= find(G(vertAct, :));										% Note all neighbors of vertAct
    newOpen		= [];
    
    newHistLen	= vertData(vertAct).hLen(histAct, 1);
    newTail		= vertData(vertAct).hist(histAct, 1);						% Possible tail for vertNew
    actCost		= vertData(vertAct).d(histAct);								% Cost to come to (vertAct, histAct)
    actPf		= vertData(vertAct).pf(histAct);
    actState	= vertData(vertAct).st(:, histAct);
    % 	disp([actState(1:2)'/missionData.nomCellSize actState(3:5)'])
    
    %{
	if (VCell(vertAct,3) >= dMax) && (newHistLen == 2)						% Adjacent to the goal
% 		fprintf('Almost done... \n')
		[isKnownBd, locBd] = ismember(vertAct, mrBdVerts);
		
% 		if isKnownBd
% 			goalCost = (actCost + bdData(locBd).cost);
% 		else
% 			fprintf('***** WARNING: Oversize cell of vert (%i) with no boundary cost associated. *****\n', vertAct);
% 			goalCost = Inf;
% 		end
		
		if vertData(N+1).mk == 0											% Goal not already in OPEN
			newOpen	= cat(1, newOpen, [(N+1) 0 goalCost]);					% Add goal to OPEN, with known terminal cost
			vertData(N+1).mk			= 1;
			vertData(N+1).hist(1, 1:2)	= [vertAct histAct];
			vertData(N+1).d				= goalCost;
		elseif goalCost < vertData(N+1).d									% If c2come to goal can be improved
			[~, locGoal]		= ismember((N+1), openL(1:nOpen, 1));
			nOpen				= nOpen - 1;
			openL(locGoal, :)	= [];
			
			newOpen	= cat(1, newOpen, [(N+1) 0 goalCost]);
			vertData(N+1).hist(1, 1:2)	= [vertAct histAct];
			vertData(N+1).d				= goalCost;
		end
		
		openL(1, :)	= [];
		nOpen		= nOpen - 1;
		
		tmpOpen = binSort(openL(1:nOpen, :), newOpen, 3);					% Add newly opened pairs to sorted open list
		if numel(tmpOpen) == 0
			nOpen	= 0;
			openL	= [];
		else
			nOpen	= size(tmpOpen, 1);
			openL(1:nOpen, :)	= tmpOpen;
		end
		continue;
	end
    %}
    newHistAct	= vertData(vertAct).hist(histAct, 2:newHistLen);			% History corresponding to (vertAct, histAct), newHistLen replaces (H+1) here
    
    %{
	%**** FIX THIS PART WHEN USING MULTIRESOLUTION SEARCH ****
	if (VCell(vertAct,3) >= dMax)											% Technically not adjacent to the goal, do not explore further neighbors
		newHIndx= histAct;
		vertNew	= vertAct;
		
		tpResult	= tpHandle([newTail newHistAct], actState);
		tHistCost	= tpResult.cost;
		tHistPf		= tpResult.pfeas;
		newSt		= tpResult.ztaF;
% 		newIp		= tpResult.input;
		newCost		= actCost + tHistCost;
		newPf		= actPf*tHistPf;
		
		updateVertData(vertAct, histAct, newHistAct, newTail, ...
			(newHistLen-1), newCost, newSt, ...
			[vertData(vertNew).ipSeq(newHIndx).ip newIp], 1, 0);
		
		
		newOpen	= cat(1, newOpen, ...
			[vertNew newHIndx (newCost + heur(vertNew))]);					% Set of newly opened (vert, hist) pairs
		nhbrs	= [];
	end
    %}
    
    % ---- Explore each neighbor
    for vertNew = nhbrs
        fprintf('\t--- vertNew = %i\n', vertNew);
        
        if vertNew == vertData(vertAct).h(histAct), continue; end
        if vertNew == newTail, continue; end
        if any(vertNew == newHistAct), continue; end
        if any(G(vertNew, newHistAct(1:(end-1)))), continue; end
        %{
			First (H-1) elements of each row of newHistAct are "indices" of
			histories of vertNew through vertAct unique up to all but one
			element (tail of one vert) of histories of vertAct. This cost
			for vertNew through vertAct for each of these "indices" is to
			be minimized over that tail element.
        %}
        
        nHXNew		= vertData(vertNew).nHX;
        [isKnown, loc]= ismember(newHistAct,  ...
            vertData(vertNew).hist(1:nHXNew, 1:(newHistLen-1)), 'rows');
        
        if isKnown
            newHIndx = loc;
            
            if (vertData(vertNew).mk(newHIndx) == 2), continue; end
            % If a history is already CLOSED, it cannot be improved. Comes
            % from the proposition that says every pair enters open list
            % only once.
        else
            if nHXNew < nHMem
                newHIndx				= nHXNew + 1;
                vertData(vertNew).nHX	= newHIndx;
            else
                vertNewOpenHist			= find(vertData(vertNew).mk(1:nHXNew) == 1);
                if numel(vertNewOpenHist) == 0
                    continue;
                else
                    [worstNewCost, newHIndx]= max(vertData(vertNew).d(vertNewOpenHist));
                    newHIndx				= vertNewOpenHist(newHIndx);
                    if actCost >= worstNewCost, continue; end				% new history of vertNew cannot improve cost over its known ones
                end
            end
        end
        
        tpResult	= tpHandle([newTail newHistAct vertNew], actState, pThres);
        tHistCost	= tpResult.cost;
        tHistPf		= tpResult.pfeas;
        newSt		= tpResult.sgmaF;
        newCost		= actCost + tHistCost;
        newPf		= min(actPf, tHistPf); %actPf*tHistPf
        
        
        
        if (newPf < missionData.pfThreshold)
            continue;
        end
        
        if ~isKnown
            updateVertData(vertNew, newHIndx, [newHistAct vertNew], ...
                newTail, newHistLen, newCost, newSt, newPf, 1, 1);
            
            newOpen	= cat(1, newOpen, ...
                [vertNew newHIndx (newCost + heur(vertNew))]);				% Set of newly opened (vert, hist) pairs
            nStExp	= nStExp + 1;
            nHExp	= nHExp + 1;
            
        elseif (vertData(vertNew).d(newHIndx) > newCost)
            [~, t2]	= ismember([vertNew newHIndx], openL(1:nOpen, 1:2), 'rows');
            nOpen	= nOpen - 1;
            openL(t2, :)= [];
            
            updateVertData(vertNew, newHIndx, [newHistAct vertNew], ...
                newTail, newHistLen, newCost, newSt, newPf, 1, 1);
            
            newOpen	= cat(1, newOpen, ...
                [vertNew newHIndx (newCost + heur(vertNew))]);				% Set of newly opened (vert, hist) pairs
            nHExp	= nHExp + 1;
        end
        % Intentionally split these two ifs: program executes faster
        
        if any(path_tail==vertNew)
            
             disp('##############################')
%             disp(vertNew)
            flag = 1;
            vertG = vertNew;
            break;
        end
    end
    
    
    
    
    
    vertExp = cat(1, vertExp, vertAct);
    
    
    
    
    drawCdV05(VCell, missionData.plots.envAxes, 'k', 1, 7, vertAct, ...
        missionData.plots.visitedColors(vertData(vertAct).nHX, :))
    % 	thisFrame = getframe(gcf);
    % % 	open(missionData.plots.writerObject);
    % 	writeVideo(missionData.plots.writerObject, thisFrame);
    
    vertData(vertAct).mk(histAct) = 2;										% Mark this pair closed
    openL(1, :)	= [];														% Remove (vertAct, histAct) from OPEN
    nOpen		= nOpen - 1;
    
    tmpOpen = binSort(openL(1:nOpen, :), newOpen, 3);						% Add newly opened pairs to sorted open list
    if numel(tmpOpen) == 0
        nOpen	= 0;
        openL	= [];
    else
        nOpen	= size(tmpOpen, 1);
        openL(1:nOpen, :)	= tmpOpen;
    end
    
    
    % 	waitbar(min( (nHExp / (nHWorst*missionData.nFreeCells) ), 1), ...
    % 		mainWtBar);
    
    if flag == 1
%         disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        break;
    end
end

%----- Trace Optimal Path -------
[optCost, optHist]	= min(vertData(vertG).d((vertData(vertG).mk ~= 0)));
tmp = find( (vertData(vertG).mk ~= 0) );
optHist	= tmp(optHist);

% if (numel(optCost) == 0) || (optCost == Inf)
% 	fprintf('No path found.\n');
% 	searchResult = [];
% 	return;
% end

tail	= vertData(vertG).ot(optHist);
head	= vertG;
hIndx	= optHist;
hLen	= vertData(vertG).ohL(optHist);
optPath.verts	= vertData(vertG).ohst(hIndx, 1:hLen);
% optPath.ip		= ipStr;
optPath.indx	= [];
optPath.hLen	= [];
while tail ~= vertS
    
%     disp('%%%%%%%%%%%!!!trace!!!%%%%%%%%%%%')
    tail	= vertData(head).ot(hIndx);										% Get tail
    nextH	= [tail vertData(head).ohst(hIndx, 1:(hLen-1))];
    
    optPath.verts	= [tail optPath.verts];
    optPath.indx	= [hIndx optPath.indx];
    optPath.hLen	= [hLen optPath.hLen];
    
    head	= vertData(head).ohst(hIndx, (hLen-1));							% Step back one
    
    [~, hIndx]	= ismember(nextH, vertData(head).ohst, 'rows');				% The next index is the one that corresponds to the history
    % formed by tail and first H verts of current history
    
    if hIndx
        hLen		= vertData(head).ohL(hIndx);
    end
end

% close(mainWtBar);
% drawnow();

searchResult.performance.nIter	= nIter;
searchResult.performance.nStExp = nStExp;
searchResult.performance.nHExp	= nHExp;
searchResult.optPathVerts		= optPath.verts;
searchResult.vertData			= vertData;
searchResult.optPath.HIndices	= optPath.indx;
searchResult.optPath.HLengths	= optPath.hLen;
searchResult.optPath.pfeas		= vertData(vertG).pf(optHist);

%**********************************************************************
    function UL = sizeConstrHistory(L, vertV, UH)
        
        % Specialized for finding histories of a single vert
        
        UL	= [];
        lnV = getAllHistV04(G, vertV, L+1, 0, vertV, []);
        if ~numel(lnV), return; end
        
        for n = 1:L
            lnV((VCell(lnV(:,n),3) >= dMax), :) = [];						% Remove verts corresponding to big cells
            if ~numel(lnV), break; end
        end
        if ~numel(lnV), return; end
        
        % 		for n = 1:L															% Remove big-to-small transitions
        % 			dDiff = VCell(lnV(:, n+1), 3) - VCell(lnV(:, n), 3);
        % 			lnV((dDiff < 0), :) = [];
        % 			if ~numel(lnV), break; end
        % 		end
        % 		if ~numel(lnV), return; end
        
        if numel(UH)
            projRows		= ismember(lnV, UH(:, 1:L+1), 'rows');			% Remove projections
            lnV(projRows,:)	= [];
        end
        
        UL = lnV;
    end
%----------------------------------------------------------------------
%**********************************************************************
    function updateVertData(vertNew, newHIndx, newHist, newTail, ...
            newHistLen, newCost, newSt, newPf, newMk, chOrg)
        
        vertData(vertNew).hist(newHIndx, 1:(H+1))	= zeros(1, H+1);
        vertData(vertNew).hist(newHIndx, (1:newHistLen))= newHist;
        vertData(vertNew).h(newHIndx)				= newTail;
        vertData(vertNew).hLen(newHIndx, :)			= newHistLen;
        vertData(vertNew).d(newHIndx)				= newCost;
        vertData(vertNew).pf(newHIndx)				= newPf;
        vertData(vertNew).st(:, newHIndx)			= newSt;
        vertData(vertNew).mk(newHIndx)				= newMk;
        
        if chOrg
            vertData(vertNew).ohL(newHIndx)			= newHistLen;
            vertData(vertNew).ohst(newHIndx, 1:newHistLen) = newHist;
            vertData(vertNew).ot(newHIndx)			= newTail;
        end
    end
%----------------------------------------------------------------------

end
