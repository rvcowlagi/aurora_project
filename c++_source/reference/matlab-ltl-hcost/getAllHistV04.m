% *************************************************************************
% Author:		Raghvendra V. Cowlagi
%				Dept. of Aerospace Engineering, Georgia Inst. Tech.
% Description:	Compute all H-length histories of cells from a given cell
% Last Mod.:	10/18/2010, 17:00 hrs
% *************************************************************************

function allHist = getAllHistV04(G, actNode, H, seqPos, rawSeq1, allHist)
% getAllHist returns a list of all H-length traversals from a given node in
% a given graph. The program makes recursive calls to itself, traversing
% one step forward in each call. Each history returned is of length H and
% includes the given starting node.
%
% Description of input variables
% ------------------------------
% G			: adjacency matrix of cells
% actNode	: active node from which all sequences start
% H			: fixed length of sequences to be found
% rawSeq1	: input for propagating histories generated thus far to
%			  further recursive calls
% seqList	: list of fully formed histories known at all levels of recursion

actNhbrs	= find(G(:, actNode));											% Step forward once

for count1 = 1:size(actNhbrs, 1)											% For all neighbors
	newNode	= actNhbrs(count1);
	if seqPos == H - 2
		temp = [rawSeq1 newNode];
		if any(rawSeq1 == newNode), continue; end
		if any(G(newNode, rawSeq1(1:(end-1)))), continue; end
		allHist = cat(1, allHist, temp);		
	else
		rawSeq2	= cat(2, rawSeq1, newNode);
		if any(rawSeq1 == newNode), continue; end
		if any(G(newNode, rawSeq1(1:(end-1)))), continue; end
% 		if size(unique(rawSeq2), 2) < size(rawSeq2, 2)						% If history large enough
% 			continue; 
% 		else																% Recursive call
			allHist	= getAllHistV04(G, newNode, H, seqPos + 1, rawSeq2, allHist);
% 		end
	end
end