%{
Copyright (c) 2014 Raghvendra V. Cowlagi. All rights reserved.

Copyright notice:
=================
No part of this work may be reproduced without the written permission of
the copyright holder, except for non-profit and educational purposes under
the provisions of Title 17, USC Section 107 of the United States Copyright
Act of 1976. Reproduction of this work for commercial use is a violation of
copyright.


Disclaimer:
===========
This software program is intended for educational and research purposes.
The author and the institution with which the author is affiliated are not
liable for damages resulting the application of this program, or any
section thereof, which may be attributed to any errors that may exist in
this program.


Author information:
===================
Raghvendra V. Cowlagi, Ph.D,
Assistant Professor, Aerospace Engineering Program,
Department of Mechanical Engineering, Worcester Polytechnic Institute.
 
Higgins Laboratories, 247,
100 Institute Road, Worcester, MA 01609.
Phone: +1-508-831-6405
Email: rvcowlagi@wpi.edu
Website: http://www.wpi.edu/~rvcowlagi


The author welcomes questions, comments, suggestions for improvements, and
reports of errors in this program.


Program description:
====================
Geometric data about a tile (sequence of cells)
%}

function tile_data = get_tile_data(H, tile_vertices)
%{
	'verts' for vertices in the graph
	'vertices' for geometric points
%}


%----- Associate transitions with transformations
%{
	Base opposite is left -> right
	Base adjacent is left -> down
	Face numbers: (1, -1, 2, -2) = (left, right, up, down)

	This is the mnemonic here in each row.
	Columns 1&2 : [from_face to_face]
	Column 3	: traversal type (1 for opposite, 2 for adjacent)
	Columns 4&5 : transformations to get to the "base"

	Transformation reference.
	0: Base
	1: Rotate -90
	2: Rotate 90
	3: Flip horizontal
	4: Flip vertical
%}
FACE_REF = [...
	1	-1	1	0	0;
    -2	 2	1	2	0;
	2	-2	1	1	0;
    -1	 1	1	4	0;
    1	-2	2	0	0;
    -2	-1	2	2	0;
    2	 1	2	1	0;
    -1	-2	2	4	0;
    1	 2	2	3	0;
    2	-1	2	4	2;
    -2	 1	2	4	1;
    -1	 2	2	4	3];

%----- Describe how vertices are permuted
VERTICES_PERMUTATION	= [...
    1	2	3	4;															% Row 1 = Base: no transformation
    4	1	2	3;															% Row 2 = Rotate -90
    2	3	4	1;															% Row 3	= Rotate 90
    4	3	2	1;															% Row 4 = Flip horizontal
    2	1	4	3];															% Row 5 = Flip vertical

%----- Describe inverse transforms
INVERSE_XFORM	= [0; 2; 1; 3; 4];											% Inverse transformations in the same order as above rows (-1 to accout for the base row)

%{
%----- Describe how cell faces are permuted
problem_data.geometry.tfRef	= ...
    [	1	2	3	4	5	6	7	8	9	10	11	12;					% Col 1 = Base: no transformation
    3	1	4	2	7	5	12	11	10	8	9	6;					% Col 2 = Rotate -90
    2	4	1	3	6	12	5	10	11	9	8	7;					% Col 3 = Rotate 90
    1	3	2	4	9	10	11	12	5	6	7	8;					% Col 4 = Flip horizontal
    4	2	3	1	8	11	10	5	12	7	6	9]';				% Col 5 = Flip vertical
%}

%----- Locations of cells
I4	= eye(4);
traversal_type	= zeros(H, 1);												% Opposite (1) or adjacent (2) traversal
cell_xform		= zeros(H, 2);												% Transformations to bring to standard

chan_mid(:, 1:2)	= (diag(tile_vertices(2:(end-1), 3)))*ones(H, 2);		% Rectangle dimensions (dx, dy)
chan_mid(:, 3:4)	= tile_vertices(2:(end-1), 1:2);						% Rectangle center coordinates		

chan_full(:, 1:2)	= (diag(tile_vertices(:, 3)))*ones(H+2, 2);				% Full channel, needed only to define
chan_full(:, 3:4)	= tile_vertices(:, 1:2);								% entry and exit segments

%----- Entry segments
ul			= zeros(H, 1);	
exit_seg	= zeros(H+1, 1);
cell_edge	= zeros(H, 4);

for n = 2:(H+2)
	del_X = chan_full(n, 3) - chan_full(n-1, 3);	
	tol = 1e-6;

	if abs(abs(del_X) - sum(chan_full(n-1:n, 1))/2) < tol
		if chan_full(n, 3) > chan_full(n-1, 3)								% X transition (left or right)
			exit_seg(n-1)	= -1;											% Right
		else
			exit_seg(n-1)	= 1;											% Left
		end
		ul(n-1) = 0;
		x_lim	= chan_full(n-1, 3) - exit_seg(n-1)*chan_full(n-1, 1)/2;
		y_lim	= [min(chan_full(n, 4) + chan_full(n, 2)/2, chan_full(n-1, 4) + chan_full(n-1, 2)/2) ...
					max(chan_full(n, 4) - chan_full(n, 2)/2, chan_full(n-1, 4) - chan_full(n-1, 2)/2)];
		cell_edge(n-1, :) = [x_lim y_lim(1) x_lim y_lim(2)];
	else
		if chan_full(n, 4) > chan_full(n-1, 4)								% Y transition (up or down)
			exit_seg(n-1)	= 2;											% Up
		else
			exit_seg(n-1)	= -2;											% Down
		end
		ul(n-1) = 1;
		y_lim	= chan_full(n-1, 4) + (exit_seg(n-1)/2)*chan_full(n-1, 2)/2;
		x_lim	= [min(chan_full(n, 3) + chan_full(n, 1)/2, chan_full(n-1, 3) + chan_full(n-1, 1)/2) ...
					max(chan_full(n, 3) - chan_full(n, 1)/2, chan_full(n-1, 3) - chan_full(n-1, 1)/2)];
		cell_edge(n-1, :) = [x_lim(1) y_lim x_lim(2) y_lim];
	end	
end
face_from	= -exit_seg(1:H, 1);
face_to		= exit_seg(2:(H+1), 1);		

%----- Type of transition
cell_vertices = zeros(4, 2*H);												% Vertices of each rectangle, side by side
for n = 1:H
	this_cell_vertices(1,:) = [(chan_mid(n, 3) - chan_mid(n, 1)/2) (chan_mid(n, 4) + chan_mid(n, 2)/2)];	% Vertices of the rectangle
	this_cell_vertices(2,:) = [(chan_mid(n, 3) + chan_mid(n, 1)/2) (chan_mid(n, 4) + chan_mid(n, 2)/2)];	% in CW order starting
	this_cell_vertices(3,:) = [(chan_mid(n, 3) + chan_mid(n, 1)/2) (chan_mid(n, 4) - chan_mid(n, 2)/2)];	% with top-left
	this_cell_vertices(4,:) = [(chan_mid(n, 3) - chan_mid(n, 1)/2) (chan_mid(n, 4) - chan_mid(n, 2)/2)];

	[~, idx1]			= ismember([face_from(n) face_to(n)], FACE_REF(:, 1:2), 'rows');
	cell_xform(n, :)	= FACE_REF(idx1, 4:5);								% Existing transformation on current rectangle
	traversal_type(n)	= (ul(n) ~= ul(n+1)) + 1;

	xform1	= INVERSE_XFORM(FACE_REF(idx1, 5) + 1);
	xform2	= INVERSE_XFORM(FACE_REF(idx1, 4) + 1);							% Get inverse transforms
	p1		= VERTICES_PERMUTATION(xform1 + 1, :);		
	p2		= VERTICES_PERMUTATION(xform2 + 1, :);

	this_cell_vertices = I4(p2,:)*I4(p1,:)*this_cell_vertices;
	cell_vertices(:, (2*n - 1):(2*n)) = this_cell_vertices;
end

tile_data.channel_data		= chan_mid;
tile_data.cell_vertices		= cell_vertices;
tile_data.traversal_type	= traversal_type;
tile_data.cell_xform		= cell_xform;
tile_data.traversal_faces	= [face_from; face_to];
tile_data.cell_edge			= cell_edge;
