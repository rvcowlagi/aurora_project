function G = findAdj04(V)
% Calculate G for the first time

nV	= size(V, 1);
eRec= [];

for m1 = 1:nV
	for m2 = (m1 + 1):nV
		v1 = V(m1, 1:3);
		v2 = V(m2, 1:3);
		
		if v1(3) > v2(3)
			temp = v1;
			v1 = v2;
			v2 = temp;
		end

		indx = (v1(1:2) - v2(1:2))./v1(3);		
		if ((indx(1) <= v2(3)/v1(3)) && (indx(2) < v2(3)/v1(3))...
				&& (indx(1) >= -1) && (indx(2) > -1)) || ...
			((indx(2) <= v2(3)/v1(3)) && (indx(1) < v2(3)/v1(3))...
				&& (indx(2) >= -1) && (indx(1) > -1))						% 4-connectivity
			eRec = cat(1, eRec, [m1 m2 1; m2 m1 1]);
		end
		
	end
end
G = sparse(eRec(:,1), eRec(:,2), eRec(:,3), nV, nV);