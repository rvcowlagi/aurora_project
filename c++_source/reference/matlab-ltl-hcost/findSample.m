% *************************************************************************
% Author:		Raghvendra V. Cowlagi
%				Dept. of Aerospace Engineering, Georgia Inst. Tech.
% Description:	In a row of ascending numbers, find the index of a value
%				closest to a given value 
% Last Mod.:	08/11/2010
%**************************************************************************
function smp = findSample(ySmp, y)

if y < ySmp(1), smp = 1; return; end
if y > ySmp(end), smp = numel(ySmp); return; end

temp= (ySmp >= y);															% First sample no less than
smp	= find(temp, 1, 'first');
%--------------------------------------------------------------------------