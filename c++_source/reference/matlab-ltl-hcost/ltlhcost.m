%{
Copyright (c) 2014 Raghvendra V. Cowlagi. All rights reserved.

Copyright notice: 
=================
No part of this work may be reproduced without the written permission of
the copyright holder, except for non-profit and educational purposes under
the provisions of Title 17, USC Section 107 of the United States Copyright
Act of 1976. Reproduction of this work for commercial use is a violation of
copyright.


Disclaimer:
===========
This software program is intended for educational and research purposes.
The author and the institution with which the author is affiliated are not
liable for damages resulting the application of this program, or any
section thereof, which may be attributed to any errors that may exist in
this program.


Author information:
===================
Raghvendra V. Cowlagi, Ph.D,
Assistant Professor, Aerospace Engineering Program,
Department of Mechanical Engineering, Worcester Polytechnic Institute.
 
Higgins Laboratories, 247,
100 Institute Road, Worcester, MA 01609.
Phone: +1-508-831-6405
Email: rvcowlagi@wpi.edu
Website: http://www.wpi.edu/~rvcowlagi


The author welcomes questions, comments, suggestions for improvements, and
reports of errors in this program.


Program description:
====================
H-cost planning algorithm for satisfying LTL specifications.
%}

function search_result = ltlhcost(problem_data)

%----- Planner parameters
H		= problem_data.hcost.H;
n_HMem	= problem_data.hcost.nHMem;
%{
	H		: length of history
	nHMem	: number of histories to be remembered per vert,
			Inf => remember everything 
%}

%----- Problem Data -------
G		= problem_data.GCell;												% transition cost matrix of c.d. graph
VCell	= problem_data.VCell;												% locations and dimensions of cells
heur	= problem_data.heuristic;											% heuristic
vert_s	= problem_data.vertS;												% start vertex in c.d. graph
vert_g	= problem_data.vertG;												% goal vertex in c.d. graph
dMax	= problem_data.maxHCellSize;										% largest cell size to consider for history, Inf => all
n_states= (problem_data.tiles.nGridPts_w)*(problem_data.tiles.nGridPts_a);	% number of discrete "states"
st_init	= problem_data.zta0;												% initial (search) state
handle_tp= problem_data.tilePlanner;										% handle to tile planning function
	% Format: [newCost, newPFeas] = tilePlannerName(vertSeq, stAct) 


%----- Variables Initialization -------
N		= size(G, 1);														% Number of verts in original graph
nOpen	= 0;
openL	= [];																% Sorted OPEN lists

n_Hseries= [4 12 36 100 284 780 2172 5916 16268 44100]';
n_Hworst	= min(n_HMem, n_Hseries(H));

% ipStr	= struct('primSeq', [], 'primPar', [], 'coastTimes', [], ...
% 	'traj', [], 'trajPt', []);
% mainStr	= struct('hist', [], 'hLen', [], 'd', [], 'h', [], 'mk', [], ...
% 	'nHX', [], 'st', [], 'ipSeq', (repmat(ipStr, 1, nHWorst)), ...
% 	'ot', [], 'ohst', [], 'ohL', []);
mainStr	= struct('hist', [], 'hLen', [], 'd', [], 'h', [], 'mk', [], ...
	'nHX', [], 'st', [], 'ot', [], 'ohst', [], 'ohL', [], 'pf', []);
vertData= repmat(mainStr, 1, N);

for m1 = 1:(N+1)
	vertData(m1).hist	= zeros(n_Hworst, H+1);								% List of histories known so far
	vertData(m1).hLen	= zeros(n_Hworst, 1);								% Length of each history, needed for "multi-history"
	vertData(m1).d		= zeros(n_Hworst, 1);								% Label, one per history
	vertData(m1).pf		= zeros(n_Hworst, 1);								% P-feas, one per history
	vertData(m1).h		= zeros(n_Hworst, 1);								% 'Tail', one per history
	vertData(m1).mk		= zeros(n_Hworst, 1);								% Marker, 0 = NEW; 1 = OPEN; 2 = CLOSED
	vertData(m1).nHX	= 0;												% Number of histories known so far (X for explored)	
	vertData(m1).st		= zeros(n_states, n_Hworst);							% Possible discrete states, one set per history
	vertData(m1).ot		= zeros(n_Hworst, 1);								% Original 'Tail', one per history
	vertData(m1).ohst	= zeros(n_Hworst, H+1);								% List of original histories known so far
	vertData(m1).ohL	= zeros(n_Hworst, 1);								% Original length of each history, needed for "multi-history"
end

%----- Algorithm Initialization -------
vertSHist = sizeConstrHistory(H+1, vert_s, []);
% for L = H:-1:1	
% 	allnSHist	= sizeConstrHistory(L, vertS, vertSHist);					% Get "single" histories of start vert of length L+1
% 	if numel(allnSHist)
% 		vertSHist	= cat(1, vertSHist, [allnSHist zeros(size(allnSHist, 1), H+1-L)]);
% 	end
% end

nhbrs_vert_s = find(G(vert_s, :));
disp(st_init')

nhbr_s_relevant = 0;
for m = nhbrs_vert_s
	if max(abs(st_init(1:2, :) - (VCell(m, 1:2) + VCell(m, 3)/2)')) ...
			<= (0.5 + problem_data.constants.BDTOL)*problem_data.nomCellSize
		nhbr_s_relevant = m;
		break;
	end
end
disp(nhbr_s_relevant)
disp(vertSHist)

if nhbr_s_relevant
	vertSHist = vertSHist(vertSHist(:, 2) == nhbr_s_relevant, :);
else
	search_result = [];
	fprintf('Search failed, incompatible initial state and start vertex.\n');
	return
end

vertExp = [];
% initWtbar = waitbar(0, 'Initializing search data structure');
for m1 = 1:size(vertSHist, 1)	
	tHistLen	= sum(vertSHist(m1, :) > 0);								% In next line, tHistLen replaces (H+2) from standard history search
	tVert		= vertSHist(m1, tHistLen);									% Last vert in H+2 history, will go to OPEN
	
	tpResult	= handle_tp(vertSHist(m1,1:(tHistLen)), [], pThres, st_init);			% Cost of this history
% 	waitbar(m1/size(vertSHist,1), initWtbar);
	
	tHistCost	= tpResult.cost;
	tHistSt		= tpResult.sgmaF;
	tHistPf		= tpResult.pfeas;

	tnHX		= vertData(tVert).nHX;
	if tnHX < n_HMem
		tnHX				= tnHX + 1;
		vertData(tVert).nHX	= tnHX;
		tIndx				= tnHX;
	else																	% What to do when enough histories are known
		[worstCost, tIndx]	= max(vertData(tVert).d(1:tnHX));
		if tHistCost >= worstCost, continue; end							% If this history is worse than all known ones, ignore it
		
		% If not, remove the worst known history from OPEN and replace it
		% with this one
		[~, loc]		= ismember([tVert tIndx], openL(1:nOpen, 1:2), 'rows');
		openL(loc, :)	= [];
		nOpen			= nOpen - 1;
	end	

	updateVertData(tVert, tIndx, (vertSHist(m1, 2:tHistLen)), vert_s, ...
		(tHistLen - 1), tHistCost, tHistSt, tHistPf, 1, 1);
	
	vertExp = cat(1, vertExp, tVert);
	openL	= cat(1, openL, [tVert tIndx (tHistCost + heur(tVert))]);		% Add to OPEN list
	nOpen	= nOpen + 1;
end
openL(1:nOpen, :) = sortrows(openL(1:nOpen, :), 3);
% close(initWtbar);
drawnow();

%----- Algorithm Iterative Steps -------
% mainWtBar = waitbar(0, 'H-Cost search in progress');
nIter	= 0; nStExp = nOpen; nHExp = nOpen;
goalClosed = 0;

while (nOpen > 0) && (~goalClosed) && (openL(1, 3) < Inf)
	nIter	= nIter + 1;
	
	fprintf('******************** Iteration %i\n', nIter);
	
	vertAct	= openL(1, 1);		histAct	= openL(1, 2);
	
% 	if (vertAct == (N+1)), goalClosed = 1; continue; end					% Goal is a dummy vert
	if (vertAct == vert_g)
		goalClosed = 1;
% 		waitbar(1, mainWtBar);
		continue; 
	end
	
	fprintf('*** vertAct = %i\n', vertAct);
	disp(vertData(vertAct).hist(histAct, :))

	nhbrs		= find(G(vertAct, :));										% Note all neighbors of vertAct
	newOpen		= [];

	newHistLen	= vertData(vertAct).hLen(histAct, 1);
	newTail		= vertData(vertAct).hist(histAct, 1);						% Possible tail for vertNew
	actCost		= vertData(vertAct).d(histAct);								% Cost to come to (vertAct, histAct)
	actPf		= vertData(vertAct).pf(histAct);
	actState	= vertData(vertAct).st(:, histAct);
% 	disp([actState(1:2)'/missionData.nomCellSize actState(3:5)'])
	
	%{
	if (VCell(vertAct,3) >= dMax) && (newHistLen == 2)						% Adjacent to the goal
% 		fprintf('Almost done... \n')
		[isKnownBd, locBd] = ismember(vertAct, mrBdVerts);
		
% 		if isKnownBd
% 			goalCost = (actCost + bdData(locBd).cost);
% 		else
% 			fprintf('***** WARNING: Oversize cell of vert (%i) with no boundary cost associated. *****\n', vertAct);
% 			goalCost = Inf;
% 		end
		
		if vertData(N+1).mk == 0											% Goal not already in OPEN
			newOpen	= cat(1, newOpen, [(N+1) 0 goalCost]);					% Add goal to OPEN, with known terminal cost
			vertData(N+1).mk			= 1;
			vertData(N+1).hist(1, 1:2)	= [vertAct histAct];
			vertData(N+1).d				= goalCost;
		elseif goalCost < vertData(N+1).d									% If c2come to goal can be improved
			[~, locGoal]		= ismember((N+1), openL(1:nOpen, 1));
			nOpen				= nOpen - 1;
			openL(locGoal, :)	= [];
			
			newOpen	= cat(1, newOpen, [(N+1) 0 goalCost]);
			vertData(N+1).hist(1, 1:2)	= [vertAct histAct];
			vertData(N+1).d				= goalCost;
		end
		
		openL(1, :)	= [];
		nOpen		= nOpen - 1;
		
		tmpOpen = binSort(openL(1:nOpen, :), newOpen, 3);					% Add newly opened pairs to sorted open list
		if numel(tmpOpen) == 0
			nOpen	= 0;
			openL	= [];
		else
			nOpen	= size(tmpOpen, 1);
			openL(1:nOpen, :)	= tmpOpen;
		end		
		continue;
	end
	%}
	newHistAct	= vertData(vertAct).hist(histAct, 2:newHistLen);			% History corresponding to (vertAct, histAct), newHistLen replaces (H+1) here

	%{
	%**** FIX THIS PART WHEN USING MULTIRESOLUTION SEARCH ****
	if (VCell(vertAct,3) >= dMax)											% Technically not adjacent to the goal, do not explore further neighbors
		newHIndx= histAct;
		vertNew	= vertAct;		
		
		tpResult	= tpHandle([newTail newHistAct], actState);
		tHistCost	= tpResult.cost;
		tHistPf		= tpResult.pfeas;
		newSt		= tpResult.ztaF;		
% 		newIp		= tpResult.input;
		newCost		= actCost + tHistCost;
		newPf		= actPf*tHistPf;
		
		updateVertData(vertAct, histAct, newHistAct, newTail, ...
			(newHistLen-1), newCost, newSt, ...
			[vertData(vertNew).ipSeq(newHIndx).ip newIp], 1, 0);
		
		
		newOpen	= cat(1, newOpen, ...
			[vertNew newHIndx (newCost + heur(vertNew))]);					% Set of newly opened (vert, hist) pairs		
		nhbrs	= [];		
	end
	%}

	% ---- Explore each neighbor
	for vertNew = nhbrs
		fprintf('\t--- vertNew = %i\n', vertNew);
		
		if vertNew == vertData(vertAct).h(histAct), continue; end
		if vertNew == newTail, continue; end
		if any(vertNew == newHistAct), continue; end
		if any(G(vertNew, newHistAct(1:(end-1)))), continue; end
		%{
			First (H-1) elements of each row of newHistAct are "indices" of
			histories of vertNew through vertAct unique up to all but one
			element (tail of one vert) of histories of vertAct. This cost
			for vertNew through vertAct for each of these "indices" is to
			be minimized over that tail element.
		%}
		
		nHXNew		= vertData(vertNew).nHX;
		[isKnown, loc]= ismember(newHistAct,  ...
			vertData(vertNew).hist(1:nHXNew, 1:(newHistLen-1)), 'rows');
		
		if isKnown
			newHIndx = loc;
			
			if (vertData(vertNew).mk(newHIndx) == 2), continue; end
			% If a history is already CLOSED, it cannot be improved. Comes
			% from the proposition that says every pair enters open list
			% only once. 
		else
			if nHXNew < n_HMem
				newHIndx				= nHXNew + 1;
				vertData(vertNew).nHX	= newHIndx;
			else
				vertNewOpenHist			= find(vertData(vertNew).mk(1:nHXNew) == 1);
				if numel(vertNewOpenHist) == 0
					continue;
				else
					[worstNewCost, newHIndx]= max(vertData(vertNew).d(vertNewOpenHist));
					newHIndx				= vertNewOpenHist(newHIndx);
					if actCost >= worstNewCost, continue; end				% new history of vertNew cannot improve cost over its known ones
				end
			end
		end		
	
		tpResult	= handle_tp([newTail newHistAct vertNew], actState, pThres);
		tHistCost	= tpResult.cost;
		tHistPf		= tpResult.pfeas;
		newSt		= tpResult.sgmaF;
		newCost		= actCost + tHistCost;
		newPf		= actPf*tHistPf;
		
		if (newPf < problem_data.pfThreshold)
			continue;
		end
		
		if ~isKnown
			updateVertData(vertNew, newHIndx, [newHistAct vertNew], ...
				newTail, newHistLen, newCost, newSt, newPf, 1, 1);
			
			newOpen	= cat(1, newOpen, ...
				[vertNew newHIndx (newCost + heur(vertNew))]);				% Set of newly opened (vert, hist) pairs
			nStExp	= nStExp + 1;
			nHExp	= nHExp + 1;
			
		elseif (vertData(vertNew).d(newHIndx) > newCost) 
			[~, t2]	= ismember([vertNew newHIndx], openL(1:nOpen, 1:2), 'rows');
			nOpen	= nOpen - 1; 
			openL(t2, :)= [];
			
			updateVertData(vertNew, newHIndx, [newHistAct vertNew], ...
				newTail, newHistLen, newCost, newSt, newPf, 1, 1);
			
			newOpen	= cat(1, newOpen, ...
				[vertNew newHIndx (newCost + heur(vertNew))]);				% Set of newly opened (vert, hist) pairs
			nHExp	= nHExp + 1;
		end
		% Intentionally split these two ifs: program executes faster
	end
	
	vertExp = cat(1, vertExp, vertAct);
% 	drawCdV05(VCell, missionData.plots.envAxes, 'k', 1, 11, vertAct, ...
% 		missionData.plots.visitedColors(vertData(vertAct).nHX, :))
% 	thisFrame = getframe(gcf);
% % 	open(missionData.plots.writerObject);
% 	writeVideo(missionData.plots.writerObject, thisFrame);
		
	vertData(vertAct).mk(histAct) = 2;										% Mark this pair closed
	openL(1, :)	= [];														% Remove (vertAct, histAct) from OPEN
	nOpen		= nOpen - 1;
	
	tmpOpen = binSort(openL(1:nOpen, :), newOpen, 3);						% Add newly opened pairs to sorted open list
	if numel(tmpOpen) == 0
		nOpen	= 0;
		openL	= [];
	else
		nOpen	= size(tmpOpen, 1);
		openL(1:nOpen, :)	= tmpOpen;
	end	
	
% 	waitbar(min( (nHExp / (nHWorst*missionData.nFreeCells) ), 1), ...
% 		mainWtBar);
end

%----- Trace Optimal Path -------
[optCost, optHist]	= min(vertData(vert_g).d((vertData(vert_g).mk ~= 0)));

if (numel(optCost) == 0) || (optCost == Inf)
	fprintf('No path found.\n');
	search_result = [];
	return;
end

tail	= vertData(vert_g).ot(optHist);
head	= vert_g;
hIndx	= optHist;
hLen	= vertData(vert_g).ohL(optHist);
optPath.verts	= vertData(vert_g).ohst(hIndx, 1:hLen);
% optPath.ip		= ipStr;
optPath.indx	= [];
optPath.hLen	= [];
while tail ~= vert_s
	tail	= vertData(head).ot(hIndx);										% Get tail	
	nextH	= [tail vertData(head).ohst(hIndx, 1:(hLen-1))];
	
	optPath.verts	= [tail optPath.verts];	
	optPath.indx	= [hIndx optPath.indx];
	optPath.hLen	= [hLen optPath.hLen];
		
	head	= vertData(head).ohst(hIndx, (hLen-1));							% Step back one
		
	[~, hIndx]	= ismember(nextH, vertData(head).ohst, 'rows');				% The next index is the one that corresponds to the history
																			% formed by tail and first H verts of current history
	
	if hIndx
		hLen		= vertData(head).ohL(hIndx);
	end
end

% close(mainWtBar);
% drawnow();

search_result.performance.nIter	= nIter;
search_result.performance.nStExp = nStExp;
search_result.performance.nHExp	= nHExp;
search_result.optPathVerts		= optPath.verts;
search_result.vertData			= vertData;
search_result.optPath.HIndices	= optPath.indx;
search_result.optPath.HLengths	= optPath.hLen;
search_result.optPath.pfeas		= vertData(vert_g).pf(optHist);

	%**********************************************************************
	function UL = sizeConstrHistory(L, vertV, UH)

		% Specialized for finding histories of a single vert

		UL	= [];
		lnV = getAllHistV04(G, vertV, L+1, 0, vertV, []);
		if ~numel(lnV), return; end

		for n = 1:L
			lnV((VCell(lnV(:,n),3) >= dMax), :) = [];						% Remove verts corresponding to big cells
			if ~numel(lnV), break; end
		end
		if ~numel(lnV), return; end

% 		for n = 1:L															% Remove big-to-small transitions
% 			dDiff = VCell(lnV(:, n+1), 3) - VCell(lnV(:, n), 3);
% 			lnV((dDiff < 0), :) = [];
% 			if ~numel(lnV), break; end
% 		end
% 		if ~numel(lnV), return; end

		if numel(UH)
			projRows		= ismember(lnV, UH(:, 1:L+1), 'rows');			% Remove projections
			lnV(projRows,:)	= [];
		end

		UL = lnV;
	end
	%----------------------------------------------------------------------
	%**********************************************************************
	function updateVertData(vertNew, newHIndx, newHist, newTail, ...
			newHistLen, newCost, newSt, newPf, newMk, chOrg)
		
		vertData(vertNew).hist(newHIndx, 1:(H+1))	= zeros(1, H+1);
		vertData(vertNew).hist(newHIndx, (1:newHistLen))= newHist;
		vertData(vertNew).h(newHIndx)				= newTail;
		vertData(vertNew).hLen(newHIndx, :)			= newHistLen;
		vertData(vertNew).d(newHIndx)				= newCost;
		vertData(vertNew).pf(newHIndx)				= newPf;
		vertData(vertNew).st(:, newHIndx)			= newSt;
		vertData(vertNew).mk(newHIndx)				= newMk;

		if chOrg
			vertData(vertNew).ohL(newHIndx)			= newHistLen;
			vertData(vertNew).ohst(newHIndx, 1:newHistLen) = newHist;
			vertData(vertNew).ot(newHIndx)			= newTail;
		end
	end
	%----------------------------------------------------------------------
end
