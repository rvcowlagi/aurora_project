function path = find_path(backpointer, nodeG, nodeS)
current_node = nodeG;
order = 1;
while current_node ~= nodeS
    path(order) = current_node;
    current_node = backpointer(current_node);
    order = order + 1;
end
path(order) = nodeS;

path = path(end:-1:1);
end