function x = pi2pi(x)
% Returns angle between -pi and pi

if numel(x) > 1
	x = mod(x, 2*pi);
	x(x>pi) = x(x>pi) - 2*pi;
	x(x<-pi) = x(x<-pi) + 2*pi;
else
	while (x > pi) || (x < -pi)
		if x > pi,
			x = x - 2*pi;
		else
			x = x + 2*pi;
		end
	end
% 	x = mod(x, 2*pi);	
end