%{
Copyright (c) 2015 Raghvendra V. Cowlagi. All rights reserved.

Copyright notice:
=================
No part of this work may be reproduced without the written permission of
the copyright holder, except for non-profit and educational purposes under
the provisions of Title 17, USC Section 107 of the United States Copyright
Act of 1976. Reproduction of this work for commercial use is a violation of
copyright.


Disclaimer:
===========
This software program is intended for educational and research purposes.
The author and the institution with which the author is affiliated are not
liable for damages resulting the application of this program, or any
section thereof, which may be attributed to any errors that may exist in
this program.


Author information:
===================
Raghvendra V. Cowlagi, Ph.D,
Assistant Professor, Aerospace Engineering Program,
Department of Mechanical Engineering, Worcester Polytechnic Institute.
 
Higgins Laboratories, 247,
100 Institute Road, Worcester, MA 01609.
Phone: +1-508-831-6405
Email: rvcowlagi@wpi.edu
Website: http://www.wpi.edu/~rvcowlagi


The author welcomes questions, comments, suggestions for improvements, and
reports of errors in this program.


Program description:
====================
Draws rectangular cell decompositions.
%}

function draw_cells(verts_cd, axes_to_draw, color_of_edge, width_of_edge, ...
	text_on_cell, cells_to_draw, color_of_face, image_size, color_map, ...
	alt_text, alt_text_size, alt_text_color)

axes(axes_to_draw); hold on;
if (nargin <= 5) || (numel(cells_to_draw) == 0)
	drawCells = 1:size(verts_cd, 1);
else
	drawCells = cells_to_draw;
end

if (nargin > 6) && (numel(color_of_face) == 1) && (color_of_face == 0)
	Z	= zeros(image_size);
	for n = 1:size(verts_cd,1)
		Z((verts_cd(n,2) + 1):(verts_cd(n,2) + verts_cd(n,3)), ...
			(verts_cd(n,1) + 1):(verts_cd(n,1) + verts_cd(n,3))) = verts_cd(n,4);
	end
	image(Z, 'XData', 0.5, 'YData', 0.5); colormap(color_map);
end

for m = drawCells
	if (nargin > 6) && ((numel(color_of_face) ~= 1) || (color_of_face ~= 0))
		rectangle('Position', [verts_cd(m,1) verts_cd(m,2) verts_cd(m,3) ...
			verts_cd(m,4)], 'EdgeColor', color_of_edge, 'LineWidth', width_of_edge, ...
			'FaceColor', color_of_face);
	else
		rectangle('Position', [verts_cd(m,1) verts_cd(m,2) verts_cd(m,3) ...
			verts_cd(m,4)], 'EdgeColor', color_of_edge, 'LineWidth', width_of_edge);
	end
	if text_on_cell
		text(verts_cd(m,1) + 0.2*verts_cd(m,3), ...
			verts_cd(m,2) + 0.2*verts_cd(m,3), num2str(m), 'FontName', ...
			'Consolas', 'FontSize', text_on_cell, ...
			'FontWeight', 'bold', 'Color', 'k');
	end
	if nargin > 9
		text(verts_cd(m,1) + 0.2*verts_cd(m,3), ...
			verts_cd(m,2) + 0.2*verts_cd(m,3), alt_text, 'FontName', ...
			'Consolas', 'FontSize', alt_text_size, ...
			'FontWeight', 'bold', 'Color', alt_text_color);
	end
end
drawnow;