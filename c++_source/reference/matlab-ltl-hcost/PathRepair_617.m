%{
Zetian Zhang
Worcester, MA.

June. 17, 2014

Path repair algorithm simulation based on H-cost planning
 with probabilistic guarantees on feasibility
%}

function path_new = PathRepair_617(init_path, final_H, init_loc, init_angle)
global missionData

VCell = missionData.VCell;

path_new = init_path;

init_start = init_path(1);

[~,max_path_length] = size(path_new);



while missionData.hcost.H~=final_H
    
    
    fprintf('---Now H = %i\n', missionData.hcost.H);
    
    missionData.zta0 = [ init_loc*missionData.nomCellSize; init_angle]; %Inital location
    
    edge_list =  updatge_edge_list(path_new);   %Update the cost of every edge in this H
    
    [size_list,~] = size(edge_list);
    
    edge_current = [];
    
    %Pick the "broken" edge
    for edge_order = 1:size_list
        if (edge_list{edge_order,2} == Inf)
            edge_current = edge_list{edge_order,1}
            break;
        end
    end
    
    %If all the edge are fine, then H = H+1
    if isempty(edge_current)
        %         accept_h = missionData.hcost.H;
        missionData.hcost.H = missionData.hcost.H + 1;
        continue;
    end
    %     accept_h = missionData.hcost.H + 1
    %{
    Start repair the "broken" edge, apply H-cost algorthm from ahead of this
    edge to the final goal node.
    %}
    start_node_r = edge_current(1);
    
    missionData.vertS = start_node_r;   %Only change the start node
    
    loc_Snode = [VCell(start_node_r, 1) + 1, VCell(start_node_r, 2) + 1];
    head_vertSeq = path_new(1:edge_order - 1)
    [~,head_size] = size(head_vertSeq)
    
    H_save = missionData.hcost.H;
    
    if missionData.hcost.H > head_size
        missionData.hcost.H = head_size;
    end
    
    
    % Find the initial state for H-cost search
    head_tpResult = cbtaFeasTPV01(head_vertSeq, [], 0.9, missionData.zta0);
    missionData.hcost.H = H_save;
    tail_vertSeq = path_new(edge_order + missionData.hcost.H + 1:end);
    init_st = head_tpResult.sgmaF;
    % Location of the start node
    missionData.zta0 = [ [loc_Snode(1); loc_Snode(2)]*missionData.nomCellSize; 0];
    %missionData.zta0 = init_st;
    %Searching, stopped if search to the node which in the tail of last path
    searchResult_repair_edge = HCostPlan_repair_2(tail_vertSeq, init_st);
    
    
    pre_path = path_new;    %Store the last path
    
    repaired_edge = searchResult_repair_edge.optPathVerts
    
    %update the path with the repaired edge
    path_new = update_path(path_new, repaired_edge);
    
    accept_h = missionData.hcost.H;
    
    
    
    %Draw
    drawCdV07(VCell, missionData.plots.envAxes, 'k', 1, 7, pre_path(2:end-1), [1 1 0]);
    drawCdV07(VCell, missionData.plots.envAxes, 'k', 2, 7, path_new(2:end-1), [1 0.7 0]);
    
    
    [~,path_length] = size(path_new);
    if path_length > max_path_length
        max_path_length = path_length;
        if max_path_length < missionData.hcost.H
            break;
        end
    end
    
    %     if missionData.hcost.H > max_path_length
    
end
% drawCdV07(VCell, missionData.plots.envAxes, 'k', 1, 7, path_new(2:end-1), [1 1 0]);

    missionData.vertS = init_start;

    missionData.hcost.H = final_H;
    missionData.zta0 = [ init_loc*missionData.nomCellSize; init_angle];
    searchResult = HCostPlan();

    path_new = searchResult.optPathVerts;
    drawCdV07(VCell, missionData.plots.envAxes, 'k', 1, 7, pre_path(2:end-1), [1 1 0]);
     drawCdV07(VCell, missionData.plots.envAxes, 'k', 2, 7, path_new(2:end-1), [1 0.7 0]);






end
