%{
Copyright (c) 2014 Raghvendra V. Cowlagi. All rights reserved.

Copyright notice: 
=================
No part of this work may be reproduced without the written permission of
the copyright holder, except for non-profit and educational purposes under
the provisions of Title 17, USC Section 107 of the United States Copyright
Act of 1976. Reproduction of this work for commercial use is a violation of
copyright.


Disclaimer:
===========
This software program is intended for educational and research purposes.
The author and the institution with which the author is affiliated are not
liable for damages resulting the application of this program, or any
section thereof, which may be attributed to any errors that may exist in
this program.


Author information:
===================
Raghvendra V. Cowlagi, Ph.D,
Assistant Professor, Aerospace Engineering Program,
Department of Mechanical Engineering, Worcester Polytechnic Institute.
 
Higgins Laboratories, 247,
100 Institute Road, Worcester, MA 01609.
Phone: +1-508-831-6405
Email: rvcowlagi@wpi.edu
Website: http://www.wpi.edu/~rvcowlagi


The author welcomes questions, comments, suggestions for improvements, and
reports of errors in this program.


Program description:
====================
H-cost planning algorithm for satisfying LTL specifications.
%}


clear variables; close all; clc;
addpath(genpath(pwd));

%% Units
units.ft2m		= 0.3048;													% feet to meters
units.d2r		= pi/180;													% degrees to radians
units.mi2m		= 1609.344;													% miles to meters
units.mps2miph	= 3600/units.mi2m;											% m/s to mi/h
units.kt2miph	= 1.15078;													% knots to mi/h
units.rpm2rad	= 2*pi/60;													% rpm to rad/s
units.lbf2n		= 4.44822162;												% pounds-force to newtons
units.slug2kg	= 14.5939029;												% slugs to kg


%% Simple environment with square cell decomposition

fprintf('Loading environment map ... \t')
tic

n_row= 10;
n_col= 14;

nom_cell_size = 2000;															% base cell size, m
VCell	= [];
for cY = 1:2 
	for cX = 1:n_col
		VCell = cat(1, VCell, [cX-1 cY-1 1 1]*nom_cell_size);
	end
end
VCell	= cat(1, VCell, [0 2 4 4]*nom_cell_size);

for cY = 3:6 
	for cX = 5:n_col
		VCell = cat(1, VCell, [cX-1 cY-1 1 1]*nom_cell_size);
	end
end

for cY = 7:n_row 
	for cX = 1:n_col
		VCell = cat(1, VCell, [cX-1 cY-1 1 1]*nom_cell_size);
	end
end

nVertsCD = size(VCell, 1);

%----- Construct transition cost matrix
GCell = get_adjacency_matrix_4conn(VCell);

targetCells = 29;
baseCells	= [1 10];
threatCells	= [41:47 51:57 61:67 75:81];
noFlyCells	= [6 20:26];

obstacles	= [threatCells noFlyCells]';
for nObs = 1:size(obstacles, 1)
	nhbrs_m11 = find(GCell(obstacles(nObs), :));
	for m = nhbrs_m11
		GCell(m, obstacles(nObs)) = 0;
		GCell(obstacles(nObs), m) = 0;
	end
end
toc


%% Draw environment
fprintf('Drawing environment map ... \t')
tic
targetColor = [0.8 0.15 0.15];
baseColor	= [0 0.4 0.25];
threatColor = [0.25 0.25 0.2];
noFlyColor	= [0.25 0.25 0.2]; %[0 0.25 0.4];
txtSize		= 11;

figure('Units', 'normalized', 'Position', [0 0 0.9 0.9]); envAxes = axes; axis equal; hold on;
% figure; envAxes = axes; axis equal; hold on;

drawCdV07(VCell, envAxes, 'k', 1, txtSize)
drawCdV07(VCell, envAxes, 'k', 1, txtSize, threatCells, threatColor)
drawCdV07(VCell, envAxes, 'k', 1, txtSize, noFlyCells, noFlyColor)
drawCdV07(VCell, envAxes, 'k', 1, txtSize, baseCells, baseColor)
drawCdV07(VCell, envAxes, 'k', 2, txtSize, targetCells, targetColor)

commandwindow;
problem_data.plots.envAxes	= envAxes;
toc


%% Set problem data
problem_data.GCell			= GCell;
problem_data.VCell			= VCell;
problem_data.vertS			= 10;
problem_data.vertG			= 29;
problem_data.nomCellSize	= nom_cell_size;
problem_data.maxHCellSize	= Inf;
problem_data.nFreeCells		= size(GCell, 1) - numel(obstacles);

problem_data.constants.UNITS= units;
problem_data.constants.TOL	= 1e-6;
problem_data.constants.INFTY= 1e15;
problem_data.constants.BDTOL= 0.1;

problem_data.tilePlanner	= @cbtaFeasTPV01;
problem_data.zta0			= [ [10; 0.1]*problem_data.nomCellSize; 0];		% initial discrete "state"

%----- Manhattan heuristic
mnhtHeur = zeros(size(GCell,1), 1);
vertData = astar_multiplegoals(GCell, problem_data.vertG, 1:nVertsCD, zeros(nVertsCD, 1));
for m1 = 1:nVertsCD
	mnhtHeur(m1) = vertData(m1).d;
end
problem_data.heuristic		= 0.0*mnhtHeur;

%----- Associate transitions with transformations
problem_data.geometry.faceRef = ...
	  [	 1	-1	1	0	0;
		-2	 2	1	2	0;
		 2	-2	1	1	0;
		-1	 1	1	4	0;
		 1	-2	2	0	0;
		-2	-1	2	2	0;
		 2	 1	2	1	0;
		-1	-2	2	4	0;
		 1	 2	2	3	0;
		 2	-1	2	4	2;
		-2	 1	2	4	1;
		-1	 2	2	4	3];

%----- Describe how vertices are permuted
problem_data.geometry.vPermut	= ...
		[	1	2	3	4;													% Row 1 = Base: no transformation
			4	1	2	3;													% Row 2 = Rotate -90
			2	3	4	1;													% Row 3	= Rotate 90
			4	3	2	1;													% Row 4 = Flip horizontal
			2	1	4	3];													% Row 5 = Flip vertical

%----- Describe inverse transforms
problem_data.geometry.invTf	= [0; 2; 1; 3; 4];								% Inverse transformations in the same order as above rows

%----- Describe how cell faces are permuted
problem_data.geometry.tfRef	= ...
		[	1	2	3	4	5	6	7	8	9	10	11	12;					% Col 1 = Base: no transformation
			3	1	4	2	7	5	12	11	10	8	9	6;					% Col 2 = Rotate -90
			2	4	1	3	6	12	5	10	11	9	8	7;					% Col 3 = Rotate 90
			1	3	2	4	9	10	11	12	5	6	7	8;					% Col 4 = Flip horizontal
			4	2	3	1	8	11	10	5	12	7	6	9]';				% Col 5 = Flip vertical
		
problem_data.tiles.nGridPts_w= 31;
problem_data.tiles.nGridPts_a= 101;
problem_data.tiles.dStateSp_w= linspace(0, 1, problem_data.tiles.nGridPts_w);
problem_data.tiles.dStateSp_a= linspace(-pi/2, pi/2, problem_data.tiles.nGridPts_a);
problem_data.tiles.RCrvSp	= [linspace(1.5, 7.5, 13) linspace(7.5, 15, 7)];
problem_data.tiles.waGrid	= zeros(problem_data.tiles.nGridPts_w*...
	problem_data.tiles.nGridPts_a, 2);
nGridPts= 0;
for m1 = 1:problem_data.tiles.nGridPts_w
	for m2 = 1:problem_data.tiles.nGridPts_a
		nGridPts = nGridPts + 1;
		problem_data.tiles.waGrid(nGridPts, :) = ...
			[problem_data.tiles.dStateSp_w(m1), problem_data.tiles.dStateSp_a(m2)];
	end
end


%% H-Cost planner parameters
H		= 2;
nHMem	= 15;

problem_data.hcost.H	= H;
problem_data.hcost.nHMem= nHMem;


%% Create lifted graph
fprintf('Constructing lifted graph ... \t')
tic
%********************* Initial states.. FIX***********
vert_s	= 10;
[VH, GH, vertH_s] = get_lifted_graph(GCell, H, vert_s);
toc

%% Pre-process CBTA

% % tic
% % CBRAResults	= CBRA;
% % fprintf('Pre-processing tiles ... \t\t')
% % toc
% % 
% % tic
% % fprintf('Saving pre-processed data ... \t')
% % save CBRAResults.mat CBRAResults
% % toc
% % return
% 
% tic
% load CBRAResults.mat CBRAResults
% toc
% 
% problem_data.tiles.CBRAResults = CBRAResults;

%% Remove edges in lifted graph based on CBTA

% TBA later

%% Create Buchi automaton

fprintf('Constructing Buchi automaton ...')
tic
alphabet	= {'p1','p2','p3'};
orig_alph	= {'p1', 'p2'};
Alph_s		= alphabet_set(alphabet);

formula		= '(G p1) & (F !p2)'; %read_formula(alphabet,orig_alph);
buchi_aut	= get_buchi(formula, Alph_s);
toc

% for m1 = 1:numel(buchi_aut.S)
% 	for m2 = 1:numel(buchi_aut.S)
% 		if ~numel(buchi_aut.trans{m1, m2}), continue; end
% 		fprintf('Transition:\t'); disp([m1 m2])
% 		disp(buchi_aut.trans{m1, m2}')
% 	end
% end

%% Form product graph

n_vertsH= size(VH, 1);
tmp1	= zeros(1, n_vertsH);
tmp1(vertH_s) = 1;


VH_aug	= [VH; zeros(1, H+1)];
GH_aug	= [GH zeros(n_vertsH, 1); tmp1 0]; 

bl_product.states_n		= (n_vertsH + 1)*numel(buchi_aut.S);		% Number of states
bl_product.states_init	= (n_vertsH*numel(buchi_aut.S) + 1) : ((n_vertsH + 1)*numel(buchi_aut.S) );
bl_product.states_accept= zeros(n_vertsH*numel(buchi_aut.F), 1);
bl_product.states_id	= zeros(bl_product.states_n, H+2);
bl_product.adjacency	= [];

edge_list	= zeros( (nnz(GH) + size(VH, 1))*(numel(buchi_aut.S)^2), 2);
n_edges		= 0;
for m1 = 0:(bl_product.states_n-1)
	m11 = floor( m1/numel(buchi_aut.S) ) + 1;
	m12 = mod( m1, numel(buchi_aut.S) ) + 1;
	
	bl_product.states_id(m1 + 1, :)	= [VH_aug(m11, :) buchi_aut.S(m12)];
	
	nhbrs_m11 = find(GH_aug(m11, :) > 0);
	if ~numel(nhbrs_m11), continue; end
	
	for m33 = nhbrs_m11
		for m22 = 1:numel(buchi_aut.S)
			if numel(buchi_aut.trans{m12, m22})
				%{
					Check if there is a transition in the Buchi automaton
					from the Buchi state portion of the current
					"product-state"  to this (m22) Buchi state. If yes,
					then find the index of the new "product-state" and add
					to the list of edges in the product automaton.
				%}
				m2		= (m33 - 1)*numel(buchi_aut.S) + m22;
				n_edges	= n_edges + 1;
				edge_list(n_edges, :)	= [m1 m2 1];
			end
		end
	end
end
bl_product.adjacency = sparse(edge_list(:, 1), edge_list(:, 2), ...
	edge_list(:, 3), bl_product.states_n, bl_product.states_n);



return
%% Test TP
% vertS	= 12;
% allHist	= getAllHist(GCell, vertS, H+2, 0, vertS, []);
% disp(allHist)
% 
% tmp1= size(allHist, 1);
% tmp2= min((round(rand*tmp1) + 1), tmp1);
% tmp2= 5;
% 
% sgmaInit	= zeros(problem_data.tiles.nGridPts_w*problem_data.tiles.nGridPts_a, 1);
% % sgmaInit( (floor(problem_data.tiles.nGridPts_w/2))*problem_data.tiles.nGridPts_a + ...
% % 	floor(problem_data.tiles.nGridPts_a/2) + 1 ) = 1;
% 
% 
% sgmaInit( ((floor(problem_data.tiles.nGridPts_w/2) - 3)*problem_data.tiles.nGridPts_a + 1) : ...
% 	( (floor(problem_data.tiles.nGridPts_w/2) - 3)*problem_data.tiles.nGridPts_a ...
% 	+ floor(problem_data.tiles.nGridPts_a/2) + 1 ) ) = 1;
% sgmaInit( ((floor(problem_data.tiles.nGridPts_w/2) - 2)*problem_data.tiles.nGridPts_a + 1) : ...
% 	( (floor(problem_data.tiles.nGridPts_w/2) - 2)*problem_data.tiles.nGridPts_a ...
% 	+ floor(problem_data.tiles.nGridPts_a/2) + 1 ) ) = 1;
% sgmaInit( ((floor(problem_data.tiles.nGridPts_w/2) - 1)*problem_data.tiles.nGridPts_a + 1) : ...
% 	( (floor(problem_data.tiles.nGridPts_w/2) - 1)*problem_data.tiles.nGridPts_a ...
% 	+ floor(problem_data.tiles.nGridPts_a/2) + 1 ) ) = 1;
% sgmaInit( ((floor(problem_data.tiles.nGridPts_w/2) - 0)*problem_data.tiles.nGridPts_a + 1) : ...
% 	( (floor(problem_data.tiles.nGridPts_w/2) - 0)*problem_data.tiles.nGridPts_a ...
% 	+ floor(problem_data.tiles.nGridPts_a/2) + 1 ) ) = 1;
% 
% 
% % disp(problem_data.tiles.waGrid(sgmaInit > 0, :))
% % find(sgmaInit > 0)
% tic
% tpResult = cbtaFeasTPV01(allHist(tmp2, :), sgmaInit);
% toc
% 
% zta0 = [ [12; 0.7]*problem_data.nomCellSize; 45*pi/180];
% % tpResult = cbtaFeasTPV01(allHist(tmp2, :), [], zta0);
% 
% return



%% Search!
fprintf('\nSearching ... \n')

tic
searchResult = HCostPlan;
toc

%----- Display resultant path
if ~isempty(searchResult)
	drawCdV07(VCell, envAxes, 'k', 1, txtSize, ...
		searchResult.optPathVerts, [1 0.7 0])
	
	searchResult.optPath.pfeas
end

return

%% Aircraft data
problem_data.vCruise			= vCruise;
problem_data.loadFactorLimits= [1; 2];
problem_data.spdVar			= 0;
problem_data.ssDim			= 5;
problem_data.zta0			= [x0; y0; yaw0; vCruise; problem_data.loadFactorLimits(2)];

addpath('..\AircraftDynamics');
addpath('Dubins\')

fprintf('Loading aircraft data ... \t\t\t\t\t\t\t')
tic
%----- The global variables GTMData and us76Data are loaded here
load '..\AircraftDynamics\typicalAircraftDataV01.mat';
toc
