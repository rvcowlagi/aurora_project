% *************************************************************************
% Author:		Raghvendra V. Cowlagi
%				Dept. of Aerospace Engineering, Georgia Inst. Tech.
% Description:	Find zero crossings of a continuously varying function
%				given as a table of values
% Last Mod.:	08/11/2010
%**************************************************************************
function xZeros = findZeros(fSmp)

f1 = [sign(fSmp) 0];
f2 = [0 sign(fSmp)];
fDiff = f1 - f2;
fDiff = fDiff(2:(end-1));

xZeros = find(fDiff);														% Last sample of that sign
%--------------------------------------------------------------------------