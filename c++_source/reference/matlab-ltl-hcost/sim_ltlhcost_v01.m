%%
addpath('ltl2ba\');

alphabet = {'p1','p2','p3'};
orig_alph = {'p1', 'p2'};
Alph_s = alphabet_set(alphabet)
formula=read_formula(alphabet,orig_alph)
B = create_buchi(formula, Alph_s);