function propositions = get_region(edge, region_matrix, alphabet)
%look up the edge in lifted graph in which region
s = [];

for i = 1:length(edge)
    [~, p] = find(region_matrix == edge(i));
    s = [s, p'];
end

state = zeros(1, length(alphabet));

for i = 1:length(s)
    state(s(i)) = 1;
end
propositions = [alphabet{find(state)}];
end


