function ort = find_ort(edge)
global missionData

node_1 = edge(1);
node_2 = edge(2);

position_1 = missionData.VCell(node_1, 1:2);
position_2 = missionData.VCell(node_2, 1:2);

ort = 0;

if position_2(1) > position_1(1)
    ort = 0;
end

if position_2(1) < position_1(1)
    ort = pi;
end

if position_2(2) > position_1(2)
    ort = pi/2;
end

if position_2(2) < position_1(2)
    ort = pi*3/2;
end