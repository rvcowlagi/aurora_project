Reference:

Graph

* http://www.geeksforgeeks.org/graph-and-its-representations/
* https://www.topcoder.com/community/data-science/data-science-tutorials/power-up-c-with-the-standard-template-library-part-1/
* https://www.topcoder.com/community/data-science/data-science-tutorials/power-up-c-with-the-standard-template-library-part-2/
* http://stackoverflow.com/questions/17473753/c11-return-value-optimization-or-move/17473869#17473869
* http://lafstern.org/matt/col1.pdf

Search

* http://homepages.abdn.ac.uk/f.guerin/pages/teaching/CS1013/practicals/aStarTutorial.htm
* http://www.cppblog.com/mythit/archive/2009/04/19/80492.aspx (Chinese Version)
* http://users.cis.fiu.edu/~weiss/adspc++2/code/
* http://heyes-jones.com/astar.php
* http://stackoverflow.com/questions/11912736/c-a-star-implementation-determining-whether-a-node-is-already-in-the-priori
* http://stackoverflow.com/questions/10394508/which-std-container-to-use-in-a-algorithms-openset
* https://github.com/justinhj/astar-algorithm-cpp
* http://code.activestate.com/recipes/577457-a-star-shortest-path-algorithm/
* http://theory.stanford.edu/~amitp/GameProgramming/

C++

* http://stackoverflow.com/questions/642229/why-do-i-need-to-use-typedef-typename-in-g-but-not-vs
* http://stackoverflow.com/questions/8584431/why-is-the-keyword-typename-needed-before-qualified-dependent-names-and-not-b
* http://stackoverflow.com/questions/18671062/override-function-parameter-type-with-type-of-derived-class