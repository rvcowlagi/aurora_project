/*
 * test_time_collision.cpp
 *
 *  Created on: 1 Dec 2016
 *      Author: zetian
 */


/*
 * test_dynamic.cpp
 *
 *  Created on: 30 Nov 2016
 *      Author: zetian
 */



/*
 * test_product_time.cpp
 *
 *  Created on: 23 Nov 2016
 *      Author: zetian
 */




// standard libaray
#include <stdio.h>
#include <vector>
#include <ctime>
#include <tuple>
#include <algorithm>
#include <bitset>
// opencv
#include "opencv2/opencv.hpp"

// user
#include "graph/graph.h"
#include "graph/astar.h"
#include "map/sgrid_builder.h"
#include "map/graph_builder.h"
#include "map/map_utils.h"
#include "vis/graph_vis.h"

#include "trans_sys/buchi_automaton.h"
#include "trans_sys/graph_lifter.h"
#include "trans_sys/product_automaton.h"
#include "trans_sys/product_time_automaton.h"
#include "trans_sys/hcost/hcost_interface.h"

using namespace cv;
using namespace srcl;

int main(int argc, char** argv )
{
	/*** 0. Preprocessing CBTA data ***/
//	std::map<unsigned int,std::shared_ptr<Hlevel>> h_levels = HCost::hcost_preprocessing();
	std::map<unsigned int,std::shared_ptr<Hlevel>> h_levels;
	/*** 1. Create a empty square grid ***/
	int row_num = 5;
	int col_num = 5;

	std::shared_ptr<SquareGrid> grid = MapUtils::CreateSquareGrid(row_num,col_num,50, 0);
	grid->SetInterestedRegionLabel(22, 2);
	grid->SetObstacleRegionLabel(10, 1);
	grid->SetObstacleRegionLabel(11, 1);
	grid->SetObstacleRegionLabel(13, 1);
	grid->SetObstacleRegionLabel(14, 1);
	// assign properties of square cells






	DynamicTarget dt4(DynamicTargetType::OBSTACLE, 1, 0);
	std::vector<int64_t> dt4_seq;
	//dt4_seq.push_back(17);
	dt4_seq.push_back(-1);
	dt4_seq.push_back(12);
	dt4_seq.push_back(12);
	dt4_seq.push_back(12);
	dt4_seq.push_back(12);
	dt4_seq.push_back(12);
	dt4_seq.push_back(-1);
	dt4_seq.push_back(-1);
	dt4_seq.push_back(-1);
	dt4.AddRepeatSequence(dt4_seq);
	grid->AddDynamicTarget(dt4);



	/*** 2. Construct a graph from the square grid ***/
	std::shared_ptr<Graph_t<SquareCell*>> graph = GraphBuilder::BuildFromSquareGrid_IgnoreObstacle(grid,false);
//	namedWindow("Processed Image", WINDOW_NORMAL ); // WINDOW_AUTOSIZE
////	Mat vis_img;
//	Mat vis_img;
//	GraphVis::VisSquareGridWithLabels(*grid, vis_img);
//	for(int i = 0; i < 15; i++)
//	{
//		GraphVis::VisSquareGridWithLabels(*grid, vis_img, i);
//		imshow("Processed Image", vis_img);
//		waitKey(0);
//	}
//	return 0;
	/*** 3. Construct a graph from the buchi automata ***/
//	std::string ltl_formula = "([] p0) && ([] !p1) && (<> p3) && (<> p4) && ((<> p2) || (<> p5))";
//	std::string ltl_formula = "([] p0) && ([] !p1) && (<> p3) && (<> p4) && (<> p2) && (<> p5)";
	std::string ltl_formula = "[] p0 && [] !p1 && <> p2";
//	std::string ltl_formula = "([] p0) && ([] ! p1) && ( <> p4 ||<> p5 || <> p2 )";
	std::vector<std::string> buchi_regions;
	buchi_regions.push_back("p0");
	buchi_regions.push_back("p1");
	buchi_regions.push_back("p2");
//	buchi_regions.push_back("p5");
//	buchi_regions.push_back("p3");
//	buchi_regions.push_back("p5");
//	buchi_regions.push_back("p4");

	std::shared_ptr<Graph_t<BuchiState>> buchi_graph = BuchiAutomaton::CreateBuchiGraph(ltl_formula,buchi_regions);

	/*** 4. Construct a lifted graph ***/
	int HistoryH = 0;
	std::shared_ptr<Graph_t<LiftedSquareCell>> lifted_graph = GraphLifter::CreateLiftedGraph(HistoryH, graph);


	/*** 5. Construct a product graph ***/
	clock_t     total_time;
	clock_t		product_time;
	total_time = clock();
	product_time = clock();
//	std::shared_ptr<Graph_t<ProductState>> product_graph = ProductAutomaton::CreateProductAutomaton(lifted_graph,buchi_graph);
	std::shared_ptr<Graph_t<ProductTimeState>> product_graph_time = std::make_shared<Graph_t<ProductTimeState>>();
	product_time = clock() - product_time;
//	std::cout << "Product graph time in " << double(product_time)/CLOCKS_PER_SEC << " s." << std::endl;

	/*** 6. Search in the product graph ***/
	// Set start node in original graph
	Vertex<SquareCell *> * start_node_origin = graph->GetVertexFromID(2);

	// Convert from original node to product node
	uint64_t virtual_start_id = ProductTimeAutomaton::SetVirtualStartIncre(start_node_origin, lifted_graph, buchi_graph, product_graph_time);
//	uint64_t virtual_start_id = ProductAutomaton::SetVirtualStartIncre(start_node_origin, lifted_graph, buchi_graph, product_graph);

//	auto virtual_start = product_graph->GetVertexFromID(virtual_start_id);
	// Search in product graph
	std::vector<uint32_t>  buchi_acc = buchi_graph->GetVertexFromID(0)->bundled_data_.acc_state_idx;

	GetProductTimeNeighbour get_product_time_neighbour(lifted_graph, buchi_graph, h_levels, grid);

	std::vector<ProductTimeState> path = AStar::ProductIncSearch(product_graph_time, virtual_start_id,buchi_acc, GetNeighbourBDSFunc_t<ProductTimeState>(get_product_time_neighbour));
//	std::vector<Vertex<ProductState>*> path = AStar::ProductSearch(product_graph, virtual_start,buchi_acc);

	total_time = clock() - total_time;
	std::cout << "Total time in " << double(total_time)/CLOCKS_PER_SEC << " s." << std::endl;
	// For debug, check path detail
	//	std::cout << "location: "<<std::endl;
	//	for (auto it = path.begin()+1; it!=path.end();it++){
	//		std::cout <<"Product id: " << (*it)->bundled_data_.GetID() <<", history:";
	//			for (auto ite = (*it)->bundled_data_.lifted_vertex_->bundled_data_.history.begin();ite != (*it)->bundled_data_.lifted_vertex_->bundled_data_.history.end(); ite++){
	//				std::cout << " "<<(*ite)->bundled_data_->GetID();
	//			}
	//		std::cout << ", Buchi state: " <<  (*it)->bundled_data_.buchi_vertex_->bundled_data_.GetID();
	//		std::cout << std::endl;
	//	}

	// Map path in the product graph back to the square grid graph

	std::vector<Vertex<SquareCell*>*> path_origin;
	for (auto it = path.begin()+1; it != path.end(); it++){
		std::cout << "path location id: "<<(*it).lifted_vertex_->bundled_data_.history.back()->vertex_id_ << "   Time step: " <<(*it).time_step_<< std::endl;
		path_origin.push_back((*it).lifted_vertex_->bundled_data_.history.front());
	}
	path_origin.insert(path_origin.end(),path.back().lifted_vertex_->bundled_data_.history.begin()+1,path.back().lifted_vertex_->bundled_data_.history.end());
	std::cout << "final time step: " << path.back().time_step_ << std::endl;
	/*** 7. Visualize the map and graph ***/
	// Image Layouts: square grid -> graph -> path
	GraphVis vis;
	Mat vis_img;

//	vis.VisSquareGrid(*grid, vis_img);
//	vis.VisSquareGridGraph(*graph, vis_img, vis_img, true);
//	vis.VisSquareGridPath(path_origin, vis_img, vis_img);

	namedWindow("Processed Image", WINDOW_NORMAL ); // WINDOW_AUTOSIZE
	std::vector<uint8_t> obs_ids, int_ids;
	obs_ids.push_back(1);
	//obs_ids.push_back(3);
	int_ids.push_back(2);
	int_ids.push_back(3);
	int_ids.push_back(4);

	for(int i = 0; i < path_origin.size(); i++)
	{
		GraphVis::VisSquareGridWithColoredLabels(*grid, vis_img, path_origin, obs_ids, int_ids, i);
		imshow("Processed Image", vis_img);
		waitKey(0);
	}

	waitKey(0);

	return 0;
}



