/*
 * test_collision.cpp
 *
 *  Created on: Dec 1, 2016
 *      Author: rdu
 */

// standard libaray
#include <stdio.h>
#include <vector>
#include <ctime>
#include <tuple>
#include <algorithm>
#include <bitset>
// opencv
#include "opencv2/opencv.hpp"

// user
#include "graph/graph.h"
#include "graph/astar.h"
#include "map/sgrid_builder.h"
#include "map/graph_builder.h"
#include "map/map_utils.h"
#include "vis/graph_vis.h"

#include "trans_sys/buchi_automaton.h"
#include "trans_sys/graph_lifter.h"
#include "trans_sys/product_automaton.h"
#include "trans_sys/hcost/hcost_interface.h"

using namespace cv;
using namespace srcl;

int main(int argc, char** argv )
{
	/*** 1. Create a empty square grid ***/
	int row_num = 4;
	int col_num = 4;

	std::shared_ptr<SquareGrid> grid = MapUtils::CreateSquareGrid(row_num, col_num, 100, 0);

//	grid->SetObstacleRegionLabel(2, 1);
//	grid->SetObstacleRegionLabel(12, 1);
//	grid->SetObstacleRegionLabel(22, 1);
//	grid->SetObstacleRegionLabel(32, 1);

	grid->SetInterestedRegionLabel(8, 2);
//	grid->SetInterestedRegionLabel(25, 2);
//	grid->SetInterestedRegionLabel(71, 3);
//	grid->SetInterestedRegionLabel(66, 4);
//	grid->SetInterestedRegionLabel(99, 5);

//	DynamicTarget dt1(DynamicTargetType::INTERESTED, 2, 0);
//	std::vector<int64_t> dt1_seq;
//	dt1_seq.push_back(2);
//	dt1_seq.push_back(3);
//	dt1_seq.push_back(4);
//	dt1_seq.push_back(5);
//	dt1_seq.push_back(6);
//	dt1_seq.push_back(5);
//	dt1_seq.push_back(4);
//	dt1_seq.push_back(3);
//	dt1.AddRepeatSequence(dt1_seq);
//	grid->AddDynamicTarget(dt1);

	DynamicTarget dt2(DynamicTargetType::OBSTACLE, 1, 0);
	std::vector<int64_t> dt2_seq;
	dt2_seq.push_back(1);
	dt2_seq.push_back(5);
	dt2_seq.push_back(9);
	dt2_seq.push_back(5);
	dt2.AddRepeatSequence(dt2_seq);
	grid->AddDynamicTarget(dt2);

	DynamicTarget dt3(DynamicTargetType::OBSTACLE, 3, 0);
	std::vector<int64_t> dt3_seq;
	dt3_seq.push_back(7);
	dt3_seq.push_back(11);
	dt3.AddRepeatSequence(dt3_seq);
	grid->AddDynamicTarget(dt3);

	/*** 7. Visualize the map and graph ***/
	namedWindow("Processed Image", WINDOW_NORMAL ); // WINDOW_AUTOSIZE

	// Image Layouts: square grid -> graph -> path
	Mat vis_img;
	std::vector<uint8_t> obs_ids, int_ids;
	obs_ids.push_back(1);
	obs_ids.push_back(3);
	int_ids.push_back(2);

	for(int i = 0; i < 15; i++)
	{
		std::cout << "collision checking (9 -> 5): " << grid->GetCellFromID(9)->CheckCollision(5, i) << std::endl;
		std::cout << "collision checking (6 -> 7): " << grid->GetCellFromID(6)->CheckCollision(7, i) << std::endl;
		std::cout << "collision checking (11 -> 10): " << grid->GetCellFromID(11)->CheckCollision(10, i) << std::endl;

		GraphVis::VisSquareGridWithColoredLabels(*grid, vis_img, obs_ids, int_ids, i);
		imshow("Processed Image", vis_img);
		waitKey(0);
	}

	return 0;
}



