/*
 * ltl_dyn_env.cpp
 *
 *  Created on: 1 Dec 2016
 *      Author: zetian
 */

// standard libaray
#include <stdio.h>
#include <vector>
#include <ctime>
#include <tuple>
#include <algorithm>
#include <bitset>
// opencv
#include "opencv2/opencv.hpp"

// user
#include "graph/graph.h"
#include "graph/astar.h"
#include "map/sgrid_builder.h"
#include "map/graph_builder.h"
#include "map/map_utils.h"
#include "vis/graph_vis.h"

#include "trans_sys/buchi_automaton.h"
#include "trans_sys/graph_lifter.h"
#include "trans_sys/product_automaton.h"
#include "trans_sys/product_time_automaton.h"
#include "trans_sys/hcost/hcost_interface.h"

using namespace cv;
using namespace srcl;

int main(int argc, char** argv )
{
	/*** 0. Preprocessing CBTA data ***/
//	std::map<unsigned int,std::shared_ptr<Hlevel>> h_levels = HCost::hcost_preprocessing();
	std::map<unsigned int,std::shared_ptr<Hlevel>> h_levels;
	/*** 1. Create a empty square grid ***/
	int row_num = 12;
	int col_num = 12;

	std::shared_ptr<SquareGrid> grid = MapUtils::CreateSquareGrid(row_num,col_num,50, 0);

	// assign properties of square cells


	grid->SetObstacleRegionLabel(96, 1);
	grid->SetObstacleRegionLabel(97, 1);
	for (int i =100; i < 104; i++){
		grid->SetObstacleRegionLabel(i, 1);
	}
	grid->SetObstacleRegionLabel(106, 1);
	grid->SetObstacleRegionLabel(107, 1);

	grid->SetObstacleRegionLabel(5, 1);
	grid->SetObstacleRegionLabel(17, 1);
	grid->SetObstacleRegionLabel(29, 1);
	grid->SetObstacleRegionLabel(65, 1);
	grid->SetObstacleRegionLabel(77, 1);
	grid->SetObstacleRegionLabel(89, 1);

	DynamicTarget dt1(DynamicTargetType::INTERESTED, 4, 0);
	std::vector<int64_t> dt1_seq;
	dt1_seq.push_back(120);
	dt1_seq.push_back(121);
	dt1_seq.push_back(122);
	dt1_seq.push_back(134);
	dt1_seq.push_back(133);
	dt1_seq.push_back(132);

	dt1.AddRepeatSequence(dt1_seq);
	grid->AddDynamicTarget(dt1);

	DynamicTarget dt2(DynamicTargetType::INTERESTED, 2, 0);
	std::vector<int64_t> dt2_seq;
	dt2_seq.push_back(9);
	dt2_seq.push_back(10);
	dt2_seq.push_back(11);
	dt2_seq.push_back(23);
	dt2_seq.push_back(35);
	dt2_seq.push_back(34);
	dt2_seq.push_back(33);
	dt2_seq.push_back(21);
	dt2.AddRepeatSequence(dt2_seq);
	grid->AddDynamicTarget(dt2);


//	DynamicTarget dt2(DynamicTargetType::INTERESTED, 3, 0);
//	std::vector<int64_t> dt2_seq;
//	dt2_seq.push_back(32);
//	dt2_seq.push_back(33);
//	dt2_seq.push_back(34);
//	dt2_seq.push_back(33);
//	dt2_seq.push_back(32);
//	dt2_seq.push_back(31);
//	dt2.AddRepeatSequence(dt2_seq);
//	grid->AddDynamicTarget(dt2);

	DynamicTarget dt3(DynamicTargetType::INTERESTED, 3, 0);
	std::vector<int64_t> dt3_seq;
	dt3_seq.push_back(0);
	dt3_seq.push_back(1);
	dt3_seq.push_back(2);
	dt3_seq.push_back(14);
	dt3_seq.push_back(26);
	dt3_seq.push_back(25);
	dt3_seq.push_back(24);
	dt3_seq.push_back(12);
	dt3.AddRepeatSequence(dt3_seq);
	grid->AddDynamicTarget(dt3);

	//First moving wall
	DynamicTarget dt4(DynamicTargetType::OBSTACLE, 1, 0);
	std::vector<int64_t> dt4_seq;
	dt4_seq.push_back(27);
	dt4_seq.push_back(39);
	dt4_seq.push_back(51);
	dt4_seq.push_back(39);
	dt4_seq.push_back(27);
	dt4_seq.push_back(15);
	dt4_seq.push_back(3);
	dt4_seq.push_back(15);
	dt4.AddRepeatSequence(dt4_seq);
	grid->AddDynamicTarget(dt4);

	DynamicTarget dt5(DynamicTargetType::OBSTACLE, 1, 0);
	std::vector<int64_t> dt5_seq;
	dt5_seq.push_back(39);
	dt5_seq.push_back(51);
	dt5_seq.push_back(63);
	dt5_seq.push_back(51);
	dt5_seq.push_back(39);
	dt5_seq.push_back(27);
	dt5_seq.push_back(15);
	dt5_seq.push_back(27);
	dt5.AddRepeatSequence(dt5_seq);
	grid->AddDynamicTarget(dt5);

	DynamicTarget dt6(DynamicTargetType::OBSTACLE, 1, 0);
	std::vector<int64_t> dt6_seq;
	dt6_seq.push_back(51);
	dt6_seq.push_back(63);
	dt6_seq.push_back(75);
	dt6_seq.push_back(63);
	dt6_seq.push_back(51);
	dt6_seq.push_back(39);
	dt6_seq.push_back(27);
	dt6_seq.push_back(39);
	dt6.AddRepeatSequence(dt6_seq);
	grid->AddDynamicTarget(dt6);

	DynamicTarget dt7(DynamicTargetType::OBSTACLE, 1, 0);
	std::vector<int64_t> dt7_seq;
	dt7_seq.push_back(63);
	dt7_seq.push_back(75);
	dt7_seq.push_back(87);
	dt7_seq.push_back(75);
	dt7_seq.push_back(63);
	dt7_seq.push_back(51);
	dt7_seq.push_back(39);
	dt7_seq.push_back(51);
	dt7.AddRepeatSequence(dt7_seq);
	grid->AddDynamicTarget(dt7);


	//Second moving wall

	DynamicTarget dt8(DynamicTargetType::OBSTACLE, 1, 0);
	std::vector<int64_t> dt8_seq;
	dt8_seq.push_back(68);
	dt8_seq.push_back(69);
	dt8_seq.push_back(68);
	dt8_seq.push_back(67);
	dt8_seq.push_back(66);
	dt8_seq.push_back(67);
	dt8.AddRepeatSequence(dt8_seq);
	grid->AddDynamicTarget(dt8);

	DynamicTarget dt9(DynamicTargetType::OBSTACLE, 1, 0);
	std::vector<int64_t> dt9_seq;
	dt9_seq.push_back(69);
	dt9_seq.push_back(70);
	dt9_seq.push_back(69);
	dt9_seq.push_back(68);
	dt9_seq.push_back(67);
	dt9_seq.push_back(68);
	dt9.AddRepeatSequence(dt9_seq);
	grid->AddDynamicTarget(dt9);
//
	DynamicTarget dt10(DynamicTargetType::OBSTACLE, 1, 0);
	std::vector<int64_t> dt10_seq;
	dt10_seq.push_back(70);
	dt10_seq.push_back(71);
	dt10_seq.push_back(70);
	dt10_seq.push_back(69);
	dt10_seq.push_back(68);
	dt10_seq.push_back(69);
	dt10.AddRepeatSequence(dt10_seq);
	grid->AddDynamicTarget(dt10);


	// First door
	DynamicTarget dt11(DynamicTargetType::OBSTACLE, 1, 0);
	std::vector<int64_t> dt11_seq;
	for (int i=0;i<10;i++){
		dt11_seq.push_back(104);
	}
	for (int i=0;i<5;i++){
		dt11_seq.push_back(-1);
	}
	dt11.AddRepeatSequence(dt11_seq);
	grid->AddDynamicTarget(dt11);

	DynamicTarget dt12(DynamicTargetType::OBSTACLE, 1, 0);
	std::vector<int64_t> dt12_seq;
	for (int i=0;i<10;i++){
		dt12_seq.push_back(105);
	}
	for (int i=0;i<5;i++){
		dt12_seq.push_back(-1);
	}
	dt12.AddRepeatSequence(dt12_seq);
	grid->AddDynamicTarget(dt12);


	// Second door
	DynamicTarget dt13(DynamicTargetType::OBSTACLE, 1, 0);
	std::vector<int64_t> dt13_seq;
	for (int i=0;i<35;i++){
		dt13_seq.push_back(41);
	}
	for (int i=0;i<5;i++){
		dt13_seq.push_back(-1);
	}
	dt13.AddRepeatSequence(dt13_seq);
	grid->AddDynamicTarget(dt13);

	DynamicTarget dt14(DynamicTargetType::OBSTACLE, 1, 0);
	std::vector<int64_t> dt14_seq;
	for (int i=0;i<35;i++){
		dt14_seq.push_back(53);
	}
	for (int i=0;i<5;i++){
		dt14_seq.push_back(-1);
	}
	dt14.AddRepeatSequence(dt14_seq);
	grid->AddDynamicTarget(dt14);

	// Third door
	DynamicTarget dt15(DynamicTargetType::OBSTACLE, 1, 0);
	std::vector<int64_t> dt15_seq;
	for (int i=0;i<55;i++){
		dt15_seq.push_back(98);
	}
	for (int i=0;i<5;i++){
		dt15_seq.push_back(-1);
	}
	dt15.AddRepeatSequence(dt15_seq);
	grid->AddDynamicTarget(dt15);

	DynamicTarget dt16(DynamicTargetType::OBSTACLE, 1, 0);
	std::vector<int64_t> dt16_seq;
	for (int i=0;i<55;i++){
		dt16_seq.push_back(99);
	}
	for (int i=0;i<5;i++){
		dt16_seq.push_back(-1);
	}
	dt16.AddRepeatSequence(dt16_seq);
	grid->AddDynamicTarget(dt16);


	/*** 2. Construct a graph from the square grid ***/
	std::shared_ptr<Graph_t<SquareCell*>> graph = GraphBuilder::BuildFromSquareGrid_IgnoreObstacle(grid,false);

	/*** 3. Construct a graph from the buchi automata ***/
//	std::string ltl_formula = "([] p0) && ([] !p1) && (<> p3) && (<> p4) && ((<> p2) || (<> p5))";
//	std::string ltl_formula = "([] p0) && ([] !p1) && (<> p3) && (<> p4) && (<> p2) && (<> p5)";
	std::string ltl_formula = "[] p0 && [] !p1 && <> p2 && <> p3 && <>[] p4";
//	std::string ltl_formula = "[] p0 && [] !p1 && <> (p2 && <> (p3 && <> p4))";
//	std::string ltl_formula = "[] p0 && [] !p1 &&  ((<> p2 && <> p3) R <> p4)";
//	std::string ltl_formula = "([] p0) && ([] ! p1) && ( <> p4 ||<> p5 || <> p2 )";
	std::vector<std::string> buchi_regions;
	buchi_regions.push_back("p0");
	buchi_regions.push_back("p1");
	buchi_regions.push_back("p2");
//	buchi_regions.push_back("p5");
	buchi_regions.push_back("p3");
//	buchi_regions.push_back("p5");
	buchi_regions.push_back("p4");

	std::shared_ptr<Graph_t<BuchiState>> buchi_graph = BuchiAutomaton::CreateBuchiGraph(ltl_formula,buchi_regions);

	/*** 4. Construct a lifted graph ***/
	int HistoryH = 0;
	std::shared_ptr<Graph_t<LiftedSquareCell>> lifted_graph = GraphLifter::CreateLiftedGraph(HistoryH, graph);


	/*** 5. Construct a product graph ***/
	clock_t     total_time;
	clock_t		product_time;
	total_time = clock();
	product_time = clock();
//	std::shared_ptr<Graph_t<ProductState>> product_graph = ProductAutomaton::CreateProductAutomaton(lifted_graph,buchi_graph);
	std::shared_ptr<Graph_t<ProductTimeState>> product_graph_time = std::make_shared<Graph_t<ProductTimeState>>();
	product_time = clock() - product_time;

	/*** 6. Search in the product graph ***/
	// Set start node in original graph
	Vertex<SquareCell *> * start_node_origin = graph->GetVertexFromID(143);

	// Convert from original node to product node
	uint64_t virtual_start_id = ProductTimeAutomaton::SetVirtualStartIncre(start_node_origin, lifted_graph, buchi_graph, product_graph_time);
//	uint64_t virtual_start_id = ProductAutomaton::SetVirtualStartIncre(start_node_origin, lifted_graph, buchi_graph, product_graph);

	// Search in product graph
	std::vector<uint32_t>  buchi_acc = buchi_graph->GetVertexFromID(0)->bundled_data_.acc_state_idx;

	GetProductTimeNeighbour get_product_time_neighbour(lifted_graph, buchi_graph, h_levels, grid);

	std::vector<ProductTimeState> path = AStar::ProductIncSearch(product_graph_time, virtual_start_id,buchi_acc, GetNeighbourBDSFunc_t<ProductTimeState>(get_product_time_neighbour));
//	std::vector<Vertex<ProductState>*> path = AStar::ProductSearch(product_graph, virtual_start,buchi_acc);

	total_time = clock() - total_time;
	std::cout << "Total time in " << double(total_time)/CLOCKS_PER_SEC << " s." << std::endl;
	// For debug, check path detail
	//	std::cout << "location: "<<std::endl;
	//	for (auto it = path.begin()+1; it!=path.end();it++){
	//		std::cout <<"Product id: " << (*it)->bundled_data_.GetID() <<", history:";
	//			for (auto ite = (*it)->bundled_data_.lifted_vertex_->bundled_data_.history.begin();ite != (*it)->bundled_data_.lifted_vertex_->bundled_data_.history.end(); ite++){
	//				std::cout << " "<<(*ite)->bundled_data_->GetID();
	//			}
	//		std::cout << ", Buchi state: " <<  (*it)->bundled_data_.buchi_vertex_->bundled_data_.GetID();
	//		std::cout << std::endl;
	//	}

	// Map path in the product graph back to the square grid graph

	std::vector<Vertex<SquareCell*>*> path_origin;
	for (auto it = path.begin()+1; it != path.end(); it++){
		std::cout << "path location id: "<<(*it).lifted_vertex_->bundled_data_.history.back()->vertex_id_ << "   Time step: " <<(*it).time_step_<< std::endl;
		path_origin.push_back((*it).lifted_vertex_->bundled_data_.history.front());
	}
	path_origin.insert(path_origin.end(),path.back().lifted_vertex_->bundled_data_.history.begin()+1,path.back().lifted_vertex_->bundled_data_.history.end());
	std::cout << "final time step: " << path.back().time_step_ << std::endl;

//	auto path_vis = (*it).lifted_vertex_->bundled_data_.history.back();

	/*** 7. Visualize the map and graph ***/
	// Image Layouts: square grid -> graph -> path
	Mat vis_img;

	namedWindow("Processed Image", WINDOW_NORMAL ); // WINDOW_AUTOSIZE
	std::vector<uint8_t> obs_ids, int_ids;
	obs_ids.push_back(1);
	//obs_ids.push_back(3);
	int_ids.push_back(2);
	int_ids.push_back(3);
	int_ids.push_back(4);

//	GraphVis::VisSquareGridWithColoredLabels(*grid, vis_img, obs_ids, int_ids);
//	VideoWriter outputVideo;
//	outputVideo.open("./ltl_dyn_env.mp4",  CV_FOURCC('M','P','4','2'), 1, vis_img.size(), true);
//	if (!outputVideo.isOpened())
//	{
//		std::cout  << "FAILED TO CREATE VIDEO" << std::endl;
//		return -1;
//	}

//	for(int i = 0; i < path_origin.size(); i++)
//	{
//		GraphVis::VisSquareGridWithColoredLabels(*grid, vis_img, path_origin, obs_ids, int_ids, i);
//		outputVideo << vis_img;
//		std::string id_str;
//		if(i < 10)
//			id_str = "0"+std::to_string(i);
//		else
//			id_str = std::to_string(i);
//		imwrite( "image-"+id_str+".jpg", vis_img);
//		imshow("Processed Image", vis_img);
//		waitKey(0);
//	}

	// $ffmpeg -framerate 1 -i image-%02d.jpg -c:v libx264 -vf fps=25 -pix_fmt yuv420p out_1hz.mp4

	while(1){
		for(int i = 0; i < path_origin.size(); i++)
		{
			GraphVis::VisSquareGridWithColoredLabels(*grid, vis_img, path_origin, obs_ids, int_ids, i);
			imshow("Processed Image", vis_img);
			waitKey(0);
		}
	}

	return 0;
}

