## Module: Graph Visualization

Notes:

* Change the adjancency list of the graph to be public, for visualization purpose
* Maybe we can add edges to be part of the vertex?
* Any reason for starting vertex index from 1 instead of 0?

Reference:

* [Color code](http://www.rapidtables.com/web/color/RGB_Color.htm)