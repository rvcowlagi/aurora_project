/*
 * label_graph.h
 *
 *  Created on: Nov 23, 2016
 *      Author: rdu
 */

#ifndef SRC_TRANS_SYS_LABEL_GRID_H_
#define SRC_TRANS_SYS_LABEL_GRID_H_

#include <vector>
#include <string>

#include "graph/bds_base.h"
#include "map/common_types.h"
#include "map/region_label.h"

namespace srcl
{

enum class DynamicTargetType
{
	OBSTACLE,
	INTERESTED
};

class DynamicTarget
{
public:
	DynamicTarget(DynamicTargetType type, uint8_t target_label_id, uint64_t start_timestamp):
		type_(type),
		target_label_id_(target_label_id),
		start_timestamp_(start_timestamp){};
	DynamicTarget() = delete;
	~DynamicTarget(){};

	friend class TaskRegionGrid;

public:
	uint8_t target_label_id_;
	DynamicTargetType type_;

private:
	uint64_t start_timestamp_;
	std::vector<int64_t> repeat_seq_;

private:
	bool GetTargetRegionIDAtTime(uint64_t timestamp, uint64_t& id)
	{
		if(timestamp < start_timestamp_ ||  repeat_seq_.empty())
			return false;

		uint64_t relative_time = timestamp - start_timestamp_;
		int64_t seq_id = repeat_seq_[relative_time % repeat_seq_.size()];

		if(seq_id == -1)
			return false;
		else
			id = static_cast<uint64_t>(seq_id);

		return true;
	}

public:
	void AddRepeatSequence(std::vector<int64_t>& seq) { repeat_seq_ = seq; }
};

class TaskRegion: public BDSBase<TaskRegion>
{
public:
	TaskRegion(uint64_t id, uint32_t row, uint32_t col):
		BDSBase<TaskRegion>(id)
	{
		index_.x = col;
		index_.y = row;
	};
	~TaskRegion(){};

	friend class TaskRegionGrid;

private:
	std::vector<RegionLabel> static_labels_;
	std::vector<RegionLabel> dynamic_labels_;

	Position2D index_;

private:
	bool LabelExist(uint8_t label);

	std::string GetTaskRegionName() const;
	uint32_t GetTaskRegionBitMap() const;

	std::string GetFullTaskRegionName() const;
	uint32_t GetFullTaskRegionBitMap() const;

	void AssignRegionLabel(uint8_t label);
	void RemoveRegionLabel(uint8_t label);

	bool AssignDynamicRegionLabel(uint8_t label);
	void ClearDynamicRegionLabel() { dynamic_labels_.clear(); };

	// useless function, just to make the template happy to build a graph
	double GetHeuristic(const TaskRegion& other_struct) const { return 0.0; };
};


class TaskRegionGrid
{
public:
	TaskRegionGrid(uint32_t row_num, uint32_t col_num);
	TaskRegionGrid(uint32_t row_num, uint32_t col_num, uint8_t default_label);
	~TaskRegionGrid();

	friend class SquareGrid;

private:
	uint32_t row_size_;
	uint32_t col_size_;
	uint8_t default_label_; // label for workspace

	std::vector<TaskRegion*> labels_;
	std::vector<DynamicTarget> dynamic_targets_;

	std::string GetTaskRegionName(uint32_t row, uint32_t col);
	uint32_t GetTaskRegionBitMap(uint32_t row, uint32_t col);
	void AssignLabelToRegion(uint32_t row, uint32_t col, uint8_t label);
	void RemoveLabelFromRegion(uint32_t row, uint32_t col, uint8_t label);

	void AssignLabelToRegion(uint64_t id, uint8_t label);
	void RemoveLabelFromRegion(uint64_t id, uint8_t label);

	bool RegionIsObstacle(uint64_t id);
	void SetDynamicTargetsToTRGrid(std::vector<uint64_t>& int_region_ids, std::vector<uint64_t>& obs_region_ids, uint64_t timestamp);
	void ResetDynamicTargetsToTRGrid(const std::vector<uint64_t>& int_region_ids, const std::vector<uint64_t>& obs_region_ids);

public:
	// general helper functions
	uint64_t GetIDFromIndex(uint32_t row, uint32_t col);
	uint8_t GetDefaultRegionLabel() const { return default_label_; };

	void AddDynamicTarget(DynamicTarget& target) { dynamic_targets_.push_back(target); };
	void ClearDynamicTargets() { dynamic_targets_.clear(); };

	// for static task region grid
	std::string GetTaskRegionName(uint64_t id) const;
	uint32_t GetTaskRegionBitMap(uint64_t id) const;

	// for dynamic task region grid
	std::string GetDynamicTaskRegionName(uint64_t id, uint64_t timestamp);
	uint32_t GetDynamicTaskRegionBitMap(uint64_t id, uint64_t timestamp);

	bool CheckCollision(uint64_t current_id, uint64_t target_id, uint64_t timestamp);
};

}



#endif /* SRC_TRANS_SYS_LABEL_GRID_H_ */
