/*
 * square_grid.h
 *
 *  Created on: Jan 28, 2016
 *      Author: rdu
 */

#ifndef SRC_MAP_SQUARE_GRID_H_
#define SRC_MAP_SQUARE_GRID_H_

#include <map>
#include <vector>
#include <cstdint>
#include <string>
#include <memory>

#include "graph/bds_base.h"
#include "map/common_types.h"
#include "map/region_label.h"
#include "map/task_region_grid.h"

namespace srcl{

class SquareGrid;

class SquareCell: public BDSBase<SquareCell>{
public:
	SquareCell(uint64_t id, uint32_t row, uint32_t col, BoundingBox bbox, OccupancyType occupancy, SquareGrid* parent):
		BDSBase<SquareCell>(id),
		occu_(occupancy),
		parent_grid_(parent)
	{
		index_.x = col;
		index_.y = row;

		bbox_ = bbox;

		location_.x = bbox_.x.min + (bbox_.x.max - bbox_.x.min)/2;
		location_.y = bbox_.y.min + (bbox_.y.max - bbox_.y.min)/2;
	}
	~SquareCell(){};

	Position2D index_;
	Position2D location_;
	OccupancyType occu_;
	BoundingBox bbox_;
	SquareGrid* parent_grid_;

public:
	double GetHeuristic(const SquareCell& other_struct) const{
		double x1,x2,y1,y2;

		x1 = this->location_.x;
		y1 = this->location_.y;

		x2 = other_struct.location_.x;
		y2 = other_struct.location_.y;

		// static_cast: can get wrong result to use "unsigned long" type for deduction
		long x_error = static_cast<long>(x1) - static_cast<long>(x2);
		long y_error = static_cast<long>(y1) - static_cast<long>(y2);

		double cost = std::abs(x_error) + std::abs(y_error);
		//	std::cout<< "heuristic cost: " << cost << std::endl;

		return cost;
	}

public:
	/*------------- Label related variables and function -------------*/
	std::string GetTaskRegionName() const;
	uint32_t GetTaskRegionBitMap() const;

	std::string GetTaskRegionName(uint64_t timestamp) const;
	uint32_t GetTaskRegionBitMap(uint64_t timestamp) const;

	bool CheckCollision(uint64_t target_id, uint64_t timestamp);
};

class SquareGrid{
public:
	SquareGrid(uint32_t row_num, uint32_t col_num, uint32_t cell_size);
	SquareGrid(uint32_t row_num, uint32_t col_num, uint32_t cell_size, uint8_t default_label);
	~SquareGrid();

	friend class SquareCell;

public:
	std::map<uint64_t, SquareCell*> cells_;

public:
	uint32_t row_size_;
	uint32_t col_size_;
	uint32_t cell_size_;

private:
	BoundingBox CalcBoundingBox(uint64_t id);

public:
	void SetCellOccupancy(uint32_t row, uint32_t col, OccupancyType occ);
	void SetCellOccupancy(uint64_t id, OccupancyType occ);
	uint64_t GetIDFromIndex(uint32_t row, uint32_t col);
	uint64_t GetIDFromPosition(uint32_t x, uint32_t y);
	SquareCell* GetCellFromID(uint64_t id);
	std::vector<SquareCell*> GetNeighbours(uint64_t id, bool allow_diag);

	/*------------- Label related variables and function -------------*/
private:
	uint8_t default_label_;
	std::shared_ptr<TaskRegionGrid> label_grid_;

private:
	// for static task region grid
	std::string GetTaskRegionName(uint64_t id) { return label_grid_->GetTaskRegionName(id); };
	uint32_t GetTaskRegionBitMap(uint64_t id) { return label_grid_->GetTaskRegionBitMap(id); };

	// for dynamic task region grid
	std::string GetTaskRegionName(uint64_t id, uint64_t timestamp) { return label_grid_->GetDynamicTaskRegionName(id, timestamp); };
	uint32_t GetTaskRegionBitMap(uint64_t id, uint64_t timestamp) { return label_grid_->GetDynamicTaskRegionBitMap(id, timestamp); };
	bool CheckCollision(uint64_t current_id, uint64_t target_id, uint64_t timestamp) { return label_grid_->CheckCollision(current_id, target_id, timestamp); };

public:
	void SetObstacleRegionLabel(uint64_t id, uint8_t label);
	void SetInterestedRegionLabel(uint64_t id, uint8_t label);

	void AddDynamicTarget(DynamicTarget& target);
	void ClearDynamicTargets();
};

}


#endif /* SRC_MAP_SQUARE_GRID_H_ */
