/*
 * label_grid.cpp
 *
 *  Created on: Nov 23, 2016
 *      Author: rdu
 */

#include <task_region_grid.h>

using namespace srcl;

/*************************** TaskRegion *********************************/

bool TaskRegion::LabelExist(uint8_t label)
{
	bool label_exist = false;

	for(auto& reg : static_labels_)
	{
		if(reg.GetRegionLabelID() == label)
		{
			label_exist = true;
			break;
		}
	}

	for(auto& reg : dynamic_labels_)
	{
		if(reg.GetRegionLabelID() == label)
		{
			label_exist = true;
			break;
		}
	}

	return label_exist;
}

std::string TaskRegion::GetTaskRegionName() const
{
	std::string region_name;

	for(auto& it : static_labels_)
		region_name += it.GetRegionLabelName();

	return region_name;
}

uint32_t TaskRegion::GetTaskRegionBitMap() const {
	uint32_t bit_map = 0;

	for(auto& it : static_labels_)
		bit_map = bit_map | it.GetRegionLabelBitMap();

	return bit_map;
}

void TaskRegion::AssignRegionLabel(uint8_t label)
{
	bool label_exist = false;

	for(auto& reg : static_labels_)
	{
		if(reg.GetRegionLabelID() == label)
		{
			label_exist = true;
			break;
		}
	}

	if(!label_exist) {
		RegionLabel new_region;
		new_region.SetRegionLabelID(label);
		static_labels_.push_back(new_region);
	}
}

void TaskRegion::RemoveRegionLabel(uint8_t label)
{
	bool label_exist = false;

	for(auto it = static_labels_.begin(); it != static_labels_.end(); it++)
	{
		if((*it).GetRegionLabelID() == label)
		{
			label_exist = true;
			static_labels_.erase(it);
			break;
		}
	}
}

std::string TaskRegion::GetFullTaskRegionName() const
{
	std::string region_name;

	for(auto& it : static_labels_)
		region_name += it.GetRegionLabelName();

	for(auto& it : dynamic_labels_)
		region_name += it.GetRegionLabelName();

	return region_name;
}

uint32_t TaskRegion::GetFullTaskRegionBitMap() const {
	uint32_t bit_map = 0;

	for(auto& it : static_labels_)
		bit_map = bit_map | it.GetRegionLabelBitMap();

	for(auto& it : dynamic_labels_)
		bit_map = bit_map | it.GetRegionLabelBitMap();

	return bit_map;
}

bool TaskRegion::AssignDynamicRegionLabel(uint8_t label)
{
	bool label_exist = false;

	for(auto& reg : static_labels_)
	{
		if(reg.GetRegionLabelID() == label)
		{
			label_exist = true;
			break;
		}
	}

	for(auto& reg : dynamic_labels_)
	{
		if(reg.GetRegionLabelID() == label)
		{
			label_exist = true;
			break;
		}
	}

	//std::cout << "dynamic label " << label << " existed :" << label_exist << std::endl;

	if(label_exist)
		return false;

	RegionLabel new_region;
	new_region.SetRegionLabelID(label);
	dynamic_labels_.push_back(new_region);

	return true;
}

/*************************** TaskRegionGrid *********************************/

TaskRegionGrid::TaskRegionGrid(uint32_t row_num, uint32_t col_num):
		row_size_(row_num), col_size_(col_num),
		default_label_(0)
{
	labels_.resize(row_size_ * col_size_);

	for(uint32_t i = 0; i < row_num; i++)
		for(uint32_t j = 0; j < col_num; j++)
		{
			uint64_t new_id = i * col_num + j;
			TaskRegion* new_label = new TaskRegion(new_id, i, j);
			new_label->AssignRegionLabel(default_label_);
			labels_[new_id] = new_label;
		}
}

TaskRegionGrid::TaskRegionGrid(uint32_t row_num, uint32_t col_num, uint8_t default_label):
		row_size_(row_num), col_size_(col_num),
		default_label_(default_label)
{
	labels_.resize(row_size_ * col_size_);

	for(uint32_t i = 0; i < row_num; i++)
		for(uint32_t j = 0; j < col_num; j++)
		{
			uint64_t new_id = i * col_num + j;
			TaskRegion* new_label = new TaskRegion(new_id, i, j);
			new_label->AssignRegionLabel(default_label);
			labels_[new_id] = new_label;
		}
}

TaskRegionGrid::~TaskRegionGrid()
{
	for(auto label : labels_)
		delete label;
	labels_.clear();
}

uint64_t TaskRegionGrid::GetIDFromIndex(uint32_t row, uint32_t col)
{
	return row * col_size_ + col;
}

std::string TaskRegionGrid::GetTaskRegionName(uint64_t id) const
{
	return labels_[id]->GetTaskRegionName();
}

uint32_t TaskRegionGrid::GetTaskRegionBitMap(uint64_t id) const
{
	return labels_[id]->GetTaskRegionBitMap();
}

void TaskRegionGrid::SetDynamicTargetsToTRGrid(std::vector<uint64_t>& int_region_ids, std::vector<uint64_t>& obs_region_ids, uint64_t timestamp)
{
	uint64_t updating_id;

	// add dynamic target labels
	for(auto& dt : dynamic_targets_)
	{
		if(dt.GetTargetRegionIDAtTime(timestamp, updating_id))
		{
			if(dt.type_ == DynamicTargetType::INTERESTED)
			{
				if(!RegionIsObstacle(updating_id))
				{
					labels_[updating_id]->AssignDynamicRegionLabel(dt.target_label_id_);
					int_region_ids.push_back(updating_id);
				}
			}
			else if(dt.type_ == DynamicTargetType::OBSTACLE)
			{
				if(labels_[updating_id]->AssignDynamicRegionLabel(dt.target_label_id_))
				{
					labels_[updating_id]->RemoveRegionLabel(default_label_);
					obs_region_ids.push_back(updating_id);
				}
			}
		}
	}
}

void TaskRegionGrid::ResetDynamicTargetsToTRGrid(const std::vector<uint64_t>& int_region_ids, const std::vector<uint64_t>& obs_region_ids)
{
	// clear dynamic target labels
	for(auto& rg_id : int_region_ids)
		labels_[rg_id]->ClearDynamicRegionLabel();

	for(auto& rg_id : obs_region_ids)
	{
		labels_[rg_id]->ClearDynamicRegionLabel();
		labels_[rg_id]->AssignRegionLabel(default_label_);
	}
}

uint32_t TaskRegionGrid::GetDynamicTaskRegionBitMap(uint64_t id, uint64_t timestamp)
{
//	uint64_t updating_id;
	std::vector<uint64_t> interested_region_ids;
	std::vector<uint64_t> obstacle_region_ids;

	// add dynamic target labels
	SetDynamicTargetsToTRGrid(interested_region_ids, obstacle_region_ids, timestamp);

	// get bitmap
	uint32_t bitmap = labels_[id]->GetFullTaskRegionBitMap();

	// clear dynamic target labels
	ResetDynamicTargetsToTRGrid(interested_region_ids, obstacle_region_ids);

	return bitmap;
}

bool TaskRegionGrid::CheckCollision(uint64_t current_id, uint64_t target_id, uint64_t timestamp)
{
	uint64_t checked_obs_id;

	for(auto& dt : dynamic_targets_)
	{
		// only check obstacles
		if(dt.type_ == DynamicTargetType::OBSTACLE)
		{
			// if the obstacle exists at next time stamp
			if(dt.GetTargetRegionIDAtTime(timestamp + 1, checked_obs_id) && checked_obs_id == current_id)
			{
				if(dt.GetTargetRegionIDAtTime(timestamp, checked_obs_id) && checked_obs_id == target_id)
					return true;
			}
		}
	}

	return false;
}

std::string TaskRegionGrid::GetDynamicTaskRegionName(uint64_t id, uint64_t timestamp)
{
	std::vector<uint64_t> interested_region_ids;
	std::vector<uint64_t> obstacle_region_ids;

	// add dynamic target labels
	SetDynamicTargetsToTRGrid(interested_region_ids, obstacle_region_ids, timestamp);

	// get bitmap
	std::string label_name = labels_[id]->GetFullTaskRegionName();

	// clear dynamic target labels
	ResetDynamicTargetsToTRGrid(interested_region_ids, obstacle_region_ids);

	return label_name;
}

void TaskRegionGrid::AssignLabelToRegion(uint64_t id, uint8_t label)
{
	labels_[id]->AssignRegionLabel(label);
}

void TaskRegionGrid::RemoveLabelFromRegion(uint64_t id, uint8_t label)
{
	labels_[id]->RemoveRegionLabel(label);
}

std::string TaskRegionGrid::GetTaskRegionName(uint32_t row, uint32_t col)
{
	uint64_t id = GetIDFromIndex(row, col);

	return GetTaskRegionName(id);
}

uint32_t TaskRegionGrid::GetTaskRegionBitMap(uint32_t row, uint32_t col)
{
	uint64_t id = GetIDFromIndex(row, col);

	return GetTaskRegionBitMap(id);
}

void TaskRegionGrid::AssignLabelToRegion(uint32_t row, uint32_t col, uint8_t label)
{
	uint64_t id = GetIDFromIndex(row, col);

	AssignLabelToRegion(id, label);
}

void TaskRegionGrid::RemoveLabelFromRegion(uint32_t row, uint32_t col, uint8_t label)
{
	uint64_t id = GetIDFromIndex(row, col);

	RemoveLabelFromRegion(id, label);

}

bool TaskRegionGrid::RegionIsObstacle(uint64_t id)
{
	return !labels_[id]->LabelExist(default_label_);
}
