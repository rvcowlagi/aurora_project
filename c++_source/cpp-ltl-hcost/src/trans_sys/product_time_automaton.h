/*
 * product_time_automaton.h
 *
 *  Created on: 23 Nov 2016
 *      Author: zetian
 */

#ifndef SRC_TRANS_SYS_PRODUCT_TIME_AUTOMATON_H_
#define SRC_TRANS_SYS_PRODUCT_TIME_AUTOMATON_H_

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/SparseCore>


#include <vector>

#include "graph/graph.h"
#include "trans_sys/graph_lifter.h"
#include "trans_sys/buchi_automaton.h"
#include "trans_sys/hcost/hcost_tile_library.h"
#include "trans_sys/hcost/hcost_interface.h"

namespace srcl
{

struct ProductTimeState: public BDSBase<ProductTimeState>
{
	ProductTimeState(uint64_t id):

		BDSBase<ProductTimeState>(id),
		rgn_idx_init_(),
		lifted_vertex_(nullptr),
		buchi_vertex_(nullptr),
//		product_vertex_(nullptr),
		time_step_(){};
	~ProductTimeState(){};

//	Vertex<ProductState>* product_vertex_;
	Vertex<LiftedSquareCell>* lifted_vertex_;
	Vertex<BuchiState>* buchi_vertex_;
	uint64_t time_step_;
	std::vector<int> rgn_idx_init_;
	double GetHeuristic(const ProductTimeState& other_struct) const
	{
		return 0.0;
	}
};

namespace ProductTimeAutomaton
{

uint64_t SetVirtualStartIncre(Vertex<SquareCell *> * start_node_origin, std::shared_ptr<Graph_t<LiftedSquareCell>> lifted_graph, std::shared_ptr<Graph_t<BuchiState>> buchi_graph, std::shared_ptr<Graph_t<ProductTimeState>> product_time_graph);

};

class GetProductTimeNeighbour {
public:
	GetProductTimeNeighbour(std::shared_ptr<Graph_t<LiftedSquareCell>> lifted_graph, std::shared_ptr<Graph_t<BuchiState>> buchi_graph, std::map<unsigned int,std::shared_ptr<Hlevel>> h_levels, std::shared_ptr<SquareGrid> grid){
		lifted_graph_ = lifted_graph;
		buchi_graph_ = buchi_graph;
//		tile_traversal_data_ = tile_traversal_data;
		//		h_levels_ = h_levels;
		grid_ = grid;
		history_H_ = lifted_graph->GetVertexFromID(0)->bundled_data_.history.size() - 1;
		Eigen::MatrixXi verts_cd(grid->row_size_*grid->col_size_,4);
		int total_rows = 0;
		for (int m2 = 0; m2 < grid->row_size_; m2++)
			for (int m1 = 0; m1 < grid->col_size_; m1++){
				verts_cd.row(total_rows) << m1, m2, 1, 1;
				total_rows++;
			}
		verts_cd_ = verts_cd;
	}

private:
	std::shared_ptr<Graph_t<LiftedSquareCell>> lifted_graph_;
	std::shared_ptr<Graph_t<BuchiState>> buchi_graph_;
//	TileTraversalData tile_traversal_data_;
	int history_H_;
	std::shared_ptr<SquareGrid> grid_;
	Eigen::MatrixXi verts_cd_;

public:
	std::vector<std::tuple<ProductTimeState, double>> operator()(ProductTimeState cell);
};
}

#endif /* SRC_TRANS_SYS_PRODUCT_TIME_AUTOMATON_H_ */
