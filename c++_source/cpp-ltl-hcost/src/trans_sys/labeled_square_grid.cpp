/*
 * labeled_square_grid.cpp
 *
 *  Created on: Nov 23, 2016
 *      Author: rdu
 */

#include "trans_sys/labeled_square_grid.h"

using namespace srcl;

LabeledSquareGrid::LabeledSquareGrid(uint32_t row_num, uint32_t col_num, uint32_t cell_size, uint8_t default_label):
		SquareGrid(row_num, col_num, cell_size)
{

}

LabeledSquareGrid::~LabeledSquareGrid()
{

}

void LabeledSquareGrid::SetObstacleRegionLabel(uint64_t id, uint8_t label)
{
	this->SetCellOccupancy(id, OccupancyType::OCCUPIED);
	label_grid_->RemoveLabel(id, label_grid_->GetDefaultLabel());
	label_grid_->AssignLabel(id, label);
}

void LabeledSquareGrid::SetInterestedRegionLabel(uint64_t id, uint8_t label)
{
	this->SetCellOccupancy(id, OccupancyType::INTERESTED);
	label_grid_->AssignLabel(id, label);
}

