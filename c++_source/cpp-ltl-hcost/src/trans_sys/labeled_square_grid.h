/*
 * labeled_square_grid.h
 *
 *  Created on: Nov 23, 2016
 *      Author: rdu
 */

#ifndef SRC_TRANS_SYS_LABELED_SQUARE_GRID_H_
#define SRC_TRANS_SYS_LABELED_SQUARE_GRID_H_

#include <task_region_grid.h>
#include <memory>

#include "map/square_grid.h"

namespace srcl {

class LabeledSquareGrid: public SquareGrid
{
public:
	LabeledSquareGrid(uint32_t row_num, uint32_t col_num, uint32_t cell_size, uint8_t default_label);
	~LabeledSquareGrid();

private:
	std::shared_ptr<TaskRegionGrid> label_grid_;

public:
	void SetObstacleRegionLabel(uint64_t id, uint8_t label);
	void SetInterestedRegionLabel(uint64_t id, uint8_t label);

};

}



#endif /* SRC_TRANS_SYS_LABELED_SQUARE_GRID_H_ */
