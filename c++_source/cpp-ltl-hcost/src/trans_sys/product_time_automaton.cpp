/*
 * product_time_automaton.cpp
 *
 *  Created on: 23 Nov 2016
 *      Author: zetian
 */

#include "trans_sys/product_time_automaton.h"

#include <iostream>
#include <sstream>
#include <cstring>
#include <cstdint>
#include <map>
#include <vector>
#include <algorithm>
#include <bitset>

using namespace srcl;

uint64_t ProductTimeAutomaton::SetVirtualStartIncre(Vertex<SquareCell *> * start_node_origin, std::shared_ptr<Graph_t<LiftedSquareCell>> lifted_graph, std::shared_ptr<Graph_t<BuchiState>> buchi_graph, std::shared_ptr<Graph<ProductTimeState>> product_time_graph){
	uint64_t start_time_step = 0;
	std::vector<Vertex<LiftedSquareCell>*> start_node_lifted;
	for (auto it = start_node_origin->lifted_vertices_id_.begin();it != start_node_origin->lifted_vertices_id_.end(); it++){
		start_node_lifted.push_back(lifted_graph->GetVertexFromID((*it)));
//		for(auto i=0; i<lifted_graph->GetVertexFromID((*it))->bundled_data_.history.size();i++){
//		std::cout << lifted_graph->GetVertexFromID((*it))->bundled_data_.history[i]->bundled_data_->GetID()<< "   ";
//		}
//		std::cout << "history" << std::endl;
	}

	auto init_buchi_idx_ = buchi_graph->GetVertexFromID(0)->bundled_data_.init_state_idx_;
	std::vector<Vertex<BuchiState>*> buchi_vertex = buchi_graph->GetGraphVertices();
	int num_buchi_states = buchi_vertex.size();
	std::vector<uint32_t> alphabet_set =buchi_graph->GetVertexFromID(0)->bundled_data_.alphabet_set ;
	//std::vector<Vertex<ProductState>*> start_product;
	std::vector<ProductTimeState> start_product;

	for(auto it = start_node_lifted.begin(); it != start_node_lifted.end(); it++){
		uint32_t RegionBitMap = (*it)->bundled_data_.history[0]->bundled_data_->GetTaskRegionBitMap(start_time_step);
		for (int i = 1; i < (*it)->bundled_data_.history.size(); i++){
			auto bitmap = (*it)->bundled_data_.history[i]->bundled_data_->GetTaskRegionBitMap(start_time_step);
			RegionBitMap = RegionBitMap|bitmap;
		}
		for (auto ite = buchi_vertex.begin();ite != buchi_vertex.end();ite++){
			std::vector<uint32_t> buchi_transition = buchi_graph->GetVertexFromID(init_buchi_idx_)->GetBuchiTransition(*ite);
			for(auto it_buchi_trans = buchi_transition.begin(); it_buchi_trans != buchi_transition.end(); it_buchi_trans++) {
				if (alphabet_set[(*it_buchi_trans)] == RegionBitMap){
					auto product_time_start_id = (*it)->vertex_id_*num_buchi_states + (*ite)->vertex_id_;

					ProductTimeState product_time_start(product_time_start_id);
					product_time_start.buchi_vertex_ = *ite;
					product_time_start.lifted_vertex_ = *it;
					product_time_start.time_step_ = 0;
					start_product.push_back(product_time_start);
				}
			}
		}
	}
	uint64_t virtual_start_id = buchi_graph->GetGraphVertices().size()*lifted_graph->GetGraphVertices().size();

	ProductTimeState virtual_start(virtual_start_id);

	virtual_start.buchi_vertex_ = buchi_graph->GetVertexFromID(init_buchi_idx_);
	for (auto it = start_product.begin(); it!=start_product.end();it++){
		product_time_graph->AddEdge(virtual_start,*it, 0);
	}

	return virtual_start_id;
}


std::vector<std::tuple<ProductTimeState, double>> GetProductTimeNeighbour::operator()(ProductTimeState cell){
	std::vector<std::tuple<ProductTimeState, double>> adjacent_cells;
	std::vector<uint32_t> alphabet_set = buchi_graph_->GetVertexFromID(0)->bundled_data_.alphabet_set;
	std::vector<uint32_t> buchi_transition;
	Vertex<SquareCell*>* last_cell;

	uint32_t cell_region_bit_map;
	uint64_t node_id_new;
	uint64_t node_id_static;
	double edge_cost;
	uint64_t current_time_step = cell.time_step_;
	uint64_t new_time_step = current_time_step + 1;
	Vertex<LiftedSquareCell>* current_lifted_vertex = cell.lifted_vertex_;
	uint64_t current_lifted_id = current_lifted_vertex->vertex_id_;
	Vertex<BuchiState>* current_buchi_vertex = cell.buchi_vertex_;
	std::vector<Vertex<LiftedSquareCell>*> neighbour_from_lift = current_lifted_vertex->GetNeighbours();
	std::vector<Vertex<BuchiState>*> buchi_vertex = buchi_graph_->GetGraphVertices();
	std::vector<Vertex<LiftedSquareCell>*> lifted_graph = lifted_graph_->GetGraphVertices();

	SquareCell* current_grid = cell.lifted_vertex_->bundled_data_.history.back()->bundled_data_;



	int num_buchi_states = buchi_vertex.size();
	int num_lifted_states = lifted_graph.size();
	int max_num_state = num_buchi_states*num_lifted_states;
	neighbour_from_lift.push_back(current_lifted_vertex);

	for (auto it_lifted_neighbour = neighbour_from_lift.begin(); it_lifted_neighbour!= neighbour_from_lift.end(); it_lifted_neighbour++){
		uint64_t neighbour_grid_id = (*it_lifted_neighbour)->bundled_data_.history.back()->bundled_data_->data_id_;
		if (current_grid->CheckCollision(neighbour_grid_id, current_time_step)){
			continue;
		}
		for (auto it_buchi = buchi_vertex.begin();it_buchi != buchi_vertex.end();it_buchi++){
			buchi_transition = current_buchi_vertex->GetBuchiTransition(*it_buchi);
			last_cell = (*it_lifted_neighbour)->bundled_data_.history.back();
			cell_region_bit_map = last_cell->bundled_data_->GetTaskRegionBitMap(new_time_step);
			for(auto it_buchi_trans = buchi_transition.begin(); it_buchi_trans != buchi_transition.end(); it_buchi_trans++) {
				if ((*it_buchi_trans)>alphabet_set.size()-1)
					continue;
				if (alphabet_set[(*it_buchi_trans)] == cell_region_bit_map){
//					edge_cost = 2;
					if ((*it_lifted_neighbour)->vertex_id_==current_lifted_id){
						edge_cost=1;
					}
					else{
						edge_cost = 2;
					}
					node_id_new = new_time_step*max_num_state + (*it_lifted_neighbour)->vertex_id_*num_buchi_states + (*it_buchi)->vertex_id_;
					ProductTimeState new_product_time_state(node_id_new);
					new_product_time_state.lifted_vertex_ = *it_lifted_neighbour;
					new_product_time_state.buchi_vertex_ = *it_buchi;
					new_product_time_state.time_step_ = new_time_step;
//					new_product_time_state.product_vertex_ = new_product_state;
					adjacent_cells.emplace_back(new_product_time_state, edge_cost);
					break;
				}
			}
		}
	}
	return adjacent_cells;
}
