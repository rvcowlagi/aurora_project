function formula = read_formula(alphabet,orig_alph)
%read an LTL formula and replace names of propositions (px -> p0..0x)

N_p=length(alphabet)-1; %number of defined atomic props
%formula=input(sprintf('\nLTL formula involving all %d atomic propositions (use parantheses, spaces and !,&,|,F,G,U,R):\n',N_p),'s');    %read formula as a string
formula=input(sprintf('\nLTL_x (without "next" operator) formula (between apostrophes) involving all %d atomic propositions:\n',N_p));    %read formula
for i=N_p:-1:1 %replace props px with p0...0x (start with the last one) - to not confund p1 with p1x, ...
    formula=regexprep(formula,['\<' orig_alph{i} '\>'],alphabet{i});    %sintax \< , \> for exact match
end
%formula=regexprep(formula,orig_alph,alphabet(1:end-1));    %bad sintax forr more than 10 props

if ~isempty(find(cellfun('isempty',regexp(formula,alphabet(1:N_p)))))  %test if all props from alphabet(1:end-1) (without dummy one) are in formula
    fprintf('\nWarning - Not all defined atomic propositions are in the formula')
end %there is no test for atomic props that are not defined and appear in formula, nor for well-formulated formula
