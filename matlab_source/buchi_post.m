function [buchi_rank, buchi_aut] = buchi_post(Alph_s, buchi_aut,indep_events)
index = 1;
for i = 1:length(indep_events)
    for j=i+1:length(indep_events)
        bad_comb{index} =  [indep_events{[i,j]}];
        index = index + 1;
    end
end
index = 1;
for i = 1:length(Alph_s)
    for j = 1:length(bad_comb)
        if strfind(Alph_s{i},bad_comb{j})
            bad_act(index) = i;
            index = index + 1;
            break;
        end
    end
end
for i = 1:length(buchi_aut.S)
    for j = 1:length(buchi_aut.S)
        [~,id,~] = intersect(buchi_aut.trans{i,j},bad_act);
%         if isempty(id)
%             continue;
%         end
        buchi_aut.trans{i,j}(id)=[];
    end
end
num_buchi = length(buchi_aut.S);
G_buchi = zeros(num_buchi, num_buchi);
G_buchi_trans = buchi_aut.trans;
G_buchi_trans = G_buchi_trans';
for i = 1:num_buchi
    for j = 1:num_buchi
        if ~isempty(G_buchi_trans{i,j})
            G_buchi(i,j)=1;
        end
    end
end
nodeS = buchi_aut.F;
% nodeG = buchi_aut.S0;

nodeData_buchi = rank_buchi(G_buchi, nodeS, zeros(num_buchi, 1));


for i = 1:num_buchi
    if nodeData_buchi(i).d==0
        buchi_rank(i) = 0;
    else
        buchi_rank(i) = nodeData_buchi(i).d - 1;
    end
end

for i = num_buchi:-1:1
    if buchi_rank(i)==inf
        buchi_rank(i)=[];
        buchi_aut.trans(i,:)=[];
        buchi_aut.trans(:,i)=[];
    end
end
buchi_aut.F = length(buchi_rank);
buchi_aut.S = [1:1:length(buchi_rank)];