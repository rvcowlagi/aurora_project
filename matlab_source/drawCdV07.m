function drawCdV07(VCell, myAxes, myCol, myWd, txt, someCells, colFace, ...
	imgSize, map, altTxtString, altTxtSize, altTxtColor)

axes(myAxes); hold on;
if (nargin <= 5) || (numel(someCells) == 0)
	drawCells = 1:size(VCell, 1);
else
	drawCells = someCells;
end

if (nargin > 6) && (numel(colFace) == 1) && (colFace == 0)
	Z	= zeros(imgSize);
	for n = 1:size(VCell,1)
		Z((VCell(n,2) + 1):(VCell(n,2) + VCell(n,3)), ...
			(VCell(n,1) + 1):(VCell(n,1) + VCell(n,3))) = VCell(n,4);
	end
	image(Z, 'XData', 0.5, 'YData', 0.5); colormap(map);
end

for m = drawCells
	if (nargin > 6) && ((numel(colFace) ~= 1) || (colFace ~= 0))
		rectangle('Position', [VCell(m,1) VCell(m,2) VCell(m,3) ...
			VCell(m,4)], 'EdgeColor', myCol, 'LineWidth', myWd, ...
			'FaceColor', colFace);
	else
		rectangle('Position', [VCell(m,1) VCell(m,2) VCell(m,3) ...
			VCell(m,4)], 'EdgeColor', myCol, 'LineWidth', myWd);
	end
	if txt
		text(VCell(m,1) + 0.2*VCell(m,3), ...
			VCell(m,2) + 0.2*VCell(m,3), num2str(m), 'FontName', ...
			'Consolas', 'FontSize', txt, ...
			'FontWeight', 'bold', 'Color', 'k');
	end
	if nargin > 9
		text(VCell(m,1) + 0.2*VCell(m,3), ...
			VCell(m,2) + 0.2*VCell(m,3), altTxtString, 'FontName', ...
			'Consolas', 'FontSize', altTxtSize, ...
			'FontWeight', 'bold', 'Color', altTxtColor);
	end
end
drawnow;