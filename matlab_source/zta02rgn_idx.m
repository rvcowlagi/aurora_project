function rgn_idx_init = zta02rgn_idx(zta0)
location = zta0(1);
angle = zta0(2);
global all_tile_traversal_data
region = all_tile_traversal_data.REGION_BD;
[~,region_n] = size(region);
for k = 1:region_n
    if (location >= region(1,k) && location < region(2,k) && angle >= region(3,k) && angle < region(4,k))
        rgn_idx_init = k - 1;
    end
end

