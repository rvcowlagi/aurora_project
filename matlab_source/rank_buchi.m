% *************************************************************************
% Author:		Raghvendra V. Cowlagi
%				Dept. of Aerospace Engineering, Georgia Inst. Tech.
% Description:	A* algorithm
% Last Mod.:	10/17/2010
% Version:		Handles mmultiple goals when cost-to-come for each goal is
%				required
% *************************************************************************
function nodeData = rank_buchi(G, nodeS, heur)
% G	: Trasition cost matrix
% nodeS		: Start node
% nodeG		: Goal node set
% heur	: Heuristic (N x 1 vector, where N is number of nodes)

%	nodeData(n).mk	= marker, 0 = NEW, 1 = OPEN, 2 = CLOSED
%	nodeData(n).d	= cost to come
%	nodeData(n).b	= backpointer

nNodes		= size(G, 1);
nodeStruct	= struct('mk', 0, 'd', Inf, 'b', 0);
nodeData	= repmat(nodeStruct, 1, nNodes);
nodeData(nodeS).mk	= 1;	nodeData(nodeS).d	= 0;

% nodeData.path = [];

nOpen	= 1;
openL	= [nodeS heur(nodeS)];
goalCl	= 0;

nIter	= 0;
while (nOpen ~= 0) && (~goalCl)
	nIter	= nIter + 1;
	nodeAct	= openL(1, 1);													% Get node from top of (sorted) open stack
	nodeData(nodeAct).mk = 2;												% Mark that node as dead
	
	nOpen		= nOpen - 1;
	openL(1, :) = [];
	
	nhbrs	= find(G(nodeAct,:));
	for nodeNew = nhbrs														% For all neighbors		
		newCost	= G(nodeAct,nodeNew);										% Cost to go from act to new
		
		if nodeData(nodeNew).mk == 0										% Unvisited
			nodeData(nodeNew).mk	= 1;									% Mark open
			nodeData(nodeNew).d		= nodeData(nodeAct).d + newCost;		% Update c2come of newly visited state
			nodeData(nodeNew).b		= nodeAct;
			
			tmpOpen = binSort(openL(1:nOpen, :), [nodeNew nodeData(nodeNew).d + heur(nodeNew)], 2);
			if numel(tmpOpen) == 0
				nOpen	= 0;
				openL	= [];
			else
				nOpen	= size(tmpOpen, 1);
				openL(1:nOpen, :)	= tmpOpen;								% Add [nodeNew cost] to sorted open list
			end			
		elseif nodeData(nodeNew).mk == 1									% Already open, update c2come if necessary
			if nodeData(nodeNew).d > nodeData(nodeAct).d + newCost
				nodeData(nodeNew).d	= nodeData(nodeAct).d + newCost;
				nodeData(nodeNew).b	= nodeAct;
				
				[~, loc] = ismember(nodeNew, openL(1:nOpen, 1));
				openL(loc, :)= [];		nOpen = nOpen - 1;
				
				tmpOpen = binSort(openL(1:nOpen, :), [nodeNew nodeData(nodeNew).d + heur(nodeNew)], 2);
				if numel(tmpOpen) == 0
					nOpen	= 0;
					openL	= [];
				else
					nOpen	= size(tmpOpen, 1);
					openL(1:nOpen, :)	= tmpOpen;							% Add [nodeNew cost] to sorted open list
				end
			end
		end
    end   
end
