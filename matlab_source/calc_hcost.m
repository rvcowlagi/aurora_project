%{
Copyright (c) 2014 Raghvendra V. Cowlagi. All rights reserved.

Copyright notice:
=================
No part of this work may be reproduced without the written permission of
the copyright holder, except for non-profit and educational purposes under
the provisions of Title 17, USC Section 107 of the United States Copyright
Act of 1976. Reproduction of this work for commercial use is a violation of
copyright.


Disclaimer:
===========
This software program is intended for educational and research purposes.
The author and the institution with which the author is affiliated are not
liable for damages resulting the application of this program, or any
section thereof, which may be attributed to any errors that may exist in
this program.


Author information:
===================
Raghvendra V. Cowlagi, Ph.D,
Assistant Professor, Aerospace Engineering Program,
Department of Mechanical Engineering, Worcester Polytechnic Institute.
 
Higgins Laboratories, 247,
100 Institute Road, Worcester, MA 01609.
Phone: +1-508-831-6405
Email: rvcowlagi@wpi.edu
Website: http://www.wpi.edu/~rvcowlagi


The author welcomes questions, comments, suggestions for improvements, and
reports of errors in this program.


Program description:
====================
Preprocessing tiles for fast computation of H-costs during search.
%}

function all_tile_traversal_data = calc_hcost()

%{
Format for storing data
1. Tile library: one per H
	Variable 'tile_traversal_sequence' with fields 'H1', 'H2', ...
	Each field 'Hn' is an array with #columns = #unique tiles, and the
	opposite-adjacent sequence of H traversals is recored in each column.

2. Lookup table with binary costs: one table per H

3. Connectivity relation: one relation per H per tile
%}

%% Constants
MAX_H = 5;																	% Largest value of H that we care about

%----- Minimum and maximum speed considered
SPD_MIN	= 1;
SPD_MAX	= 1;

%----- Numbers of regions in distance, orientation, and velocity
%{
	Numbering scheme:
	N_REGION_VEL*N_REGION_PSI*k_vel + N_REGION_PSI*k_psi + k_w
	Region numbering must start at 0
%}
N_REGION_W		= 25;
N_REGION_PSI	= 25;
N_REGION_SPD	= 1;
N_REGION_TOTAL	= N_REGION_W*N_REGION_PSI*N_REGION_SPD - 1;

%----- Extent of these regions in distance, orientation, and velocity
SIZE_REGION_W	= 1 / N_REGION_W;											% Size of cells is 1 unit
SIZE_REGION_PSI	= pi / N_REGION_PSI;										% Regions in [-pi/2, pi/2]
SIZE_REGION_SPD	= (SPD_MAX - SPD_MIN)/N_REGION_SPD;

%----- Region boundaries
% REGION_BD= [w_lower; w_upper; psi_lower; psi_upper; vel_lower; vel_upper]
REGION_BD	= zeros(6, N_REGION_TOTAL+1);
for m1 = 0:N_REGION_TOTAL
	k_w		= mod( mod(m1, N_REGION_SPD*N_REGION_PSI), N_REGION_PSI);
	k_psi	= floor( (mod(m1, N_REGION_W*N_REGION_PSI)) / N_REGION_PSI );
	k_vel	= floor( m1/N_REGION_PSI/N_REGION_W );
	
	region_w_lower	= k_w*SIZE_REGION_W;
	region_w_upper	= (k_w + 1)*SIZE_REGION_W;
	region_psi_lower= -pi/2 + k_psi*SIZE_REGION_PSI;
	region_psi_upper= -pi/2 + (k_psi + 1)*SIZE_REGION_PSI;
	region_vel_lower= SPD_MIN + k_vel*SIZE_REGION_SPD;
	region_vel_upper= SPD_MIN + (k_vel + 1)*SIZE_REGION_SPD;
	
	REGION_BD(:, m1 + 1)= [...
		region_w_lower; region_w_upper; region_psi_lower; ...
		region_psi_upper; region_vel_lower; region_vel_upper];
end

%----- CBTA numerical parameters
N_CBTA_W_SOL= 51;
N_CBTA_W	= 101;


%% Uniform cell decomposition template
n_row= 12;
n_col= 12;

verts_cd	= [];
for m2 = 1:n_row
    for m1 = 1:n_col
        verts_cd = cat(1, verts_cd, [m1-1 m2-1 1 1]);
    end
end
graph_cd	= get_adjacency_matrix_4conn(verts_cd);


%% Tile library
n_subplot = [2 2 3 4 6];
for H = 1:MAX_H %**********************
	field_name	= ['H' num2str(H)];
% 	figure('Name', field_name);
	
	unique_tiles= zeros(H, 1);												% Array to store tile traversals for this H
	n_tiles		= 0;
	all_hist	= get_histories(graph_cd, 1, H+2, 0, 1, []);
	
	% Find unique traversals
	for m1 = 1:size(all_hist, 1)
		tile_data	= get_tile_data(H, verts_cd(all_hist(m1, :), :));
		is_new_tile	= false;
		if m1 == 1
			unique_tiles(:, 1)	= tile_data.traversal_type;			
			is_new_tile			= true;
			
% 			subplot(n_subplot(H), n_subplot(H), size(unique_tiles, 2));
% 			draw_cells(verts_cd, gca, 'k', 2, [], all_hist(m1, :));
% 			axis equal;
% 			text(0.1, 1.5, ['Tile #' num2str(n_tiles + 1)])
		else
			if ~ismember(tile_data.traversal_type', unique_tiles', 'rows')
				unique_tiles= cat(2, unique_tiles, tile_data.traversal_type);				
				is_new_tile	= true;
				
% 				subplot(n_subplot(H), n_subplot(H), size(unique_tiles, 2));
% 				draw_cells(verts_cd, gca, 'k', 2, [], all_hist(m1, :));
% 				axis equal;
% 				text(0.1, 1.5, ['Tile #' num2str(n_tiles + 1)])
			end
		end
		
		if is_new_tile
			n_tiles				= n_tiles + 1;
			this_tile_name		= ['tile' num2str(n_tiles)];
			all_tile_traversal_data.(field_name).(this_tile_name).channel_data		= tile_data.channel_data;
			all_tile_traversal_data.(field_name).(this_tile_name).cell_vertices		= tile_data.cell_vertices;
			all_tile_traversal_data.(field_name).(this_tile_name).traversal_type	= tile_data.traversal_type;
			all_tile_traversal_data.(field_name).(this_tile_name).cell_xform		= tile_data.cell_xform;
			all_tile_traversal_data.(field_name).(this_tile_name).traversal_faces	= tile_data.traversal_faces;
			all_tile_traversal_data.(field_name).(this_tile_name).cell_edge			= tile_data.cell_edge;
		end
	end	
	
	all_tile_traversal_data.(field_name).tiles	= unique_tiles;
	all_tile_traversal_data.(field_name).n_tiles= n_tiles;
end
all_tile_traversal_data.N_REGION_W		= N_REGION_W;
all_tile_traversal_data.N_REGION_PSI	= N_REGION_PSI;
all_tile_traversal_data.N_REGION_SPD	= N_REGION_SPD;
all_tile_traversal_data.N_REGION_TOTAL	= N_REGION_TOTAL;
all_tile_traversal_data.REGION_BD		= REGION_BD;


%% Connectivity relation
for H = 1:MAX_H%*******************************
	field_name	= ['H' num2str(H)];	
	for tile_idx = 1:all_tile_traversal_data.(field_name).n_tiles 
		this_tile_name		= ['tile' num2str(tile_idx)];
		this_tile_edge_list = zeros(N_REGION_TOTAL, 3);						% An approximate number of expected edges in the connectivity
		n_this_tile_edges	= 0;
		
		this_tile_data	= all_tile_traversal_data.(field_name).(this_tile_name);
		r_min			= 2; %** FIX**
		cbta_results	= cbta();
		
		%----- Find grid regions at end of second cell from where traversal
		% throughout is possible (everywhere -> somewhere)
		% These are the Q sets described in the paper(s), we are
		% identifying a "discretized" approximation to it.
		% Called "cbta_target_2" and "cbta_target_1" (target sets for first
		% and second cells)
		if H == 1															% The Q set is "complete" (all regions are in it).			
			cbta_target_2.alfa	= ...
				[pi/2*ones(1, N_CBTA_W); -pi/2*ones(1, N_CBTA_W)];
			cbta_target_2.w		= linspace(0, 1, N_CBTA_W);
		else
			cbta_target_2.alfa	= cbta_results.alfa(3:4, :);
			cbta_target_2.w		= cbta_results.w(2, :);
		end
		cbta_target_1.alfa	= cbta_results.alfa(1:2, :);
		cbta_target_1.w		= cbta_results.w(1, :);
		
		region_target_2		= ones(1, N_REGION_TOTAL + 1);					% Intersection with CBTA computed target		
		for m1 = 0:N_REGION_TOTAL
			region_w_lower	= REGION_BD(1, m1 + 1);
			region_w_upper	= REGION_BD(2, m1 + 1);
			region_psi_lower= REGION_BD(3, m1 + 1);
			region_psi_upper= REGION_BD(4, m1 + 1);
			
			if (region_w_lower > cbta_target_2.w(end)) || ...
					(region_w_upper < cbta_target_2.w(1)), continue; end
			
			region_w_lower_idx = find_sample(cbta_target_2.w, region_w_lower);
			region_w_upper_idx = find_sample(cbta_target_2.w, region_w_upper);
			
			if all( (cbta_target_2.alfa(1, region_w_lower_idx:region_w_upper_idx) < region_psi_lower) | ...
					(cbta_target_2.alfa(2, region_w_lower_idx:region_w_upper_idx) > region_psi_upper) )
				region_target_2(m1 + 1) = 0;
			end	
		end
		
% % 		%******************
% % 		figure('Name', 'Region 2'); set(gcf, 'Position', get(0,'Screensize'));
% % 		plot(cbta_target_1.w, cbta_target_2.alfa(1, :)*180/pi, 'b', ...
% % 			cbta_target_1.w, cbta_target_2.alfa(2, :)*180/pi, 'b');
% % 		hold on; grid on; xlim([0 1]); ylim([-90 90]);
% % 		set(gca,'Xtick',0:SIZE_REGION_W:1);
% % 		set(gca,'Ytick', -90:(SIZE_REGION_PSI*180/pi):90)
% % 		for m22 = 0:N_REGION_TOTAL
% % 			region_w_lower	= REGION_BD(1, m22 + 1);			
% % 			region_psi_lower= REGION_BD(3, m22 + 1);
% % 		if region_target_2(m22 + 1)
% % 				text(region_w_lower, (region_psi_lower + 0.2*SIZE_REGION_PSI)*180/pi, num2str(m22), 'Color', 'r');
% % 			else
% % 				text(region_w_lower, (region_psi_lower + 0.2*SIZE_REGION_PSI)*180/pi, num2str(m22), 'Color', 'k');
% % 			end
% % 		end
% % 		%******************
		
		region_target_1		= zeros(1, N_REGION_TOTAL + 1);					% Inclusion under CBTA computed target
		for m1 = 0:N_REGION_TOTAL
			region_w_lower	= REGION_BD(1, m1 + 1);
			region_w_upper	= REGION_BD(2, m1 + 1);
			region_psi_lower= REGION_BD(3, m1 + 1);
			region_psi_upper= REGION_BD(4, m1 + 1);
			
			if (region_w_lower < cbta_target_1.w(1)) || ...
					(region_w_upper > cbta_target_1.w(end)), continue; end
			
			region_w_lower_idx = find_sample(cbta_target_1.w, region_w_lower);
			region_w_upper_idx = find_sample(cbta_target_1.w, region_w_upper);
			
			if all( (cbta_target_1.alfa(1, region_w_lower_idx:region_w_upper_idx) >= region_psi_upper) & ...
					(cbta_target_1.alfa(2, region_w_lower_idx:region_w_upper_idx) <= region_psi_lower) )
				region_target_1(m1 + 1) = 1;
			end
		end
		
% % 		%******************
% % 		figure('Name', 'Region 1'); set(gcf, 'Position', get(0,'Screensize'));
% % 		plot(cbta_target_1.w, cbta_target_1.alfa(1, :)*180/pi, 'b', ...
% % 			cbta_target_1.w, cbta_target_1.alfa(2, :)*180/pi, 'b');
% % 		hold on; grid on; xlim([0 1]); ylim([-90 90]);
% % 		set(gca,'Xtick',0:SIZE_REGION_W:1);
% % 		set(gca,'Ytick', -90:(SIZE_REGION_PSI*180/pi):90)
% % 		for m22 = 0:N_REGION_TOTAL
% % 			region_w_lower	= REGION_BD(1, m22 + 1);
% % 			region_psi_lower= REGION_BD(3, m22 + 1);
% % 			if region_target_1(m22 + 1)
% % 				text(region_w_lower, (region_psi_lower + 0.2*SIZE_REGION_PSI)*180/pi, num2str(m22), 'Color', 'r');
% % 			else
% % 				text(region_w_lower, (region_psi_lower + 0.2*SIZE_REGION_PSI)*180/pi, num2str(m22), 'Color', 'k');
% % 			end
% % 		end
% % 		%******************
		
		
		
		%----- Find grid regions at the end of first cell from where
		% traversal is possible (everywhere -> somewhere), call this Q_0
		%----- For each of these, figure out reachable grid regions
		%----- Intersect these grid regions with Q, and get the "to"
		% regions in the connectivity graph
		for m1 = 0:N_REGION_TOTAL
			%--- If this region not in Q_0, then do nothing
			if ~region_target_1(m1 + 1), continue; end
			
% 			m1
			region_neighbors	= cbra(m1);
% 			tmp = region_neighbors'
			%--- Intersect reachable region with Q, record edge
			this_tile_edge_list( (n_this_tile_edges + 1):(n_this_tile_edges ...
				+ numel(region_neighbors)), 1:3) = ...
				[m1*ones(numel(region_neighbors), 1) region_neighbors ...
				ones(numel(region_neighbors), 1)];
			n_this_tile_edges	= n_this_tile_edges + numel(region_neighbors);
			
% % % 			%******************
% % % 			figure('Name', 'Region 2'); set(gcf, 'Position', get(0,'Screensize'));
% % % 			plot(cbta_target_2.w, cbta_target_2.alfa(1, :)*180/pi, 'b', ...
% % % 				cbta_target_2.w, cbta_target_2.alfa(2, :)*180/pi, 'b');
% % % 			hold on; grid on; xlim([0 1]); ylim([-90 90]);
% % % 			set(gca,'Xtick',0:SIZE_REGION_W:1);
% % % 			set(gca,'Ytick', -90:(SIZE_REGION_PSI*180/pi):90)
% % % 			for m22 = 0:N_REGION_TOTAL
% % % 				region_w_lower	= REGION_BD(1, m22 + 1);			
% % % 				region_psi_lower= REGION_BD(3, m22 + 1);
% % % 				if any(m22 == region_neighbors)
% % % 					text(region_w_lower, (region_psi_lower + 0.2*SIZE_REGION_PSI)*180/pi, num2str(m22), 'Color', 'r');
% % % % 				else
% % % % 					text(region_w_lower, (region_psi_lower + 0.2*SIZE_REGION_PSI)*180/pi, num2str(m22), 'Color', 'k');
% % % 				end
% % % 			end
% % % 			%******************
			
% % % 			drawnow();
		end
				
		all_tile_traversal_data.(field_name).(this_tile_name).connectivity ...
			= sparse( (this_tile_edge_list(1:n_this_tile_edges, 1) + 1), ...
			(this_tile_edge_list(1:n_this_tile_edges, 2) + 1), ...
			this_tile_edge_list(1:n_this_tile_edges, 3), ...
			N_REGION_TOTAL + 1, N_REGION_TOTAL + 1);
	end
end

return

%% Functions
	%**********************************************************************
	function cbta_results = cbta()
		y_exit	= zeros(H,1);
		z_exit	= zeros(H,1);
		w_upper	= zeros(H,1);
		w_lower	= zeros(H,1);
		
		cell_vertices	= this_tile_data.cell_vertices;
		cell_edge		= this_tile_data.cell_edge;
		channel_data	= this_tile_data.channel_data;
		traversal_type	= this_tile_data.traversal_type;
		cell_xform		= this_tile_data.cell_xform;

		w_sol	= zeros(H, N_CBTA_W_SOL);									% Points on the entry segment, to be solved for
		w_smp	= zeros(H, N_CBTA_W);										% Points on the entry segment, to be recorded
		x_smp	= zeros(H, N_CBTA_W);										% Points on the exit segment
		bta_smp	= zeros(2*H, N_CBTA_W);										% alfa_smp measured in cell axes system, not global axes
		bta_smp((2*H-1):(2*H),:)= [pi/2*ones(1, N_CBTA_W); -pi/2*ones(1, N_CBTA_W)];

		alfa_sol				= zeros(2*H, N_CBTA_W_SOL);
		alfa_smp(1:2:(2*H-1),:) = -Inf(H, N_CBTA_W);
		alfa_smp(2:2:2*H, :)	= Inf(H, N_CBTA_W);
		
		
		for n = 1:H
			switch traversal_type(n)
				case 1														% Measure distance from C for y, z
					y1	= norm(cell_vertices(3, (2*n - 1):(2*n)) - cell_edge(n+1, 1:2));
					y2	= norm(cell_vertices(3, (2*n - 1):(2*n)) - cell_edge(n+1, 3:4));
				case 2														% Measure distance from D for y, z
					y1	= norm(cell_vertices(4, (2*n - 1):(2*n)) - cell_edge(n+1, 1:2));
					y2	= norm(cell_vertices(4, (2*n - 1):(2*n)) - cell_edge(n+1, 3:4));
			end
			y_exit(n)	= (min(y1, y2));	z_exit(n)	= (max(y1, y2));
		end
		
		
		for n = 1:H
			w1	= norm(cell_vertices(4, (2*n - 1):(2*n)) - cell_edge(n, 1:2));	% Measure distance from D for wL, wU
			w2	= norm(cell_vertices(4, (2*n - 1):(2*n)) - cell_edge(n, 3:4));
			w_lower(n)	= (min(w1, w2));
			w_upper(n)	= (max(w1, w2));
		end
		for n = 1:H
			if traversal_type(n) == 2		
				w_sol_min	= max(w_lower(n), 1e-3);
			else
				w_sol_min	= w_lower(n);
			end
			w_sol(n,:)	= linspace(w_sol_min, w_upper(n), N_CBTA_W_SOL);
			w_smp(n, :)	= linspace(w_lower(n), w_upper(n), N_CBTA_W);
			x_smp(n, :)	= linspace(y_exit(n), z_exit(n), N_CBTA_W);
		end
		
		if traversal_type(H) == 1
			fcn_cone = @cbta_s1;
		else
			fcn_cone = @cbta_s2;
		end
		for q = 1:N_CBTA_W_SOL
			w	= w_sol(H, q);	
			alfa= fcn_cone(w, channel_data(H,1), x_smp(H,:), bta_smp((2*H-1):(2*H), :));
			alfa_sol((2*H-1):(2*H), q)	= alfa';		
		end

		% --------- Interpolate solved alfa over the whole grid
		alfa_smp(((2*H-1):(2*H)), :) = interp_broken_seg(w_sol(H, :), ...
			alfa_sol(((2*H-1):(2*H)), :), w_smp(H, :));

		for p = (H-1):-1:1
			% ------- Transform alfa of previous to get bta for current
			if (ismember(3, cell_xform(p + 1, :)) && ~ismember(4, cell_xform(p + 1, :))) ...
					|| (~ismember(3, cell_xform(p + 1, :)) && ismember(4, cell_xform(p + 1, :)))	% If flipped once
				bta_smp((2*p-1):(2*p), :) = fliplr(-flipud(alfa_smp((2*p+1):(2*p+2), :)));
			else
				bta_smp((2*p-1):(2*p), :) = alfa_smp((2*p+1):(2*p+2), :);
			end
			if (ismember(3, cell_xform(p, :)) && ~ismember(4, cell_xform(p, :))) ...
					|| (~ismember(3, cell_xform(p, :)) && ismember(4, cell_xform(p, :)))			% If flipped once

				bta_smp((2*p-1):(2*p), :) = fliplr(-flipud(bta_smp((2*p-1):(2*p), :)));
			end

			% ------- Solve for target sets
			if traversal_type(p) == 1
				fcn_cone = @cbta_s1;
			else
				fcn_cone = @cbta_s2;
			end

			for q = 1:N_CBTA_W_SOL		
				w	= w_sol(p, q);	
				alfa= fcn_cone(w, channel_data(p,1), x_smp(p,:), bta_smp((2*p-1):(2*p), :));
				alfa_sol((2*p-1):(2*p), q)	= alfa';
			end

			% ------- Interpolate over all x		
			alfa_smp(((2*p-1):(2*p)), :) = ...
				interp_broken_seg(w_sol(p, :), ...
				alfa_sol(((2*p-1):(2*p)), :), w_smp(p, :));
		end

		cbta_results.alfa	= alfa_smp;
		cbta_results.bta	= bta_smp;
		cbta_results.w_lower= w_lower;
		cbta_results.w_upper= w_upper;
		cbta_results.x		= x_smp;
		cbta_results.w		= w_smp;
		cbta_results.w_sol	= w_sol;		
	end
	%**********************************************************************
	
	%**********************************************************************
	function y_interp = interp_broken_seg(x_data, y_data, x_interp)
		n_poly_order	= 5;
		[~, y_data_wo_inf]	= remove_inf_values(y_data(1,:));		
		n_poly_order= min(n_poly_order, (numel(y_data_wo_inf) - 1));
		
		if numel(y_data_wo_inf)
			idx1	= find(x_interp >= x_data(y_data_wo_inf(1)), 1, 'first');
			idx2	= find(x_interp <= x_data(y_data_wo_inf(end)), 1, 'last');
			y_interp= [-Inf(1, numel(x_interp)); Inf(1, numel(x_interp))];	
			y_poly1	= polyfit(x_data(y_data_wo_inf), ...
				y_data(1, y_data_wo_inf), n_poly_order);
			y_poly2	= polyfit(x_data(y_data_wo_inf), ...
				y_data(2, y_data_wo_inf), n_poly_order);
			
			y_interp(:, idx1:idx2) = [polyval(y_poly1, x_interp(idx1:idx2)); ...
				polyval(y_poly2, x_interp(idx1:idx2))];
		else
			y_interp= kron([-Inf; Inf], ones(1, numel(x_interp)));
		end
	end
	%**********************************************************************
	
	%**********************************************************************
	function alfa = cbta_s1(w, d, xSmp, btaSmp)
		% -------- Max and min init angle possible
		alfaStL = -acos(1 - w/r_min);
		alfaStU = acos(1 - (d-w)/r_min);

		% -------- Intersect with segment with non-Inf bta
		[~, btaNotInfIndx] = remove_inf_values(btaSmp(1,:));
		if ~numel(btaNotInfIndx)
			alfa	= [-Inf; Inf];
			return;
		end

		% -------- Portion of exit segment reachable by C arcs alone
		% Uppermost point directly reachable by C- assumed to be B because r >= d,
		% and n2L = w + sqrt(2*r*d - d^2);
		% Lowermost point directly reachable by C+ assumed to be C because r >= d,
		% and n1U = w - sqrt(2*r*d - d^2);
		if r_min < (d^2 + w^2)/2/w
			n1L		= r_min - sqrt(r_min^2 - (d - r_min*sin(-alfaStL))^2);	% Lowermost point directly reachable by C-
			smpN1L	= min(max(find_sample(xSmp, n1L), btaNotInfIndx(1)), ...
				btaNotInfIndx(end));
		else
			smpN1L	= btaNotInfIndx(1);
		end

		if r_min < (d^2 + (d-w)^2)/2/(d-w)
			n2U		= r_min - d + sqrt(r_min^2 - (d - r_min*sin(alfaStU))^2);	% Uppermost point directly reachable by C+
			smpN2U	= max(min(find_sample(xSmp, n2U), btaNotInfIndx(end)), ...
				btaNotInfIndx(1));
		else
			smpN2U	= btaNotInfIndx(end);
		end

		if smpN2U <= smpN1L
			alfa	= [-Inf; Inf];
			return;
		end

		% -------- Calculate angles g+ and g-
		gamPSmp = zeros(1, numel(btaNotInfIndx));							% Lowest angle possible at X, by C+
		gamMSmp = zeros(1, numel(btaNotInfIndx));							% Highest angle possible at X, by C- 

		for m = btaNotInfIndx(1):(smpN1L-1)
			x	= xSmp(m);
			C0	= d^2 + (x-w)^2;
			C1	= sqrt(4*(r_min^2)*C0 - C0^2);
			C2	= -2*r_min*(x-w) - C0;
			gamP= 2*atan((2*r_min*d - C1)/C2);								% Angle at X of C+ arc from W to X

			gamM= acos(1 - x/r_min);										% Angle at X C- arc tangent to DC passing through X

			gamPSmp(m - btaNotInfIndx(1) + 1) = gamP;
			gamMSmp(m - btaNotInfIndx(1) + 1) = gamM;
		end

		for m = smpN1L:smpN2U
			x	= xSmp(m);
			C0	= d^2 + (x-w)^2;
			C1	= sqrt(4*(r_min^2)*C0 - C0^2);
			C2	= -2*r_min*(x-w) - C0;
			gamP= 2*atan((2*r_min*d - C1)/C2);								% Angle at X of C+ arc from W to X
		% 	gamM= pi2pi(pi + 2*atan((2*r*d + C1)/C2));						% Angle at X of C- arc from W to X
			gamM= pi + 2*atan((2*r_min*d + C1)/C2);							% Angle at X of C- arc from W to X
			if gamM > pi, gamM= gamM - 2*pi; end		

			gamPSmp(m - btaNotInfIndx(1) + 1) = gamP;
			gamMSmp(m - btaNotInfIndx(1) + 1) = gamM;
		end

		for m = (smpN2U+1):(btaNotInfIndx(end))
			x = xSmp(m);
			gamP= -acos(1 - (d-x)/r_min);									% Angle at X C+ arc tangent to AB passing through X

			C0	= d^2 + (x-w)^2;
			C1	= sqrt(4*(r_min^2)*C0 - C0^2);
			C2	= -2*r_min*(x-w) - C0;
			gamM= pi2pi(pi + 2*atan((2*r_min*d + C1)/C2));					% Angle at X of C- arc from W to X

			gamPSmp(m - btaNotInfIndx(1) + 1) = gamP;
			gamMSmp(m - btaNotInfIndx(1) + 1) = gamM;
		end

		% -------- Find critical points (works because of continuity)
		% numel(gamPSmp)
		% numel(btaNotInfIndx)
		diffPU	= gamPSmp - btaSmp(1,btaNotInfIndx);	diffPL	= gamPSmp - btaSmp(2,btaNotInfIndx);
		diffML	= gamMSmp - btaSmp(2,btaNotInfIndx);	diffMU	= gamMSmp - btaSmp(1,btaNotInfIndx);

		xPU		= find_zeros(diffPU);
		xML		= find_zeros(diffML);
		xC		= [0 sort([xPU xML]) numel(btaNotInfIndx)];

		% -------- Find type of interval
			% Note: because of continuity, one sample of each interval suffices to
			% figure out which "category" the whole interval belongs to
		intMark = [];
		for m = 1:(numel(xC)-1)
			if (diffPU(xC(m)+1) > 0) || (diffML(xC(m)+1) < 0)				% g+ > bta_U or g- < bta_L; no solution
				intMark = cat(2, intMark, 0);
			else
				intMark = cat(2, intMark, 1);
			end
		end

		% -------- Find largest interval in N1-N2
		if intMark(1), intsGood = [xC(1)+1 xC(2)]; else intsGood = []; end
		for m = 2:(numel(xC)-1)
			if intMark(m) && intMark(m-1)
				intsGood(end,2) = xC(m+1);
			elseif intMark(m) && ~intMark(m-1)
				intsGood		= cat(1, intsGood, [xC(m)+1 xC(m+1)]);
			end
		end
		if ~numel(intsGood)
			alfa	= [-Inf; Inf];
			return;
		end

		intsGood = cat(2, intsGood, intsGood(:,2) - intsGood(:,1));
		intsGood = sortrows(intsGood, 3);
		xIntMaxSmp1 = intsGood(1, 1);	xIntMaxSmp2 = intsGood(1, 2);

		% --------- Max and min of Ux' and Lx' on interval found above
		intSmp	= xIntMaxSmp1:xIntMaxSmp2;

		if ~numel(intSmp)
			alfa	= [-Inf; Inf];
			return;
		end

		thtaPSmp= zeros(numel(intSmp),1);
		thtaMSmp= zeros(numel(intSmp),1);

		if ~numel(intSmp)
			alfa = [-Inf; Inf];	
			return;
		end

		n = 0;
		for m = intSmp
			n = n + 1;
			x = xSmp(m + btaNotInfIndx(1) - 1);
			if diffPL(m) < 0
				bta	= btaSmp(2,m + btaNotInfIndx(1) - 1);
				C0	= d^2 + (x-w)^2;
				A	= sin(bta) - d/r_min;
				B	= cos(bta) + (x-w)/r_min;
				C	= -((x-w)*cos(bta)/r_min - d*sin(bta)/r_min + C0/(2*r_min^2) - 1);
				thtaP	= 2*atan((A + sqrt(A^2 + B^2 - C^2))/(B+C));		% Tangent angle of C+C- path at W
			else
				if (m + btaNotInfIndx(1) - 1) <= smpN2U
					thtaP	= gamMSmp(m);									% Tangent angle of C+ path at W
				else
					thtaP	= alfaStU;
				end
			end
			thtaPSmp(n) = thtaP;

			if diffMU(m) > 0
				bta = btaSmp(1,m + btaNotInfIndx(1) - 1);
				C0	= d^2 + (x-w)^2;
				A	= sin(bta) + d/r_min;
				B	= cos(bta) - (x-w)/r_min;
				C	= -(-(x-w)*cos(bta)/r_min + d*sin(bta)/r_min + C0/(2*r_min^2) - 1);
				thtaM	= 2*atan((A - sqrt(A^2 + B^2 - C^2))/(B+C));		% Tangent angle of C-C+ path at W
			else
				if (m + btaNotInfIndx(1) - 1) >= smpN1L
					thtaM	= gamPSmp(m);									% Tangent angle of C- path at W
				else
					thtaM	= alfaStL;
				end
			end
			thtaMSmp(n) = thtaM;
		end

		alfa(1) = max(thtaPSmp);
		alfa(2) = min(thtaMSmp);
	end
	%**********************************************************************
	
	%**********************************************************************
	function alfa =	cbta_s2(w, ~, xSmp, btaSmp)
		% -------- Intersect with segment with non-Inf bta
		[~, btaNotInfIndx] = remove_inf_values(btaSmp(1,:));
		if ~numel(btaNotInfIndx)
			alfa	= [-Inf; Inf];			
			return;
		end

		% -------- Portion of exit segment reachable by C arcs alone
		% Rightmost point directly reachable by C- assumed to be C
		% Leftmost point directly reachable by C+ assumed to be D 
		% Rightmost point directly reachable by C+ assumed to be C
		n1L		= r_min - sqrt(r_min^2 - w^2);								% Leftmost point directly reachable by C-
		smpN1L	= min(max((find_sample(xSmp, n1L)), btaNotInfIndx(1)), ...
			btaNotInfIndx(end));
		smpN2U	= btaNotInfIndx(end);

		if smpN2U <= smpN1L
			alfa	= [-Inf; Inf];			
			return;
		end

		% [smpN1L xSmp(smpN1L) n1L]
		% -------- Calculate angles g+ and g-
		gamPSmp = zeros(1, numel(btaNotInfIndx));							% Lowest angle possible at X, by C+
		gamMSmp = zeros(1, numel(btaNotInfIndx));							% Highest angle possible at X, by C- 

		for m = btaNotInfIndx(1):(smpN1L-1)
			x	= xSmp(m);
			C0	= x^2 + w^2;
			C1	= sqrt(4*(r_min^2)*C0 - C0^2);
			C2	= (2*r_min*x + C0);
			gamP= 2*atan((-2*r_min*w + C1)/C2);								% Angle at X of C+ arc from W to X	
			gamM= acos(1 - x/r_min);

			gamPSmp(m - btaNotInfIndx(1) + 1) = gamP;
			gamMSmp(m - btaNotInfIndx(1) + 1) = gamM;
		end

		for m = smpN1L:smpN2U
			x	= xSmp(m);
			C0	= x^2 + w^2;
			C1	= sqrt(4*(r_min^2)*C0 - C0^2);
			C2	= (2*r_min*x + C0);
			gamP= 2*atan((-2*r_min*w + C1)/C2);								% Angle at X of C+ arc from W to X
		% 	gamM= pi2pi(pi + 2*atan((-2*r*w -C1)/C2));						% Angle at X of C- arc from W to X
		% 	gamM= pi + 2*atan((-2*r*w -C1)/C2) - 2*pi;						% Angle at X of C- arc from W to X
			gamM= pi + 2*atan((-2*r_min*w -C1)/C2);
			if gamM > pi, gamM= gamM - 2*pi; end							% Angle at X of C- arc from W to X


			gamPSmp(m - btaNotInfIndx(1) + 1) = gamP;
			gamMSmp(m - btaNotInfIndx(1) + 1) = gamM;
		end

		% -------- Find critical points (works because of continuity)
		diffPU	= gamPSmp - btaSmp(1,btaNotInfIndx);	diffPL	= gamPSmp - btaSmp(2,btaNotInfIndx);
		diffML	= gamMSmp - btaSmp(2,btaNotInfIndx);	diffMU	= gamMSmp - btaSmp(1,btaNotInfIndx);

		xPU		= find_zeros(diffPU);
		xML		= find_zeros(diffML);
		xC		= [0 sort([xPU xML]) numel(btaNotInfIndx)];

		% -------- Find type of interval
			% Note: because of continuity, one sample of each interval suffices to
			% figure out which "category" the whole interval belongs to
		intMark = [];
		for m = 1:(numel(xC)-1)
			if (diffPU(xC(m)+1) > 0) || (diffML(xC(m)+1) < 0)				% g+ > bta_U or g- < bta_L; no solution
				intMark = cat(2, intMark, 0);
			else
				intMark = cat(2, intMark, 1);
			end
		end

		% -------- Find largest interval in N1-N2
		if intMark(1), intsGood = [xC(1)+1 xC(2)]; else intsGood = []; end
		for m = 2:(numel(xC)-1)
			if intMark(m) && intMark(m-1)
				intsGood(end,2) = xC(m+1);
			elseif intMark(m) && ~intMark(m-1)
				intsGood		= cat(1, intsGood, [xC(m)+1 xC(m+1)]);
			end
		end
		if ~numel(intsGood)
			alfa = [-Inf; Inf];
			return;
		end

		intsGood = cat(2, intsGood, intsGood(:,2) - intsGood(:,1));
		intsGood = sortrows(intsGood, 3);
		xIntMaxSmp1 = intsGood(1, 1);	xIntMaxSmp2 = intsGood(1, 2);		

		% --------- Max and min of Ux' and Lx' on interval found above
		intSmp	= xIntMaxSmp1:xIntMaxSmp2;

		if ~numel(intSmp)
			alfa	= [-Inf; Inf];
			return;
		end

		thtaPSmp= zeros(numel(intSmp),1);
		thtaMSmp= zeros(numel(intSmp),1);
		
		n = 0;
		for m = intSmp
			n = n + 1;
			x = xSmp(m + btaNotInfIndx(1) - 1);
			if diffPL(m) < 0
				bta	= btaSmp(2,m + btaNotInfIndx(1) - 1);
				C0	= x^2 + w^2;
				A	= -cos(bta) - x/r_min;
				B	= sin(bta) - w/r_min;
				C	= 1 - (x*cos(bta) - w*sin(bta))/r_min - C0/(2*r_min^2);
				thtaP	= 2*atan((A + sqrt(A^2 + B^2 - C^2))/(B+C));		% Tangent angle of C+C- path at W
			else
				thtaP	= gamMSmp(m) - pi/2;								% Tangent angle of C+ path at W
			end
			thtaPSmp(n) = thtaP;

			if diffMU(m) > 0		
				bta = btaSmp(1,m + btaNotInfIndx(1) - 1);		
				C0	= x^2 + w^2;
				A	= -cos(bta) + x/r_min;
				B	= sin(bta) + w/r_min;
				C	= 1 + (x*cos(bta) - w*sin(bta))/r_min - C0/(2*r_min^2);
				thtaM	= max(-pi/2, 2*atan((A - sqrt(A^2 + B^2 - C^2))/(B+C)));	% Tangent angle of C-C+ path at W
			else
				if (m + btaNotInfIndx(1) - 1) >= smpN1L
					thtaM = gamPSmp(m) - pi/2;								% Tangent angle of C- path at W
				else
					thtaM = -pi/2;
				end
			end
			thtaMSmp(n) = thtaM;
		end

		alfa(1) = max(thtaPSmp);
		alfa(2) = min(thtaMSmp);
	end
	%**********************************************************************
	
	%**********************************************************************
	function [u, indx] = remove_inf_values(v)
		infty = (v == Inf) + (v == -Inf);
		if ~nnz(infty), u = v; indx = 1:numel(v); return; end
		if nnz(infty) == numel(v); u = []; indx = []; return; end

		firstInf	= find(infty, 1, 'first');
		nonInf		= (v(firstInf:end) ~= Inf).*(v(firstInf:end) ~= -Inf);
		endFirstInf = find(nonInf, 1, 'first') - 1 + firstInf - 1;
		if ~numel(endFirstInf), endFirstInf = numel(v); end

		if (firstInf - 1) >= (numel(v) - endFirstInf)
			u	= v(1:(firstInf-1));
			indx= 1:(firstInf - 1);
		else
			[u, indx]	= remove_inf_values(v((endFirstInf + 1):end));
			indx		= endFirstInf + indx;
		end
	end
	%**********************************************************************
	
	%**********************************************************************
	function smp = find_sample(ySmp, y)
		if y < ySmp(1), smp = 1; return; end
		if y > ySmp(end), smp = numel(ySmp); return; end

		temp= (ySmp >= y);													% First sample no less than
		smp	= find(temp, 1, 'first');
	end
	%**********************************************************************
	
	%**********************************************************************
	function idx_zeros = find_zeros(fSmp)
		f1		= [sign(fSmp) 0];
		f2		= [0 sign(fSmp)];
		fDiff	= f1 - f2;
		fDiff	= fDiff(2:(end-1));

		idx_zeros = find(fDiff);												% Last sample of that sign
	end
	%**********************************************************************
	
	%**********************************************************************
	function x = pi2pi(x)
		% Returns angle between -pi and pi

		if numel(x) > 1
			x			= mod(x, 2*pi);
			x(x > pi)	= x(x > pi) - 2*pi;
			x(x < -pi)	= x(x < -pi) + 2*pi;
		else
			while (x > pi) || (x < -pi)
				if x > pi,
					x = x - 2*pi;
				else
					x = x + 2*pi;
				end
			end
		end
	end
	%**********************************************************************
	
	%**********************************************************************
	function region_neighbors = cbra(idx_r_from)
		
		rgn_from_w_lower	= REGION_BD(1, idx_r_from + 1);
		rgn_from_w_upper	= REGION_BD(2, idx_r_from + 1);
		rgn_from_psi_lower	= REGION_BD(3, idx_r_from + 1);
		rgn_from_psi_upper	= REGION_BD(4, idx_r_from + 1);
		
% 		[idx_r_from rgn_from_psi_lower*180/pi rgn_from_psi_upper*180/pi rgn_from_w_lower rgn_from_w_upper]
		
		if this_tile_data.traversal_type(1) == 1
			if r_min*sin(rgn_from_psi_lower) + r_min < 1
				xL_from_wL_psiL		= -Inf;				
			else
				xL_from_wL_psiL		= rgn_from_w_lower - r_min*cos(rgn_from_psi_lower) ...
					+ sqrt(r_min^2 -(r_min*sin(rgn_from_psi_lower) - 1)^2);	% Clockwise arc				
			end
			
			if r_min - r_min*sin(rgn_from_psi_lower) < 1
				xU_from_wL_psiL		= Inf;
				btaU_from_wL_psiL	= pi/2;
			else
				xU_from_wL_psiL	= rgn_from_w_lower + r_min*cos(rgn_from_psi_lower) ...
					- sqrt(r_min^2 -(r_min*sin(rgn_from_psi_lower) + 1)^2);	% Counter-clockwise arc
				btaU_from_wL_psiL	= asin(sin(rgn_from_psi_lower) + 1/r_min);
			end

			if r_min*sin(rgn_from_psi_upper) + r_min < 1
				xL_from_wU_psiU		= -Inf;
				btaL_from_wL_psiU	= -pi/2;
			else
				xL_from_wU_psiU		= rgn_from_w_upper - r_min*cos(rgn_from_psi_upper) ...
					+ sqrt(r_min^2 -(r_min*sin(rgn_from_psi_upper) - 1)^2);	% Clockwise arc
				btaL_from_wL_psiU	= asin(sin(rgn_from_psi_upper) - 1/r_min);
			end
			
			if r_min - r_min*sin(rgn_from_psi_upper) < 1
				xU_from_wU_psiU		= Inf;
			else
				xU_from_wU_psiU		= rgn_from_w_upper + r_min*cos(rgn_from_psi_upper) ...
					- sqrt(r_min^2 - (r_min*sin(rgn_from_psi_upper) + 1)^2);	% Counter-clockwise arc
			end			
		else
			xL_from_wL_psiL		= r_min*sin(rgn_from_psi_lower) + sqrt(r_min^2 ...
				- (r_min*cos(rgn_from_psi_lower) - rgn_from_w_lower)^2);
			btaL_from_wL_psiU	= asin(cos(rgn_from_psi_upper) - rgn_from_w_lower/r_min);
			
			xL_from_wU_psiU		= r_min*sin(rgn_from_psi_upper) + sqrt(r_min^2 ...
				- (r_min*cos(rgn_from_psi_upper) - rgn_from_w_upper)^2);
			
			
			if rgn_from_psi_lower < -acos((r_min - rgn_from_w_lower)/r_min)
				xU_from_wL_psiL		= r_min*sin(-rgn_from_psi_lower) - ...
					sqrt(r_min^2 - (r_min*cos(rgn_from_psi_lower) + rgn_from_w_lower)^2);
				btaU_from_wL_psiL	= asin(cos(rgn_from_psi_lower) + rgn_from_w_lower/r_min);
			else
				xU_from_wL_psiL		= Inf;
				btaU_from_wL_psiL	= pi/2;
			end
						
			if rgn_from_psi_upper < -acos((r_min - rgn_from_w_upper)/r_min)
				xU_from_wU_psiU		= r_min*sin(-rgn_from_psi_upper) - ...
					sqrt(r_min^2 - (r_min*cos(rgn_from_psi_upper) + rgn_from_w_upper)^2);
			else
				xU_from_wU_psiU		= Inf;
			end
		end
		
% 		[btaU_from_wL_psiL btaL_from_wL_psiU]*180/pi
		
		if ~isreal([xL_from_wL_psiL xU_from_wL_psiL btaU_from_wL_psiL...
				xL_from_wU_psiU btaL_from_wL_psiU xU_from_wU_psiU xU_from_wU_psiU])
			error('Huh??')
		end
		
		region_neighbors	= 0:N_REGION_TOTAL;
		is_region_neighbors = false(1, N_REGION_TOTAL + 1);
	
		%--- Intersect reachable region with Q		
		is_flipped_abt_h_1 = any(this_tile_data.cell_xform(1, :) == 3);
		is_flipped_abt_v_1 = any(this_tile_data.cell_xform(1, :) == 4);
		is_flipped_1		= (is_flipped_abt_h_1 && ~is_flipped_abt_v_1) ...
				|| (~is_flipped_abt_h_1 && is_flipped_abt_v_1);
		
		if H > 1
			is_flipped_abt_h_2	= any(this_tile_data.cell_xform(2, :) == 3);
			is_flipped_abt_v_2	= any(this_tile_data.cell_xform(2, :) == 4);
			is_flipped_2		= (is_flipped_abt_h_2 && ~is_flipped_abt_v_2) ...
				|| (~is_flipped_abt_h_2 && is_flipped_abt_v_2);
		end
		
		for idx_r_to = 0:N_REGION_TOTAL
			%--- If this region has no reachable intersection with Q,
			% then do nothing
			%{
			The target sets of Q are already in coordinates of axes
			attached to the second cell, so no need to flip anything here.
			%}
			if ~region_target_2(idx_r_to + 1), continue; end

			if ( H > 1 ) && ( (is_flipped_1 && ~is_flipped_2) || ...
					(~is_flipped_1 && is_flipped_2) )
				rgn_to_w_lower_xform	= 1 - REGION_BD(2, idx_r_to + 1);
				rgn_to_w_upper_xform	= 1 - REGION_BD(1, idx_r_to + 1);
				rgn_to_psi_lower_xform	= -REGION_BD(4, idx_r_to + 1);
				rgn_to_psi_upper_xform	= -REGION_BD(3, idx_r_to + 1);
			else
				rgn_to_w_lower_xform	= REGION_BD(1, idx_r_to + 1);
				rgn_to_w_upper_xform	= REGION_BD(2, idx_r_to + 1);
				rgn_to_psi_lower_xform	= REGION_BD(3, idx_r_to + 1);
				rgn_to_psi_upper_xform	= REGION_BD(4, idx_r_to + 1);
			end
			
% 			if xL_from_wU_psiU > rgn_to_w_upper_xform, fprintf('Exit 1'); continue; end			
% 			if xU_from_wL_psiL < rgn_to_w_lower_xform, fprintf('Exit 2'); continue; end
% 			if xU_from_wU_psiU < rgn_to_w_lower_xform, fprintf('Exit 3'); continue; end
% 			if xL_from_wL_psiL > rgn_to_w_upper_xform, fprintf('Exit 4'); continue; end
% 			
% 			
% 			if btaL_from_wL_psiU > rgn_to_psi_upper_xform, fprintf('Exit 5'); continue; end
% 			if btaU_from_wL_psiL < rgn_to_psi_lower_xform, fprintf('Exit 6'); continue; end
			
			if xL_from_wU_psiU > rgn_to_w_upper_xform, continue; end
			if xU_from_wL_psiL < rgn_to_w_lower_xform, continue; end
			if xU_from_wU_psiU < rgn_to_w_lower_xform, continue; end
			if xL_from_wL_psiL > rgn_to_w_upper_xform, continue; end			
			
			if btaL_from_wL_psiU > rgn_to_psi_upper_xform, continue; end
			if btaU_from_wL_psiL < rgn_to_psi_lower_xform, continue; end
			
			is_region_neighbors(idx_r_to + 1) = true;			
		end
		region_neighbors = region_neighbors(is_region_neighbors)';
	end
	%**********************************************************************
end