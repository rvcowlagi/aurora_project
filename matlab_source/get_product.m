function bl_product = get_product(H, VH, GH, vertH_s, buchi_aut, region, alphabet, Alph_s)
n_vertsH= size(VH, 1);
tmp1	= zeros(1, n_vertsH);
tmp1(vertH_s) = 1;

VH_aug	= [VH; zeros(1, H+1)];
GH_aug	= [GH zeros(n_vertsH, 1); tmp1 0];
fprintf('Constructing product automaton ... \t')
tic
bl_product.states_n		= (n_vertsH + 1)*numel(buchi_aut.S);		% Number of states
bl_product.states_init	= (n_vertsH*numel(buchi_aut.S) + 1) : ((n_vertsH + 1)*numel(buchi_aut.S) );
bl_product.states_accept= zeros(n_vertsH*numel(buchi_aut.F), 1);
bl_product.states_id	= zeros(bl_product.states_n, H+2);
bl_product.adjacency	= [];
edge_list	= zeros( (nnz(GH) + size(VH, 1))*(numel(buchi_aut.S)^2), 3); % All the possible transition relation
n_edges		= 0;    %Actual number of transition relations in the product automaton

for m1 = 0:(bl_product.states_n-1)
    m11 = floor( m1/numel(buchi_aut.S) ) + 1; %m11: current node  
    %find current p using m11
    m12 = mod( m1, numel(buchi_aut.S) ) + 1;  
    bl_product.states_id(m1 + 1, :)	= [VH_aug(m11, :) buchi_aut.S(m12)]; 
    nhbrs_m11 = find(GH_aug(m11, :) > 0); %neighbour node
    if ~numel(nhbrs_m11), continue; end
    
    for m33 = nhbrs_m11  %m33 for every neighbour node
%         neighbour_region  = get_region(VH_aug(m33,:), region, alphabet);
        neighbour_region  = get_region(VH_aug(m33,end), region, alphabet); % identify the region only by the last node in lifted vertex 
        
        Alph_id = find(ismember(Alph_s,neighbour_region));
        
        
        for m22 = 1:numel(buchi_aut.S)
            if any(buchi_aut.trans{m12, m22} == Alph_id)
                %{
					Check if there is a transition in the Buchi automaton
					from the Buchi state portion of the current
					"product-state"  to this (m22) Buchi state. If yes,
					then find the index of the new "product-state" and add
					to the list of edges in the product automaton.
                %}
                m2		= (m33 - 1)*numel(buchi_aut.S) + m22;
                n_edges	= n_edges + 1;
                edge_list(n_edges, :)	= [m1+1 m2 1];
            end
        end
    end
end
bl_product.adjacency = sparse(edge_list(1:n_edges, 1), edge_list(1:n_edges, 2), ...
    edge_list(1:n_edges, 3), bl_product.states_n, bl_product.states_n);
toc